import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import * as enums from '../../services/enums';

import { userThunks, userSelectors } from '../../features/user';
import { eWalletThunks, eWalletSelectors } from '../../features/ewallet';
import { transferThunks, transferSelectors } from '../../features/transfer';

const useDataDispatch = () => {
  const dispatch = useDispatch();

  const isLoggedIn = useSelector(userSelectors.selectIsLoggedIn);
  const fetchUserStatus = useSelector(userSelectors.selectUserFetchingStatus);
  const fetchEWalletStatus = useSelector(
    eWalletSelectors.selectEWalletFetchStatus
  );
  const fetchFavoriteTransactionsStatus = useSelector(
    transferSelectors.selectFavoriteTransactionsFetchStatus
  );

  useEffect(() => {
    if (isLoggedIn && fetchUserStatus === enums.IDLE) {
      dispatch(userThunks.fetchUserData());
    }
  }, [isLoggedIn, dispatch, fetchUserStatus]);

  useEffect(() => {
    if (fetchUserStatus === enums.FULFILLED) {
      fetchEWalletStatus === enums.IDLE &&
        dispatch(eWalletThunks.fetchUserFavoriteEWallet());
    }
  }, [fetchUserStatus, dispatch, fetchEWalletStatus]);

  useEffect(() => {
    if (fetchUserStatus === enums.FULFILLED) {
      fetchFavoriteTransactionsStatus === enums.IDLE &&
        dispatch(transferThunks.fetchUserFavoriteTransactions());
    }
  }, [fetchUserStatus, fetchFavoriteTransactionsStatus, dispatch]);
};

export default useDataDispatch;
