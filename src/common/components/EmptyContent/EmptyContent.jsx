import DetectiveMan from '../../../assets/img/DetectiveMan.png';

const EmptyContent = () => {
  return (
    <section className="mx-auto d-flex flex-column align-items-center py-5">
      <div className="my-4">
        <img src={DetectiveMan} alt="" />
      </div>
      <div className="text-center py-4">
        <h3 className="empty-message">Pencarian Tidak Ditemukan</h3>
        <p className="mx-auto">
          Coba cari nama lain atau periksa kembali nama yang kamu cari
        </p>
      </div>
    </section>
  );
};

export default EmptyContent;
