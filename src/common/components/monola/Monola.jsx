import { Link } from 'react-router-dom';

import styles from './Monola.module.scss';

const Monola = ({ size, color }) => {
  return (
    <Link
      to="/"
      className={
        size === 'lg'
          ? styles.monolaLg
          : size === 'lg-2'
          ? styles.monolaLg2
          : styles.monola
      }>
      monola
    </Link>
  );
};

export default Monola;
