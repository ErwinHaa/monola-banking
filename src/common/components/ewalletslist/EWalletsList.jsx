import { useSelector } from 'react-redux';

import { eWalletSelectors } from '../../../features/ewallet';

import styles from './EWallet.module.scss';
import { classNameJoin } from '../../../services/helpers';

import EWalletItem from './EWalletItem';
import { EmptyFavorites } from '../EmptyFavorites';

const EWalletsList = () => {
  const favoriteEWallets = useSelector(eWalletSelectors.selectFavoriteEwallets);

  const eWalletClassName = classNameJoin([
    'd-flex flex-nowrap',
    styles.eWalletContainer,
  ]);

  const renderedEWallet = favoriteEWallets.map(({ id }) => (
    <EWalletItem key={id} eWalletId={id} />
  ));

  return (
    <div className="my-4">
      <h5 className={styles.title}>e-Wallet Favorit</h5>
      <div className={eWalletClassName}>
        {favoriteEWallets.length ? (
          renderedEWallet
        ) : (
          <EmptyFavorites>
            <EmptyFavorites.Head>
              eWallet Favorit Masih Kosong
            </EmptyFavorites.Head>
            <EmptyFavorites.Paragraph>
              Lakukan top up baru dan simpan akun penerima agar akun yang
              difavoritkan bisa muncul disini.
            </EmptyFavorites.Paragraph>
          </EmptyFavorites>
        )}
      </div>
    </div>
  );
};

export default EWalletsList;
