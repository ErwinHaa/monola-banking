import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation, useHistory, Redirect } from 'react-router-dom';

import {
  separatorWithDot,
  checkObjectIsNotEmpty,
} from '../../../services/helpers';

import { transferActions, transferThunks } from '../../../features/transfer';
import { eWalletActions } from '../../../features/ewallet';

import { ReactComponent as Completed } from '../../../assets/img/Completed.svg';

import BCA from '../../../assets/img/Bank-bca.png';
import OVO from '../../../assets/img/OVO.png';
import GOPAY from '../../../assets/img/Gopay.png';
import Permata from '../../../assets/img/Permata.png';
import DANA from '../../../assets/img/DANA.png';
import BRI from '../../../assets/img/BRI.png';
import CIMBNiaga from '../../../assets/img/CIMB.png';
import LinkAja from '../../../assets/img/Linkaja.png';

import { classNameJoin } from '../../../services/helpers';

import styles from './TransactionSuccess.module.scss';

const TransactionSuccess = ({
  isFavorited,
  nominal,
  description,
  date,
  recipient,
  onCoinReceived,
}) => {
  const banks = { BCA, OVO, GOPAY, Permata, DANA, BRI, CIMBNiaga, LinkAja };

  const location = useLocation();
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(transferActions.resetTransferStatus());
    dispatch(eWalletActions.resetTopUpStatus());
    dispatch(transferActions.resetFetchLastTransactionsStatus());

    if (nominal >= 50000) {
      onCoinReceived();
    }

    if (isFavorited) {
      dispatch(transferThunks.setRecipientAsFavorite());
    }

    return history.listen((location) => {
      if (history.action === 'POP') {
        history.replace('/dashboard');

        dispatch(transferActions.resetRecipient());
        dispatch(eWalletActions.resetEWalletRecipient());
      }
    });
  }, [history, dispatch, isFavorited, onCoinReceived, nominal]);

  if (
    location.pathname.includes('ewallet')
      ? !checkObjectIsNotEmpty(recipient)
      : !recipient
  ) {
    return <Redirect to="/dashboard" />;
  }

  let bankImg = banks[recipient.provider];

  const successContainerClassName = classNameJoin([
    'd-flex flex-column',
    styles.container,
  ]);

  const headingContainerClassName = classNameJoin([
    'd-flex flex-column align-items-center justify-content-between p-5',
    styles.headingContainer,
  ]);

  return (
    <>
      <div className={successContainerClassName}>
        <div className={headingContainerClassName}>
          <Completed />
          <h3 className={styles.successHeading}>
            {location.pathname.includes('ewallet') ? 'Top Up' : 'Transfer'}{' '}
            Berhasil!
          </h3>
        </div>
        <div className={styles.detailContainer}>
          <div className="row no-gutters">
            <div className="col-1">
              <div className="image">
                <img src={bankImg} alt="" />
              </div>
            </div>
            <div className="col-11">
              <div className="textCont mx-2">
                <p className={styles.recipientName}>{recipient.userName}</p>
                <p className={styles.recipientAccountDetail}>
                  {location.pathname.includes('transfer') && 'Bank'}{' '}
                  {recipient.provider} {recipient.identityNumber}
                </p>
              </div>
            </div>
          </div>
          <div className="mt-4">
            <div className="row mb-4">
              <div className="col">
                <p>Jumlah</p>
              </div>
              <div className="col text-right">
                <p className={styles.nominal}>
                  Rp. {separatorWithDot(nominal)}
                </p>
              </div>
            </div>
            <div className="row mb-4">
              <div className="col">
                <p>Deskripsi</p>
              </div>
              <div className="col text-right">
                <p className={styles.nominal}>
                  {description ? description : '-'}
                </p>
              </div>
            </div>
            <div className="row">
              <div className="col">Tgl. Transaksi</div>
              <div className="col text-right fw-600">
                {date.replace('.', ':')}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default TransactionSuccess;
