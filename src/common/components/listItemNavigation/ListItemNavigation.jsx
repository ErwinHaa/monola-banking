import { BiChevronRight } from 'react-icons/bi';

import { ReactComponent as OffToggle } from '../../../assets/img/offToggle.svg';
import { ReactComponent as OnToggle } from '../../../assets/img/onToggle.svg';

import styles from './ListItemNavigation.module.scss';

const ListItemNavigation = ({ onClick, text, toggle }) => {
  const renderedIcon = () => {
    if (toggle === true) {
      return <OnToggle className="float-right"/>
    } else if (toggle === false) {
      return <OffToggle className="float-right"/>
    } else {
      return <BiChevronRight size="1.563rem" className="float-right"/>
    }
  }

  return (
    <div className={styles.row} onClick={onClick}>
        <span className={styles.rowText}>{text}</span>
        {renderedIcon()}
    </div>
  )
}

export default ListItemNavigation