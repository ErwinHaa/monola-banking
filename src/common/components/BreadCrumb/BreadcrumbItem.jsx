import { Link } from 'react-router-dom';

import styles from './BreadcrumbItem.module.scss';

import { classNameJoin } from '../../../services/helpers';

const BreadcrumbItem = ({ to, step, children, replace, isActive }) => {
  const itemClassName = classNameJoin([styles.item, isActive && styles.active]);

  if (!!to) {
    return (
      <Link
        to={to ? to : ''}
        replace={replace ? replace : false}
        className={itemClassName}>
        {children}
      </Link>
    );
  } else {
    return (
      <span onClick={!!step ? step : () => {}} className={itemClassName}>
        {children}
      </span>
    );
  }
};

export default BreadcrumbItem;
