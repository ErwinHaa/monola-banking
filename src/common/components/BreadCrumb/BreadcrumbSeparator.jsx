import styles from './BreadcrumbSeparator.module.scss';

import { ReactComponent as Separator } from '../../../assets/img/Separator.svg';

const BreadcrumbSeparator = () => {
  return <Separator className={styles.separator} />;
};

export default BreadcrumbSeparator;
