const Breadcrumb = ({ children }) => {
  return <div className="d-flex align-items-center mb-3">{children}</div>;
};

export default Breadcrumb;
