import { useEffect } from 'react';
import { Link, useHistory, useLocation, Redirect } from 'react-router-dom';
import { Form, Spinner } from 'react-bootstrap';
import ReactCodeInput from 'react-code-input';
import { useSelector, useDispatch } from 'react-redux';

import { transferSelectors, transferThunks } from '../../../features/transfer';
import { eWalletSelectors } from '../../../features/ewallet';

import styles from './InsertPin.module.scss';

import ButtonNext from '../button/ButtonNext';
import ButtonPrevious from '../button/ButtonPrevious';
import { checkObjectIsNotEmpty } from '../../../services/helpers';

const InsertPin = ({
  pin,
  onChange,
  onClick,
  isFavorited,
  previousStep,
  lastStep,
  nextStep,
  recipient,
}) => {
  const transferStatus = useSelector((state) => state.transfer.transferStatus);
  const transferErrorMsg = useSelector(
    transferSelectors.selectTransferErrorMsg
  );

  const topUpStatus = useSelector((state) => state.eWallet.topUpStatus);
  const topUpErrorMsg = useSelector(eWalletSelectors.selectTopUpErrorMsg);

  const fetchRecipientStatus = useSelector(
    transferSelectors.selectFetchRecipientStatus
  );
  const fetchEWalletStatus = useSelector(
    eWalletSelectors.selectEWalletRecipientFetchStatus
  );

  const history = useHistory();
  const location = useLocation();
  const dispatch = useDispatch();

  useEffect(() => {
    if (transferStatus === 'rejected' && !transferErrorMsg) {
      history.push('/dashboard/transaksi/transfer/transfer-failed');
      return;
    } else if (topUpStatus === 'rejected' && !topUpErrorMsg) {
      history.push('/dashboard/ewallet/top-up-ewallet/top-up-failed');
      return;
    } else if (transferStatus === 'fulfilled') {
      if (isFavorited) {
        dispatch(transferThunks.setRecipientAsFavorite());
      }
      lastStep();
      return;
    } else if (topUpStatus === 'fulfilled') {
      lastStep();
      return;
    }
  }, [
    transferStatus,
    topUpStatus,
    lastStep,
    history,
    dispatch,
    isFavorited,
    transferErrorMsg,
    topUpErrorMsg,
  ]);

  const shouldRenderError =
    !!transferErrorMsg || !!topUpErrorMsg ? (
      <p className={styles.pinInvalidText}>
        {transferErrorMsg || topUpErrorMsg}
      </p>
    ) : null;

  const shouldRenderLoading =
    transferStatus === 'pending' || topUpStatus === 'pending' ? (
      <Spinner animation="border" variant="primary" />
    ) : (
      <>
        <div className="col-4 text-center">
          <ButtonPrevious onClick={previousStep}>Batal</ButtonPrevious>
        </div>
        <div className="col-4 text-center">
          <ButtonNext disabled={pin.length < 6} onClick={onClick}>
            {location.pathname.includes('ewallet') ? 'Top Up' : 'Transfer'}
          </ButtonNext>
        </div>
      </>
    );

  if (
    location.pathname.includes('transfer')
      ? !recipient && fetchRecipientStatus === 'idle'
      : !checkObjectIsNotEmpty(recipient) && fetchEWalletStatus === 'idle'
  ) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <>
      <div className="d-flex flex-column align-items-center justify-content-between h-100 w-100 ">
        <div className="row py-4">
          <h4>Masukkan PIN Anda</h4>
        </div>
        <div className="row py-4 w-100 justify-content-center">
          <ReactCodeInput
            type="password"
            fields={6}
            value={pin}
            onChange={onChange}
            className={styles.pinInput}
          />
          {shouldRenderError}
        </div>
        <Link to="#" onClick={nextStep} className={styles.forgetPinText}>
          Lupa PIN? Klik Disini
        </Link>
        <Form.Group className="w-100 py-5">
          <div className="row justify-content-center w-100">
            {shouldRenderLoading}
          </div>
        </Form.Group>
      </div>
    </>
  );
};

export default InsertPin;
