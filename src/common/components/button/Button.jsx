import styles from './Button.module.scss';

const Button = ({ disabled = false, children, onClick, className, style }) => {
  let classNames = styles.button;

  if (className) {
    classNames = classNames + ' ' + className;
  }

  return (
    <button
      className={classNames}
      disabled={disabled}
      onClick={onClick}
      style={style}>
      {children}
    </button>
  );
};

export default Button;
