import { Button } from 'react-bootstrap';

import styles from './ButtonOutline.module.scss';

const ButtonOutline = ({ children, onClick }) => {
  return (
    <Button className={styles.btnOutline} onClick={onClick}>
      {children}
    </Button>
  );
};

export default ButtonOutline;
