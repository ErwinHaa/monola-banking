import { Link } from 'react-router-dom';
import styles from './ButtonTransfer.module.scss';

import { ReactComponent as Transfer } from '../../../assets/img/Transfer.svg';

const ButtonTransfer = () => {
  return (
    <Link
      to="/dashboard/transaksi/transfer/search-account"
      className={styles.button}>
      <Transfer
        className={styles.icon}
        stroke="currentColor"
        fill="currentColor"
        strokeWidth="0"
      />
      TRANSFER
    </Link>
  );
};

export default ButtonTransfer;
