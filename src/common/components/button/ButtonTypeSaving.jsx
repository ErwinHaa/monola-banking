import { Button } from 'react-bootstrap';
import styles from './ButtonTypeSaving.module.scss';
const ButtonTypeSaving = ({ onClick }) => {
    return (
        <Button className={styles.button}
            onClick={onClick}>
            BUAT TABUNGAN BARU
        </Button>
    )
}

export default ButtonTypeSaving
