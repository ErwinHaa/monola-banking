import Button from './Button';

import styles from './ButtonRed.module.scss';

const ButtonRed = ({ children }) => {
  return <Button className={styles.btnRed}>{children}</Button>;
};

export default ButtonRed;
