import Button from './Button';
import ButtonPrimary from './ButtonPrimary';

Button.Primary = ButtonPrimary;

export { Button };
