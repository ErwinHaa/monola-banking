import { Alert } from 'react-bootstrap';

import { classNameJoin } from '../../../services/helpers';

import styles from './CustomAlert.module.scss';

const CustomAlert = ({ children, className }) => {
  const alertClassName = classNameJoin([styles.alert, 'my-3', className]);
  return (
    <Alert className={alertClassName}>
      <p className="text-white mb-0">{children}</p>
    </Alert>
  );
};

export default CustomAlert;
