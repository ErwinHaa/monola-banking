import ContentLoader from 'react-content-loader';

const CardLoader = (props) => (
  <ContentLoader
    speed={2}
    width={600}
    height={200}
    viewBox="0 0 600 200"
    backgroundColor="#a1a1a1"
    foregroundColor="#ecebeb"
    {...props}>
    <rect x="6" y="1" rx="3" ry="3" width="185" height="17" />
    <rect x="11" y="27" rx="3" ry="3" width="106" height="12" />
    <rect x="7" y="83" rx="3" ry="3" width="209" height="15" />
    <rect x="371" y="144" rx="3" ry="3" width="196" height="36" />
    <rect x="14" y="66" rx="3" ry="3" width="73" height="8" />
  </ContentLoader>
);

export default CardLoader;
