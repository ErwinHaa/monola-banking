import React from 'react';
import ContentLoader from 'react-content-loader';

const SidebarLoader = (props) => (
  <ContentLoader
    speed={2}
    width={300}
    height={90}
    viewBox="0 0 300 100"
    backgroundColor="#a1a1a1"
    foregroundColor="#ecebeb"
    {...props}>
    <rect x="76" y="70" rx="3" ry="3" width="150" height="13" />
    <rect x="100" y="90" rx="3" ry="3" width="100" height="10" />
    <circle cx="150" cy="30" r="30" />
  </ContentLoader>
);

export default SidebarLoader;
