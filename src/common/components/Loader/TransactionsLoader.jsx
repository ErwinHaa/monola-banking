import React from 'react';
import ContentLoader from 'react-content-loader';

const TransactionsLoader = (props) => (
  <ContentLoader
    speed={2}
    width={800}
    height={300}
    viewBox="0 0 800 300"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}>
    <rect x="20" y="18" rx="5" ry="5" width="600" height="50" />
    <rect x="20" y="75" rx="5" ry="5" width="600" height="50" />
    <rect x="20" y="135" rx="5" ry="5" width="600" height="50" />
    <rect x="20" y="195" rx="5" ry="5" width="600" height="50" />
    <rect x="20" y="255" rx="5" ry="5" width="600" height="50" />
  </ContentLoader>
);

export default TransactionsLoader;
