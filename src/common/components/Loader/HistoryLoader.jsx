import React from 'react';
import ContentLoader from 'react-content-loader';

const HistoryLoader = (props) => (
  <ContentLoader
    speed={2}
    width={1000}
    height={500}
    viewBox="0 0 1000 500"
    backgroundColor="#a1a1a1"
    foregroundColor="#ecebeb"
    {...props}>
    <rect x="25" y="40" rx="5" ry="5" width="546" height="71" />
    <rect x="27" y="14" rx="5" ry="5" width="154" height="9" />
    <rect x="31" y="341" rx="5" ry="5" width="154" height="9" />
    <rect x="27" y="143" rx="5" ry="5" width="546" height="71" />
    <rect x="28" y="248" rx="5" ry="5" width="546" height="71" />
    <rect x="30" y="370" rx="5" ry="5" width="546" height="71" />
  </ContentLoader>
);

export default HistoryLoader;
