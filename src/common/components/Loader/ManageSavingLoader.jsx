import React from 'react';
import ContentLoader from 'react-content-loader';

const ManageSavingLoader = (props) => (
  <ContentLoader
    speed={2}
    width={400}
    height={460}
    viewBox="0 0 400 460"
    backgroundColor="#a1a1a1"
    foregroundColor="#ecebeb"
    {...props}>
    <circle cx="145" cy="150" r="101" />
    <rect x="55" y="283" rx="2" ry="2" width="190" height="15" />
    <rect x="40" y="334" rx="2" ry="2" width="220" height="16" />
  </ContentLoader>
);

export default ManageSavingLoader;
