import React from 'react';
import ContentLoader from 'react-content-loader';

const HeaderLoader = (props) => (
  <ContentLoader
    speed={2}
    width={600}
    height={20}
    viewBox="0 0 600 20"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}>
    <rect x="10" y="-4" rx="3" ry="3" width="223" height="25" />
  </ContentLoader>
);

export default HeaderLoader;
