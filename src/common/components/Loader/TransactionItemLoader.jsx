import React from 'react';
import ContentLoader from 'react-content-loader';

const TransactionItemLoader = (props) => (
  <ContentLoader
    speed={2}
    width={1000}
    height={124}
    viewBox="300 0 900 124"
    backgroundColor="#a1a1a1"
    foregroundColor="#ecebeb"
    {...props}>
    <rect x="0" y="31" rx="3" ry="3" width="1000" height="60" />
  </ContentLoader>
);

export default TransactionItemLoader;
