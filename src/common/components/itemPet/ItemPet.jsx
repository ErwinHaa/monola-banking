import styles from './ItemPet.module.scss';
import { classNameJoin } from '../../../services/helpers';
import TitleSavingSection from '../titleHeaderSaving/TitleSavingSection';
import { Route, NavLink, useLocation } from 'react-router-dom';
import EditItemPet from './EditItemPet';

const ItemPet = ({coin, petItems, aksesorisItems}) => {
    const location = useLocation();
    const rowCN = classNameJoin(['row mt-3', styles.rowPetItem]);

    const renderedHewanItems = petItems.map((petItem) => (<EditItemPet  
        petImg={petItem.petImg} 
        petImgBig={petItem.petImgBig}
        owned={petItem.owned}
        isHewanItems/>));
    
    const renderedAksesorisItems = aksesorisItems.map((aksesorisItem) => (<EditItemPet 
        aksesorisImg={aksesorisItem.aksesorisImg}
        owned={aksesorisItem.owned}
        isAksesorisItems/>))

    return (
        <div className={styles.itemPet}>
            <TitleSavingSection title="Edit" coin={coin}/>
            <div className={styles.buttonContainer}>
                <NavLink
                    to="/dashboard/saving/items/hewan"
                    className={styles.navLink}
                    activeClassName={location.pathname === "/dashboard/saving/items/hewan" ? styles.active : ''}
                >
                Hewan</NavLink>
                <NavLink
                    to="/dashboard/saving/items/aksesoris"
                    className={styles.navLink}
                    activeClassName={location.pathname === "/dashboard/saving/items/aksesoris" ? styles.active : ''}>
                    Aksesoris
                </NavLink>
                <NavLink 
                    to="/dashboard/saving/items/rumah"
                    className={styles.navLink}
                    activeClassName={location.pathname === "/dashboard/saving/items/rumah" ? styles.active : ''}>
                    Rumah
                </NavLink>
            </div>
            <div className={rowCN}>
                <Route path="/dashboard/saving/items/hewan">{renderedHewanItems}</Route>
                <Route path="/dashboard/saving/items/aksesoris">{renderedAksesorisItems}</Route>
            </div>
        </div>
    )
}

export default ItemPet
