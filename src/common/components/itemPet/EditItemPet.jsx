import styles from './EditItemPet.module.scss';
import EyeFilled from '../../../assets/img/eyeFilled.svg';
import { classNameJoin } from '../../../services/helpers';
import { useState } from 'react';
import ItemModal from '../modal/ItemModal';

const EditItemPet = ({ petImg, owned, isHewanItems, isAksesorisItems, aksesorisImg, }) => {
    const lihatContainerCN = classNameJoin([styles.lihatContainer, owned ? styles.owned : ''])

    const [modalShow, setModalShow] = useState(false);

    const onClickItem = () => {
        setModalShow(true);
    }

    const onHideModal = () => {
        setModalShow(false);
    }

    const questionItemPet = 'Apakah kamu ingin mengganti pet sebelumnya dengan pet ini?'
    const questionItemAksesoris = 'Apa yang ingin kamu lakukan dengan item ini?'

    return (
        (isHewanItems ?
        <>    
            <div className="col-6 mt-3" role="button" onClick={onClickItem}>
                <div className={styles.petItems}>
                    <div className={styles.petImgContainer}>
                        <img src={petImg} alt="" />
                    </div>
                    <div className={lihatContainerCN}>
                        <img src={EyeFilled} alt="eye-filled" />
                        <span className={styles.lihat}>Lihat</span>
                    </div>
                </div>
            </div>
            <ItemModal show={modalShow} onHide={onHideModal} petVariant={petImg} question={questionItemPet} option1="BATAL" option2="GANTI"/>
        </> :
        isAksesorisItems ?
        <>
            <div className="col-6 mt-3" role="button" onClick={onClickItem}>
                <div className={styles.petItems}>
                    <div className={styles.petImgContainer}>
                        <img src={aksesorisImg} alt="" />
                    </div>
                    <div className={lihatContainerCN}>
                        <img src={EyeFilled} alt="eye-filled" />
                        <span className={styles.lihat}>Lihat</span>
                    </div>
                </div>
            </div>
            <ItemModal show={modalShow} onHide={onHideModal} petVariant={petImg} question={questionItemAksesoris} option1="LEPAS" option2="PASANG"/>
        </>
        : ''
        )

    )
}

export default EditItemPet
