import EmptyFavorites from './EmptyFavorites';
import EmptyFavoritesHeading from './EmptyFavoritesHeading';
import EmptyFavoritesParagraph from './EmptyFavoritesParagraph';

EmptyFavorites.Head = EmptyFavoritesHeading;
EmptyFavorites.Paragraph = EmptyFavoritesParagraph;

export { EmptyFavorites };
