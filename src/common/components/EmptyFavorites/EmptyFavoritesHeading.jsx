const EmptyFavoritesHeading = ({ children }) => {
  return <p className="empty-favorite__heading">{children}</p>;
};

export default EmptyFavoritesHeading;
