import styles from './DashboardLayout.module.scss';

const DashboardLayout = ({ children }) => {
  return <section className={styles.layoutContainer}>{children}</section>;
};

export default DashboardLayout;
