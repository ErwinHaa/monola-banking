import styles from './DashboardLayoutBody.module.scss';

const DashboardLayoutBody = ({ children }) => {
  return <div className={styles.body}>{children}</div>;
};

export default DashboardLayoutBody;
