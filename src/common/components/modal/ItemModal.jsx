import styles from './ItemModal.module.scss';
import { Modal, Button } from 'react-bootstrap';
import Kucing2 from '../../../assets/img/kucing2.png';

const ItemModal = (props) => {
    return (
        <Modal
            {...props}
            centered
            aria-labelledby="contained-modal-title-vcenter"
            size="lg"
            className={styles.modal}
        >
            <Modal.Header className={styles.modalHeader}>
                <div className={styles.imgContainer}>
                    <img src={Kucing2} alt="" />
                </div>
            </Modal.Header>
            <Modal.Body className={styles.modalBody}>
                <p className={styles.question}>{props.question}</p>
                <div className="d-flex justify-content-between px-5">
                    <Button className={styles.btnOnHide} variant="link" onClick={props.onHide}>{props.option1}</Button>
                    <Button className={styles.btnAccept}variant="primary" onClick={props.onHide}>{props.option2}</Button>
                </div>
            </Modal.Body>
        </Modal>
    )
}

export default ItemModal
