import { Modal, Button } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';

import { useQuery } from '../../hooks';

import { userSelectors, userThunks } from '../../../features/user';

import styles from './ShopModal.module.scss';

import Coin from '../coin/Coin';

import Shadow from '../../../assets/img/shadowPet.svg';
import CoinImg from '../../../assets/img/coin.png';
import { petThunks } from '../../../features/pet';

const ShopModal = ({ itemId, selector, ...props }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const itemType = useQuery().get('item');
  const item = useSelector((state) => selector(state, itemId));

  const userCoin = useSelector(userSelectors.selectUserCoins);

  const renderShopConfirmationText =
    itemType === 'pet'
      ? 'Apakah kamu yakin ingin membeli pet ini?'
      : 'Apakah kamu yakin ingin membeli skin ini?';

  const renderEquipConfirmationText =
    itemType === 'pet'
      ? 'Apakah kamu ingin mengganti pet sebelumnya dengan pet ini?'
      : 'Apa yang ingin kamu lakukan dengan skin ini?';

  const whichConfirmationText = location.pathname.includes('shop')
    ? renderShopConfirmationText
    : renderEquipConfirmationText;

  if (!itemId) {
    return null;
  }

  const handlePetPurchase = () => {
    dispatch(petThunks.purchasePet(itemId));

    props.onHide();

    history.replace('/dashboard/saving');
  };

  const handlePetSwitch = () => {
    dispatch(userThunks.switchUserDisplayedPet(itemId));

    props.onHide();

    history.replace('/dashboard/saving');
  };

  const handleAccessoryEquip = () => {
    dispatch(userThunks.switchUserPetEquippedAccessory(itemId));

    props.onHide();

    history.replace('/dashboard/saving');
  };

  const handleAccessoryPurchase = () => {
    dispatch(userThunks.purchaseAccessory(itemId));

    props.onHide();

    history.replace('/dashboard/saving');
  };

  const whichPurchaseHandler =
    itemType === 'pet' ? handlePetPurchase : handleAccessoryPurchase;

  const whichUpdateHandler =
    itemType === 'pet' ? handlePetSwitch : handleAccessoryEquip;

  const whichHandler = location.pathname.includes('shop')
    ? whichPurchaseHandler
    : whichUpdateHandler;

  return (
    <Modal
      {...props}
      centered
      aria-labelledby="contained-modal-title-vcenter"
      size="lg"
      className={styles.modal}>
      <Modal.Header className={styles.modalHeader}>
        <div className={styles.imgContainer}>
          <img className={styles.petImg} src={item.itemImg} alt="big-pet" />
          <img className={styles.shadow} src={Shadow} alt="shadow" />
        </div>
        {location.pathname.includes('shop') && (
          <Coin className={styles.noAbsolute} coin={item.itemPrice} />
        )}
      </Modal.Header>
      <Modal.Body className={styles.modalBody}>
        {location.pathname.includes('shop') && itemType === 'pet' && (
          <p className="text-center font-weight-bold mb-1">{`"${item.itemName}"`}</p>
        )}
        <p className={styles.question}>{whichConfirmationText}</p>
        {location.pathname.includes('shop') && (
          <div className="d-flex justify-content-between mb-4">
            <span className={styles.description}>Koin yang kamu miliki</span>
            <div className={styles.coinContainer}>
              <img className={styles.coinImg} src={CoinImg} alt="" />
              <span className={styles.coinText}>{userCoin}</span>
            </div>
          </div>
        )}
        <div className="d-flex justify-content-between px-5">
          <Button
            className={styles.btnOnHide}
            variant="link"
            onClick={props.onHide}>
            TIDAK
          </Button>
          <Button
            className={styles.btnAccept}
            variant="primary"
            onClick={whichHandler}>
            {location.pathname.includes('shop') ? 'IYA' : 'GANTI'}
          </Button>
        </div>
      </Modal.Body>
    </Modal>
  );
};

export default ShopModal;
