import React from 'react';
import { ListGroup } from 'react-bootstrap';

import TransactionItem from './TransactionItem';

const TransactionGroup = React.memo(({ items, onClick, selector }) => {
  const renderedListItem = items.map((item) => (
    <TransactionItem
      key={item.id}
      itemId={item.id}
      onClick={onClick}
      selector={selector}
    />
  ));
  return <ListGroup>{renderedListItem}</ListGroup>;
});

export default TransactionGroup;
