import { Link } from 'react-router-dom';
import { IoCloseSharp } from 'react-icons/io5';

import styles from './PopUp.module.scss';

import Adopt from '../../../assets/img/adopt-pet.png';

const PopUp = ({ onClick, isHidden }) => {
  if (isHidden) {
    return null;
  }
  return (
    <div className={styles.popUp}>
      <IoCloseSharp className={styles.icon} size="18px" onClick={onClick} />
      <div className="row">
        <div className="col-9">
          <h3 className={styles.title}>Wujudkan Mimpi Bersama Pet-mu</h3>
          <p className={styles.subTitle}>
            Bertransaksi atau menabung untuk wujudkan mimpimu sambil merawat
            Pet-mu. Pengalaman menabung menjadi lebih menyenangkan!
          </p>
          <Link to="/dashboard/saving" className={styles.nabung}>
            Nabung Sekarang
          </Link>
        </div>
        <div className="col-3">
          <img src={Adopt} alt="" />
        </div>
      </div>
    </div>
  );
};

export default PopUp;
