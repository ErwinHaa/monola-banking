import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Spinner } from 'react-bootstrap';

import { transferSelectors } from '../../../features/transfer';

const InProgress = () => {
  const history = useHistory();
  const transferErrorMsg = useSelector(
    transferSelectors.selectTransferErrorMsg
  );

  if (!!transferErrorMsg) {
    history.goBack();
    return;
  }

  return (
    <div className="mx-auto d-flex flex-column align-items-center">
      <Spinner className="my-4" animation="border" variant="primary" />
      <h3>In Progress...</h3>
    </div>
  );
};

export default InProgress;
