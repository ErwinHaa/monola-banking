import { NavLink, useLocation } from 'react-router-dom';
import { AiOutlineHistory } from 'react-icons/ai';
import { useSelector } from 'react-redux';

import { classNameJoin } from '../../../services/helpers';

// import Osas from '../../../assets/img/osas.jpg';
import UserDefault from '../../../assets/img/DefaultUser.png';

import { userSelectors } from '../../../features/user';

import { ReactComponent as Home } from '../../../assets/img/Home.svg';
import { ReactComponent as EWallet } from '../../../assets/img/EWallet.svg';
import { ReactComponent as Transaction } from '../../../assets/img/Transaction.svg';
import { ReactComponent as User } from '../../../assets/img/User.svg';
import { ReactComponent as Logout } from '../../../assets/img/Logout.svg';

import styles from './Sidebar.module.scss';

import SidebarLoader from '../Loader/SidebarLoader';
import { NavLink as CustomNavLink } from '../../../common/components/NavLink';

const Sidebar = ({
  userPhoto,
  userFullName,
  userEmail,
  onLogoutOpen,
  showModal,
}) => {
  const location = useLocation();

  const navLinkClassName = classNameJoin(['nav-link', styles.navLink]);

  const fetchUserStatus = useSelector(userSelectors.selectUserFetchingStatus);

  return (
    <nav className={styles.sidebar}>
      <div className={styles.user}>
        {fetchUserStatus === 'pending' ? (
          <SidebarLoader />
        ) : (
          <>
            <figure className={styles.userImg}>
              <img
                className="img-thumbnail rounded-circle w-50 h-50 p-0 border-0"
                src={!!userPhoto ? userPhoto : UserDefault}
                alt="User Profile"
              />
            </figure>
            <p className={styles.userName}>
              {userFullName}
              <span className={styles.userEmail}>{userEmail}</span>
            </p>{' '}
          </>
        )}
      </div>
      <ul className="nav flex-column">
        <li className="nav-item">
          <NavLink
            exact
            to="/dashboard"
            className={navLinkClassName}
            activeClassName={styles.active}
            isActive={() =>
              (location.pathname.includes('detail') ||
                location.pathname === '/dashboard') &&
              !showModal
            }>
            <div className="row container justify-content-between no-gutters">
              <div className="col-2 d-flex align-items-center justify-content-start">
                <Home
                  className={styles.navIcon}
                  stroke="currentColor"
                  fill="currentColor"
                  strokeWidth="0"
                />
              </div>
              <div className="col-10 d-flex align-items-center justify-content-start pl-1">
                Beranda
              </div>
            </div>
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink
            to="/dashboard/transaksi"
            isActive={() =>
              (location.pathname.includes('transaksi') ||
                (location.pathname.includes('ewallet') &&
                  !location.pathname.includes('detail'))) &&
              !showModal
            }
            className={navLinkClassName}
            activeClassName={styles.active}>
            <div className="row container justify-content-between no-gutters">
              <div className="col-2 d-flex align-items-center justify-content-start">
                <Transaction
                  className={styles.navIcon}
                  stroke="currentColor"
                  fill="currentColor"
                  strokeWidth="0"
                />
              </div>
              <div className="col-10 d-flex align-items-center justify-content-start pl-1">
                Transaksi
              </div>
            </div>
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink
            exact
            to="/dashboard/saving"
            className={navLinkClassName}
            activeClassName={styles.active}
            isActive={() => location.pathname.includes('saving') && !showModal}>
            <div className="row container justify-content-between no-gutters">
              <div className="col-2 d-flex align-items-center justify-content-start">
                <EWallet
                  className={styles.navIcon}
                  stroke="currentColor"
                  fill="currentColor"
                  strokeWidth="0"
                />
              </div>
              <div className="col-10 d-flex align-items-center justify-content-start pl-1">
                Saving
              </div>
            </div>
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink
            exact
            to="/dashboard/history"
            className={navLinkClassName}
            isActive={() => location.pathname.includes('history') && !showModal}
            activeClassName={styles.active}>
            <div className="row container justify-content-between no-gutters">
              <div className="col-2 d-flex align-items-center justify-content-start">
                <AiOutlineHistory className={styles.navIcon} />
              </div>
              <div className="col-10 d-flex align-items-center justify-content-start pl-1">
                Riwayat
              </div>
            </div>
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink
            exact
            to="/dashboard/profile"
            className={navLinkClassName}
            activeClassName={styles.active}
            isActive={() =>
              location.pathname.includes('profile') && !showModal
            }>
            <div className="row container justify-content-between no-gutters">
              <div className="col-2 d-flex align-items-center justify-content-start">
                <User
                  className={styles.navIcon}
                  stroke="currentColor"
                  fill="currentColor"
                  strokeWidth="0"
                />
              </div>
              <div className="col-10 d-flex align-items-center justify-content-start pl-1">
                Profil
              </div>
            </div>
          </NavLink>
        </li>
        <li className="nav-item">
          <CustomNavLink
            className={navLinkClassName}
            activeClassName={styles.active}
            isActive={showModal}
            onClick={() => {
              onLogoutOpen();
            }}>
            <div className="row container justify-content-between no-gutters">
              <div className="col-2 d-flex align-items-center justify-content-start">
                <Logout
                  className={styles.navIcon}
                  stroke="currentColor"
                  fill="currentColor"
                  strokeWidth="0"
                />
              </div>
              <div className="col-10 d-flex align-items-center justify-content-start pl-1">
                Keluar
              </div>
            </div>
          </CustomNavLink>
        </li>
      </ul>
    </nav>
  );
};

export default Sidebar;
