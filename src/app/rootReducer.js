import { combineReducers } from 'redux';

import userReducer from '../features/user/';
import transferReducer from '../features/transfer';
import balanceReducer from '../features/balance';
import eWalletReducer from '../features/ewallet';
import savingReducer from '../features/saving';
import petReducer from '../features/pet';
import accessoriesReducer from '../features/accessories';
import historyReducer from '../features/history';

export const rootReducer = combineReducers({
  user: userReducer,
  transfer: transferReducer,
  balance: balanceReducer,
  eWallet: eWalletReducer,
  saving: savingReducer,
  pet: petReducer,
  accessories: accessoriesReducer,
  history: historyReducer,
});
