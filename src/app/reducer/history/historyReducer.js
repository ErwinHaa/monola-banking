import * as types from './historyTypes';

const historyReducer = (state, action) => {
  switch (action.type) {
    case types.SET_DATA_HISTORY: {
      const historyData = action.payload;
      return {...state, ...historyData};
    }

    default: {
      return state;
    }
  }
};

export default historyReducer;