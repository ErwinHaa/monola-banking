import historyReducer from "./historyReducer";

import * as historyActions from './historyActions';
import * as historyTypes from './historyTypes';

export { historyTypes, historyActions };

export default historyReducer;