import { createContext } from 'react';

const historyContext = createContext();

const { Provider } = historyContext;

export { Provider as HistoryProvider };

export default historyContext;