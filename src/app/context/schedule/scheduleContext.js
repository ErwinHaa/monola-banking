import { createContext } from 'react';

const scheduleContext = createContext();

const { Provider } = scheduleContext;

export { Provider as ScheduleProvider };

export default scheduleContext;
