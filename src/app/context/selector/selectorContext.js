import { createContext } from 'react';

const selectorContext = createContext();

const { Provider } = selectorContext;

export { Provider as SelectorProvider };

export default selectorContext;
