import { useState } from 'react';
import { Route } from 'react-router-dom';

import Footer from '../common/components/footer/Footer';
import Header from '../common/components/header/Header';
import HiddenFooter from '../common/components/hiddenFooter/HiddenFooter';

const BaseLayout = ({ children }) => {
  const [isHidden, setIsHidden] = useState(false);

  const handleHiddenFooterClick = () => {
    setIsHidden(true);
  };

  return (
    <>
      <Header />
      {children}
      <Route exact path={['/', '/privacy-policies', '/terms-and-conditions']}>
        <Footer isHidden={isHidden} />
        <HiddenFooter isHidden={isHidden} onClick={handleHiddenFooterClick} />
      </Route>
    </>
  );
};

export default BaseLayout;
