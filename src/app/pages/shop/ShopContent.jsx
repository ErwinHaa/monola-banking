import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { petSelectors, petThunks } from '../../../features/pet';
import {
  accessoriesSelectors,
  accessoriesThunks,
} from '../../../features/accessories';
import { userSelectors } from '../../../features/user';

import { useQuery } from '../../../common/hooks';

import { ReactComponent as House1 } from '../../../assets/img/House1.svg';
import { ReactComponent as House2 } from '../../../assets/img/House2.svg';

import styles from './ShopContent.module.scss';

import { SavingContent } from '../saving/SavingContent';
import ShopModal from '../../../common/components/modal/ShopModal';
import Coin from '../../../common/components/coin/Coin';

const ShopContent = () => {
  const [showModal, setShowModal] = useState(false);
  const [itemId, setItemId] = useState('');

  const item = useQuery().get('item');
  const dispatch = useDispatch();

  const pets = useSelector(petSelectors.selectAllPets);
  const accessories = useSelector(accessoriesSelectors.selectAllAccessories);

  const userCurrentPet = useSelector(userSelectors.selectUserCurrentPet);
  const userId = useSelector(userSelectors.selectUserId);

  const petsFetchingStatus = useSelector(petSelectors.selectPetsFetchingStatus);
  const accessoriesFetchingStatus = useSelector(
    accessoriesSelectors.selectAccessoriesFetchingStatus
  );

  const handleCloseModal = () => {
    setItemId('');
    setShowModal(false);
  };

  const handleOpenModal = (value) => {
    setItemId(value);
    setShowModal(true);
  };

  const whichSelector =
    item === 'pet'
      ? petSelectors.selectPetById
      : accessoriesSelectors.selectAccessoryById;

  const usedData = item === 'pet' ? pets : accessories;

  useEffect(() => {
    if (item === 'pet' && petsFetchingStatus === 'idle' && !!userId) {
      dispatch(petThunks.fetchPets());
    } else if (
      item === 'accessories' &&
      accessoriesFetchingStatus === 'idle' &&
      !!userId
    ) {
      dispatch(accessoriesThunks.fetchAccessories());
    }
  }, [item, dispatch, petsFetchingStatus, accessoriesFetchingStatus, userId]);

  const renderedItems = () => {
    if (
      item === 'accessories' &&
      userCurrentPet.petId !== '512004e2-5364-4f54-97ad-60ba4c330d2e'
    ) {
      return (
        <div className="py-2 mx-auto d-flex flex-column text-center mb-5">
          <h3 className={styles.emptyTitle}>Ups!</h3>
          <h3 className={styles.emptySubTitle}>
            Tidak ada item untuk pet ini.
          </h3>
        </div>
      );
    } else if (item === 'house') {
      return (
        <>
          <div className="col-6 mt-3">
            <div className={styles.houseItem}>
              <div className={styles.imgContainer}>
                <House1 className={styles.itemImage} />
              </div>
              <Coin coin={2000} coinShop />
            </div>
          </div>
          <div className="col-6 mt-3">
            <div className={styles.houseItem}>
              <div className={styles.imgContainer}>
                <House2 className={styles.itemImage} />
              </div>
              <Coin coin={3000} coinShop />
            </div>
          </div>
        </>
      );
    }

    return usedData.map((item) => (
      <SavingContent.Item
        key={item.id}
        itemId={item.id}
        selector={whichSelector}
        onClick={handleOpenModal}
      />
    ));
  };

  return (
    <>
      <SavingContent.Group>{renderedItems()}</SavingContent.Group>
      {!!itemId && (
        <ShopModal
          show={showModal}
          onHide={handleCloseModal}
          selector={whichSelector}
          itemId={itemId}
        />
      )}
    </>
  );
};

export default ShopContent;
