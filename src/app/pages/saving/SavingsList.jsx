import { useSelector } from 'react-redux';

import { savingSelectors } from '../../../features/saving';

import SavingItem from './SavingItem';

const SavingsList = () => {
  const savings = useSelector(savingSelectors.selectUserSavings);

  const renderedSavings = savings.map((saving) => (
    <SavingItem
      key={saving.id}
      savingId={saving.id}
      // goalName={saving.goalName}
      // amount={saving.amount}
      // goalIcon={saving.goalIcon}
    />
  ));

  return <div>{renderedSavings}</div>;
};

export default SavingsList;
