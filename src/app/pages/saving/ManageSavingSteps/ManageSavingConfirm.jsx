import styles from './ManageSavingConfirm.module.scss';

import { classNameJoin, separatorWithDot } from '../../../../services/helpers';

import { Button } from '../../../../common/components/button';
import { SavingContent } from '../SavingContent';

import Piggy from '../../../../assets/img/piggy-bank.png';

const ManageSavingConfirm = ({
  previousStep,
  amount,
  action,
  onSubmit,
  firstStep,
  onManageFinish,
}) => {
  const renderAmount = separatorWithDot(amount);

  const detailNominalClassName = classNameJoin([
    'row justify-content-between',
    styles.detailNominal,
  ]);

  const renderSubTitleText =
    action === 'topUp'
      ? 'Uang akan dipindahkan dari saldo aktif ke Monoflex'
      : 'Uang akan dipindahkan dari Monoflex ke saldo aktif';

  const renderDescriptionText =
    action === 'topUp' ? (
      <>
        Nominal <br />
        Top Up Saldo
      </>
    ) : (
      'Nominal Penarikan Saldo'
    );

  const renderButtonText = action === 'topUp' ? 'Top Up' : 'Tarik';

  const postSubmitCallback = () => {
    // history.replace('/dashboard/saving');
    onManageFinish(action);
  };

  return (
    <SavingContent>
      <SavingContent.Title onClick={previousStep}>
        <h5 className="mb-0">Kelola Tabungan</h5>
      </SavingContent.Title>
      <SavingContent.Body className={styles.detailManage}>
        <img src={Piggy} alt="monoflex" className={styles.piggy} />
        <h6 className={styles.title}>Monoflex</h6>
        <p className={styles.subTitle}>{renderSubTitleText}</p>
        <div className={detailNominalClassName}>
          <div className="col-6">
            <span className={styles.description}>{renderDescriptionText}</span>
          </div>
          <div className="col-5">
            <span className={styles.saldo}>Rp. {renderAmount}</span>
          </div>
        </div>
      </SavingContent.Body>
      <SavingContent.Footer>
        <Button.Primary
          type="button"
          onClick={() => onSubmit(postSubmitCallback)}>
          {renderButtonText}
        </Button.Primary>
      </SavingContent.Footer>
    </SavingContent>
  );
};

export default ManageSavingConfirm;
