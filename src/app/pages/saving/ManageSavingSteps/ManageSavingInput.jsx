import { useSelector } from 'react-redux';
import CurrencyInput from 'react-currency-input-field';
import { Form } from 'react-bootstrap';

import styles from './ManageSavingInput.module.scss';

import { Button } from '../../../../common/components/button';
import { SavingContent } from '../SavingContent';

import { balanceSelectors } from '../../../../features/balance';
import { savingSelectors } from '../../../../features/saving';

const ManageSavingInput = ({
  goToStep,
  previousStep,
  nextStep,
  hashKey,
  firstStep,
  amount,
  onChange,
  onClick,
  lastStep,
}) => {
  const prefix = 'Rp. ';

  const saving = useSelector(savingSelectors.selectManagedSaving);
  const balance = useSelector(balanceSelectors.selectUserBalance);

  let isValid =
    hashKey === 'topUp'
      ? !amount || amount > balance
      : !amount || amount > saving.amount;

  const handleContinueClick = (e) => {
    e.preventDefault();
    if (hashKey === 'topUp') {
      onClick('topUp');
      nextStep();
      return;
    }

    onClick('withdraw');
    lastStep();
    return;
  };

  const handleBackClick = () => {
    onChange(0);
    hashKey === 'topUp' ? previousStep() : firstStep();
  };

  const renderTitleText =
    hashKey === 'topUp' ? 'Tambah Saldo Tabungan' : 'Tarik Saldo Tabungan';

  const renderSubTitleText =
    hashKey === 'topUp'
      ? 'Tambahkan saldo tabunganmu dengan mengisi jumlah saldo dibawah'
      : 'Pindahkan uang dari tabungan kembali ke saldo aktif';

  const shouldRenderError = () => {
    let validation;
    let errorMsg = '';
    if (hashKey === 'topUp') {
      validation = amount > balance;
      errorMsg = 'Nominal melebihi jumlah saldo utama anda';
    } else if (hashKey === 'withdraw') {
      validation = amount > saving.amount;
      errorMsg = 'Nominal melebihi jumlah saldo tabungan anda';
    }

    return validation && <div className={styles.errorMessage}>{errorMsg}</div>;
  };

  return (
    <SavingContent>
      <SavingContent.Title onClick={handleBackClick}>
        <h5 className="mb-0">Kelola Tabungan</h5>
      </SavingContent.Title>
      <p className={styles.title}>{renderTitleText}</p>
      <p className={styles.subTitle}>{renderSubTitleText}</p>
      <Form>
        <Form.Group className="mb-4" controlId="saldo">
          <Form.Label className={styles.label}>Nominal Saldo</Form.Label>
          <CurrencyInput
            className={styles.inputCurrency}
            id="saldo"
            placeholder="Masukkan Nominal"
            allowNegativeValue={false}
            groupSeparator="."
            value={amount}
            prefix={prefix}
            onValueChange={onChange}
          />
          {shouldRenderError()}
        </Form.Group>
        <SavingContent.NavTabs>
          <SavingContent.NavTabsItem
            active={amount === 50000}
            onClick={() => onChange(50000)}>
            Rp. 50.000
          </SavingContent.NavTabsItem>
          <SavingContent.NavTabsItem
            active={amount === 100000}
            onClick={() => onChange(100000)}>
            Rp. 100.000
          </SavingContent.NavTabsItem>
          <SavingContent.NavTabsItem
            active={amount === saving.amount}
            onClick={() =>
              hashKey === 'topUp' ? onChange(balance) : onChange(saving.amount)
            }>
            Semua
          </SavingContent.NavTabsItem>
        </SavingContent.NavTabs>
        {/* <ButtonLanjut
          title="LANJUT"
          disabled={isValid}
          onClick={handleOnClickTopup}
        /> */}
        <Button.Primary disabled={isValid} onClick={handleContinueClick}>
          Lanjut
        </Button.Primary>
      </Form>
    </SavingContent>
  );
};

export default ManageSavingInput;
