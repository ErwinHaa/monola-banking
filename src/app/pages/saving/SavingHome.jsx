import { useState, useEffect } from 'react';
import { NavLink, Link, useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { BiPlusCircle } from 'react-icons/bi';

import styles from './SavingHome.module.scss';

import { classNameJoin } from '../../../services/helpers';

import { savingSelectors } from '../../../features/saving';
import { userSelectors, userThunks } from '../../../features/user';

import { ReactComponent as Coin } from '../../../assets/img/coin.svg';

// import { ReactComponent as Shadow } from '../../../assets/img/shadowPet.svg';
import Shadow from '../../../assets/img/shadow-cat.png';
import EditName from '../../../assets/img/edit-name.svg';
import SSPet from '../../../assets/img/ss-pet-button.svg';
import ShopPet from '../../../assets/img/shop-pet-button.svg';
import ItemPet from '../../../assets/img/item-pet-button.svg';

import ChangeNameModal from '../../../common/components/modal/ChangeNameModal';
import SavingsList from './SavingsList';
import PetLoader from '../../../common/components/Loader/PetLoader';
import CoinLoader from './../../../common/components/Loader/CoinLoader';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';

const SavingHome = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  const [modalShow, setModalShow] = useState(false);

  const savings = useSelector(savingSelectors.selectUserSavings);
  const userId = useSelector(userSelectors.selectUserId);
  const userDisplayedPet = useSelector(userSelectors.selectUserDisplayedPet);

  const fetchUserCurrentPetStatus = useSelector(
    userSelectors.selectUserCurrentPetFetchingStatus
  );

  const fetchUserStatus = useSelector(userSelectors.selectUserFetchingStatus);

  const fetchUserDisplayedPetStatus = useSelector(
    userSelectors.selectUserCurrentVariantFetchingStatus
  );

  useEffect(() => {
    fetchUserCurrentPetStatus === 'idle' &&
      !!userId &&
      dispatch(userThunks.fetchUserCurrentPet());

    fetchUserDisplayedPetStatus === 'idle' &&
      !!userId &&
      dispatch(userThunks.fetchUserCurrentVariant());
  }, [
    userId,
    fetchUserDisplayedPetStatus,
    fetchUserCurrentPetStatus,
    dispatch,
  ]);

  const onClickEditName = () => {
    setModalShow(true);
  };

  const onHideModal = () => {
    setModalShow(false);
  };

  const userCoin = useSelector(userSelectors.selectUserCoins);

  const containerPetClassName = classNameJoin(['py-5', styles.containerPet]);
  const petClassName = classNameJoin(['py-4 px-3', styles.pet]);

  const shouldRenderPet = savings.length ? (
    <>
      <div className="mb-4 d-flex flex-column">
        <img
          src={userDisplayedPet.variantImage}
          alt="User pet"
          className={styles.userPet}
        />
        {/* <Shadow className={styles.shadow} /> */}
      </div>
      <div className="my-4">
        <span className={styles.petName}>{userDisplayedPet.userPetName}</span>
        <img
          onClick={onClickEditName}
          className={styles.iconEditName}
          src={EditName}
          alt="edit-name-pet"
        />
      </div>
      <div className="d-flex">
        <OverlayTrigger
          placement="bottom-end"
          overlay={
            <Tooltip>
              Fitur Camera
              <br /> Akan Segera Hadir
            </Tooltip>
          }>
          <div>
            <img className="mr-4" src={SSPet} alt="ss-pet" />
          </div>
        </OverlayTrigger>

        <OverlayTrigger
          placement="bottom-start"
          overlay={<Tooltip>Shop</Tooltip>}>
          <NavLink to="/dashboard/saving/shop?item=pet">
            <img className="mr-4" src={ShopPet} alt="shop-pet" />
          </NavLink>
        </OverlayTrigger>

        <OverlayTrigger
          placement="bottom-start"
          overlay={<Tooltip>Edit</Tooltip>}>
          <NavLink to={`/dashboard/saving/edit?item=pet`}>
            <img src={ItemPet} alt="item-pet" />
          </NavLink>
        </OverlayTrigger>
      </div>
    </>
  ) : fetchUserDisplayedPetStatus === 'pending' ? (
    <PetLoader />
  ) : (
    <>
      {/* <Shadow className={styles.shadow} /> */}
      <img src={Shadow} alt="" />
      <span className={styles.title}>Ups!</span>
      <p className={styles.subTitle}>
        Kamu belum buka tabungan nih. Tambah tabungan baru untuk mewarnai
        suasana disini!
      </p>
    </>
  );

  const shouldRenderSaving = savings.length ? (
    // <div className="mt-4">{renderedSavings}</div>
    <SavingsList />
  ) : null;

  return (
    <section className={petClassName}>
      <div className={containerPetClassName}>
        <div className={styles.coinContainer}>
          {fetchUserStatus === 'pending' ? (
            <CoinLoader />
          ) : (
            <Coin className={styles.coin} />
          )}
          <span className={styles.coinText}>{userCoin}</span>
        </div>

        {shouldRenderPet}
      </div>
      <Link
        className={styles.button}
        to={`${location.pathname}/add-new-saving`}>
        Tambah tabungan baru
        <BiPlusCircle size="20px" className="ml-3" />
      </Link>
      {shouldRenderSaving}
      <ChangeNameModal show={modalShow} onHide={onHideModal} />
    </section>
  );
};

export default SavingHome;
