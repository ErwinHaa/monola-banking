import { useHistory } from 'react-router-dom';
import { BiLeftArrowAlt } from 'react-icons/bi';
import { useSelector } from 'react-redux';

import { userSelectors } from '../../../../features/user';

import { classNameJoin } from '../../../../services/helpers';

import styles from './SavingContentTitle.module.scss';

import Coin from '../../../../assets/img/coin.svg';

const SavingContentTitle = ({ children, showCoin, onClick }) => {
  const userCoins = useSelector(userSelectors.selectUserCoins);
  const history = useHistory();

  const handleClick = () =>
    onClick ? onClick() : history.replace('/dashboard/saving');

  const coinClassName = classNameJoin([
    styles.coinContainer,
    !showCoin && styles.hidden,
  ]);

  return (
    <header className={styles.savingTitle}>
      <div className={styles.iconContainer}>
        <BiLeftArrowAlt className={styles.iconBack} onClick={handleClick} />
      </div>
      <span className={styles.heading}>{children}</span>
      <div className={coinClassName}>
        <img className={styles.coin} src={Coin} alt="coin" />
        <span className={styles.coinText}>{userCoins}</span>
      </div>
    </header>
  );
};

export default SavingContentTitle;
