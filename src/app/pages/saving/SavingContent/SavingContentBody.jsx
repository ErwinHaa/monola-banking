import styles from './SavingContentBody.module.scss';

const SavingContentBody = ({
  children,
  className: userClassName,
  styles: userStyles,
}) => {
  const bodyClassName = userClassName ? userClassName : styles.savingBody;

  return (
    <div className={bodyClassName} styles={userStyles ? userStyles : ''}>
      {children}
    </div>
  );
};

export default SavingContentBody;
