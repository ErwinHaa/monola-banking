import SavingContent from './SavingContent';
import SavingContentBody from './SavingContentBody';
import SavingContentGroup from './SavingContentGroup';
import SavingContentItem from './SavingContentItem';
import SavingContentNavTabs from './SavingContentNavTabs';
import SavingContentNavTabsItem from './SavingContentNavTabsItem';
import SavingContentTitle from './SavingContentTitle';
import SavingContentFooter from './SavingContentFooter';

SavingContent.Body = SavingContentBody;
SavingContent.Group = SavingContentGroup;
SavingContent.Item = SavingContentItem;
SavingContent.NavTabs = SavingContentNavTabs;
SavingContent.NavTabsItem = SavingContentNavTabsItem;
SavingContent.Title = SavingContentTitle;
SavingContent.Footer = SavingContentFooter;

export { SavingContent };
