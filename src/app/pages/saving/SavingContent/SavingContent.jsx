import styles from './SavingContent.module.scss';

const SavingContent = ({
  children,
  className: userClassName,
  styles: userStyles,
}) => {
  const contentClassName = userClassName ? userClassName : styles.savingContent;

  return (
    <div className={contentClassName} styles={userStyles ? userStyles : {}}>
      {children}
    </div>
  );
};

export default SavingContent;
