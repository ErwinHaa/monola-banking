const SavingContentNavTabs = ({ children }) => {
  return <nav className="row my-4">{children}</nav>;
};

export default SavingContentNavTabs;
