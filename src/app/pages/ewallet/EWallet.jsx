import { useState } from 'react';
import { Route, useRouteMatch, Link, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { eWalletSelectors } from '../../../features/ewallet';

import styles from './EWallet.module.scss';
import { ReactComponent as Add } from '../../../assets/img/Add.svg';
import { ReactComponent as Edit } from '../../../assets/img/Edit.svg';

import { Breadcrumb } from '../../../common/components/BreadCrumb';
import { DashboardLayout } from '../../../common/components/DashboardLayout';
import AddNewEWallet from './AddNewEWallet';
import EWalletSteps from './EWalletSteps';
import EWalletSearch from './EWalletSearch';
import EWalletDetail from './EWalletDetail';
import TransactionFailed from '../../../common/components/transactionfailed/TransactionFailed';

const EWallet = ({ userName, userAccNum, userBalance, onCoinReceived }) => {
  const recipient = useSelector(eWalletSelectors.selectEWalletRecipient);
  const [bankSelect, setBankSelect] = useState('');
  const [editMode, setEditMode] = useState(false);

  const handleToggleEdit = () => {
    setEditMode(true);
  };

  const match = useRouteMatch();
  const location = useLocation();

  const handleBankSelect = (e) => {
    setBankSelect(e.target.value);
  };

  const shouldRenderAddBtn =
    location.pathname === '/dashboard/ewallet/top-up-ewallet' ? (
      <Link to={`${match.url}/add-new-account`}>
        <Add className={styles.addIcon} />
      </Link>
    ) : location.pathname === '/dashboard/ewallet/detail-ewallet' &&
      !editMode ? (
      <Edit className={styles.addIcon} onClick={handleToggleEdit} />
    ) : null;

  return (
    <DashboardLayout>
      <DashboardLayout.Head>
        <h3 className="dashboard-heading">
          {location.pathname.includes('detail-ewallet')
            ? 'Rincian'
            : 'Top Up e-Wallet'}
        </h3>
        {shouldRenderAddBtn}
      </DashboardLayout.Head>
      <Breadcrumb>
        <Breadcrumb.Item to="/dashboard/transaksi">Transaksi</Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item isActive>Top Up e-Wallet</Breadcrumb.Item>
      </Breadcrumb>
      <Route exact path={`${match.path}/top-up-ewallet`}>
        <EWalletSearch recipient={recipient} />
      </Route>
      <Route exact path={`/dashboard/ewallet/top-up-ewallet/fill-information`}>
        <EWalletSteps
          recipient={recipient}
          userName={userName}
          userAccNum={userAccNum}
          userBalance={userBalance}
          onCoinReceived={onCoinReceived}
        />
      </Route>
      <Route path="/dashboard/ewallet/detail-ewallet">
        <EWalletDetail editMode={editMode} />
      </Route>
      <Route exact path={`${match.path}/add-new-account`}>
        <AddNewEWallet bankSelect={bankSelect} onChange={handleBankSelect} />
      </Route>
      <Route exact path={`${match.path}/top-up-ewallet/top-up-failed`}>
        <TransactionFailed />
      </Route>
    </DashboardLayout>
  );
};

export default EWallet;
