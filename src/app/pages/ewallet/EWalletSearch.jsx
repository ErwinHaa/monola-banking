import { useState, useEffect, useContext } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Form, InputGroup, FormControl } from 'react-bootstrap';
import { AiOutlineSearch } from 'react-icons/ai';
import { useHistory, useRouteMatch } from 'react-router-dom';

import { selectorContext } from '../../context';

import styles from './EWalletSearch.module.scss';

import {
  eWalletSelectors,
  eWalletActions,
  eWalletThunks,
} from '../../../features/ewallet';
import { userSelectors } from '../../../features/user';

import TransactionGroup from '../../../common/components/transactionGroup/TransactionGroup';
// import CustomAlert from '../../../common/components/alert/CustomAlert';
import TransactionsLoader from '../../../common/components/Loader/TransactionsLoader';
import { EmptyFavorites } from '../../../common/components/EmptyFavorites';
import EmptyContent from './../../../common/components/EmptyContent/EmptyContent';

const EWalletSearch = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const match = useRouteMatch();

  const { setSelector } = useContext(selectorContext);

  const [accountInput, setAccountInput] = useState('');

  const favoriteEWallets = useSelector((state) =>
    eWalletSelectors.selectFilteredFavoriteEwallets(state, accountInput)
  );
  const userId = useSelector(userSelectors.selectUserId);

  const eWalletFetchStatus = useSelector(
    eWalletSelectors.selectEWalletFetchStatus
  );

  useEffect(() => {
    dispatch(eWalletActions.resetEWalletRecipient());
    eWalletFetchStatus === 'idle' &&
      !!userId &&
      dispatch(eWalletThunks.fetchUserFavoriteEWallet());
  }, [eWalletFetchStatus, dispatch, userId]);

  const handleAccountInput = (e) => {
    setAccountInput(e.target.value);
  };

  const handleItemClick = (ewalletPhoneNumber, ewalletProvider) => {
    dispatch(
      eWalletThunks.fetchEWalletByPhoneNumber(
        ewalletPhoneNumber,
        ewalletProvider
      )
    );

    setSelector(() => eWalletSelectors.selectEWalletRecipient);

    history.push(
      `${match.path}/fill-information?phoneNumber=${ewalletPhoneNumber}&provider=${ewalletProvider}`
    );
  };

  const renderTransactionItem =
    !favoriteEWallets.length && !!accountInput ? (
      <>
        {/* <h6 className="font-weight-bold">Hasil Pencarian</h6> */}
        {/* <CustomAlert>
          e-Wallet tidak ditemukan. Mohon periksa kembali nama e-Wallet.
        </CustomAlert> */}
        <EmptyContent />
      </>
    ) : (
      <>
        <h6 className="font-weight-bold my-4">
          {!accountInput ? 'e-Wallet Favorit' : 'Hasil Pencarian'}
        </h6>
        {!!favoriteEWallets.length ? (
          <TransactionGroup
            items={favoriteEWallets}
            onClick={handleItemClick}
            selector={eWalletSelectors.selectFavoriteEWalletById}
          />
        ) : eWalletFetchStatus === 'pending' ? (
          <TransactionsLoader />
        ) : (
          <EmptyFavorites>
            <EmptyFavorites.Head>
              eWallet Favorit Masih Kosong
            </EmptyFavorites.Head>
            <EmptyFavorites.Paragraph>
              Lakukan top up baru dan simpan akun penerima agar akun yang
              difavoritkan bisa muncul disini.
            </EmptyFavorites.Paragraph>
          </EmptyFavorites>
        )}
      </>
    );

  return (
    <Form>
      <InputGroup className="mb-3">
        <FormControl
          placeholder="Cari nama e-Wallet"
          style={{ borderRight: 'none' }}
          onChange={handleAccountInput}
          value={accountInput}
          className={styles.searchInput}
        />
        <InputGroup.Append className={styles.append}>
          <InputGroup.Text className={styles.inputGroupText}>
            <AiOutlineSearch />
          </InputGroup.Text>
        </InputGroup.Append>
      </InputGroup>

      {renderTransactionItem}
    </Form>
  );
};

export default EWalletSearch;
