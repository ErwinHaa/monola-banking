import { useContext, useEffect, useState } from 'react';
import { Form } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import CurrencyInput from 'react-currency-input-field';

import { selectorContext } from '../../context';

import {
  separatorWithDot,
  creditCardNumberFormat,
  checkObjectIsNotEmpty,
} from '../../../services/helpers';

import {
  eWalletSelectors,
  eWalletActions,
  eWalletThunks,
} from '../../../features/ewallet';

import { useQuery } from './../../../common/hooks';

import TransactionItem from '../../../common/components/transactionGroup/TransactionItem';
import UserCard from '../../../common/components/userCard/UserCard';
import ButtonNext from '../../../common/components/button/ButtonNext';
import ButtonPrevious from '../../../common/components/button/ButtonPrevious';
import CustomAlert from '../../../common/components/alert/CustomAlert';
import TransactionItemLoader from '../../../common/components/Loader/TransactionItemLoader';

const FillInformation = ({
  nextStep,
  onNominalChange,
  onDescriptionChange,
  nominal,
  description,
  userName,
  userAccNum,
  userBalance,
}) => {
  const { setSelector } = useContext(selectorContext);

  const history = useHistory();
  const dispatch = useDispatch();

  const [isTouched, setIsTouched] = useState(false);

  const recipientPhoneNumber = useQuery().get('phoneNumber');
  const recipientProvider = useQuery().get('provider');

  const recipient = useSelector(eWalletSelectors.selectEWalletRecipient);

  const eWalletRecipientFetchStatus = useSelector(
    eWalletSelectors.selectEWalletRecipientFetchStatus
  );

  const isValid = nominal >= 10000 && nominal < userBalance;

  useEffect(() => {
    eWalletRecipientFetchStatus === 'idle' &&
      dispatch(
        eWalletThunks.fetchEWalletByPhoneNumber(
          recipientPhoneNumber,
          recipientProvider
        )
      );
  }, [
    dispatch,
    eWalletRecipientFetchStatus,
    recipientPhoneNumber,
    recipientProvider,
  ]);

  const handlePreviousClick = () => {
    history.push('/dashboard/transaksi');
  };

  const handleBalanceCheck = () => {
    dispatch(
      eWalletActions.saveTopUpInfo({
        topUpAmount: nominal,
        topUpDescription: description,
      })
    );

    setSelector(() => eWalletSelectors.selectEWalletRecipient);
    nextStep();
  };

  let errorText = '';

  if (nominal > userBalance) {
    errorText =
      'Nominal transfer yang anda masukkan melebihi jumlah saldo yang anda miliki!';
  } else if (nominal < 10000) {
    errorText =
      'Nominal top up yang anda masukkan tidak boleh kurang dari Rp 10.000';
  }

  const shouldShowAlert = !!errorText && isTouched && (
    <CustomAlert>{errorText}</CustomAlert>
  );

  const shouldShowMinimalInput =
    nominal < 10000 && isTouched && nominal > 0 ? (
      <Form.Text className="text-danger">Minimal transaksi Rp 10.000</Form.Text>
    ) : null;

  return (
    <>
      <UserCard small>
        <div className="row">
          <div className="col-6">
            <p className="text text--thin mb-0">Saldo Aktif</p>
            <p className="text text--bold mb-0">
              Rp. {separatorWithDot(userBalance)}
            </p>
          </div>
          <div className="col-6 d-flex align-items-end flex-column justify-content-center">
            <p className="text text--bold mb-0">{userName}</p>
            <p className="text text--thin mb-0">
              {creditCardNumberFormat(userAccNum)}
            </p>
          </div>
        </div>
      </UserCard>
      <h6 className="font-weight-bold my-4">Informasi Transaksi</h6>
      {eWalletRecipientFetchStatus === 'fulfilled' &&
      checkObjectIsNotEmpty(recipient) ? (
        <TransactionItem selector={eWalletSelectors.selectEWalletRecipient} />
      ) : (
        <TransactionItemLoader />
      )}
      {/* Alert here */}
      {shouldShowAlert}
      <Form>
        <Form.Group>
          <Form.Label className="fw-600">Nominal</Form.Label>
          <CurrencyInput
            placeholder="Masukkan Nominal"
            onValueChange={onNominalChange}
            groupSeparator="."
            value={nominal}
            className="form-control"
            prefix="Rp."
            onFocus={() => setIsTouched(true)}
          />
          {shouldShowMinimalInput}
        </Form.Group>
        <Form.Group>
          <Form.Label className="fw-600">Deskripsi (opsional)</Form.Label>
          <Form.Control
            placeholder="Masukkan Deskripsi"
            onChange={onDescriptionChange}
            value={description}
          />
        </Form.Group>
        <Form.Group className="py-4">
          <div className="row justify-content-between">
            <div className="col-6">
              <ButtonPrevious onClick={handlePreviousClick}>
                Batal
              </ButtonPrevious>
            </div>
            <div className="col-6">
              <ButtonNext
                type="button"
                onClick={handleBalanceCheck}
                disabled={!isValid}>
                Lanjut
              </ButtonNext>
            </div>
          </div>
        </Form.Group>
      </Form>
    </>
  );
};

export default FillInformation;
