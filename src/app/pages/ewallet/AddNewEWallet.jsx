import { useState, useEffect } from 'react';
import { Form } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { checkObjectIsNotEmpty } from '../../../services/helpers';

import {
  eWalletActions,
  eWalletThunks,
  eWalletSelectors,
} from '../../../features/ewallet';

import styles from './AddNewEWallet.module.scss';

import TransactionItem from '../../../common/components/transactionGroup/TransactionItem';
import ButtonNext from '../../../common/components/button/ButtonNext';
import ButtonPrevious from '../../../common/components/button/ButtonPrevious';
import CustomAlert from '../../../common/components/alert/CustomAlert';
import TransactionItemLoader from '../../../common/components/Loader/TransactionItemLoader';

const AddNewEWallet = () => {
  const [provider, setProvider] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');

  const [isFavorited, setIsFavorited] = useState(false);
  const [eWalletName, setEWalletName] = useState('');
  const [charLength, setCharLength] = useState(15);
  const [doesExists, setDoesExists] = useState(false);

  const handleFavoriteCheck = () => {
    setIsFavorited((prevState) => !prevState);
  };

  const handleEWalletNameInput = (e) => {
    setEWalletName(e.target.value);
    setCharLength(15 - e.target.value.length);
  };

  const history = useHistory();

  const recipient = useSelector(eWalletSelectors.selectEWalletRecipient);
  const searchError = useSelector(eWalletSelectors.selectEWalletSearchError);

  const favoriteEwallets = useSelector(
    eWalletSelectors.selectUniqueFavoriteEwallets
  );

  const recipientSearchStatus = useSelector(
    eWalletSelectors.selectEWalletSearchStatus
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(eWalletActions.resetEWalletRecipient());
    dispatch(eWalletActions.resetEWalletRecipientSearchStatus());
  }, [dispatch]);

  let isValid = false;

  const handleBankSelect = (e) => {
    setProvider(e.target.value);
  };

  const handleRekeningInput = (e) => {
    setPhoneNumber(e.target.value);
  };

  const handleSearchSubmit = (e) => {
    e.preventDefault();
    favoriteEwallets.forEach((ewallet) => {
      if (ewallet.identityNumber.indexOf(phoneNumber) !== -1) {
        setDoesExists(true);
      }
    });

    dispatch(eWalletThunks.fetchEWalletByPhoneNumber(phoneNumber, provider));
  };

  const handlePreviousClick = () => {
    history.goBack();
  };

  const handleNextClick = () => {
    if (isFavorited) {
      dispatch(eWalletThunks.addEWalletToFavorite(eWalletName));
    }
    history.push('/dashboard/ewallet/top-up-ewallet/fill-information');
  };

  if (!!provider && !!phoneNumber) {
    isValid = true;
  }

  return (
    <>
      <h5 className="fw-600 my-4">Informasi Akun e-Wallet</h5>
      <Form onSubmit={handleSearchSubmit}>
        <Form.Group>
          <Form.Label className="fw-600">e-Wallet</Form.Label>
          <Form.Control
            as="select"
            onChange={handleBankSelect}
            value={provider}
            className="px-3"
            disabled={recipientSearchStatus === 'fulfilled'}>
            <option>Pilih e-Wallet</option>
            <option value="GOPAY">Gopay</option>
            <option value="OVO">OVO</option>
            <option value="DANA">DANA</option>
            <option value="LINKAJA">Link Aja</option>
          </Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label className="fw-600">Nomor Handphone</Form.Label>
          <Form.Control
            placeholder="Masukkan Nomor Handphone"
            value={phoneNumber}
            onChange={handleRekeningInput}
            disabled={recipientSearchStatus === 'fulfilled'}
          />
        </Form.Group>
        {checkObjectIsNotEmpty(recipient) && !searchError ? (
          <>
            <h5>Hasil Pencarian</h5>
            <TransactionItem
              selector={eWalletSelectors.selectEWalletRecipient}
            />
            <Form.Group>
              <Form.Check
                type="checkbox"
                label="Masukkan akun e-Wallet ke daftar favorit"
                onChange={handleFavoriteCheck}
                checked={isFavorited}
                disabled={doesExists}
                className={styles.checkboxContainer}
              />
              {!!doesExists && (
                <Form.Text>Akun e-Wallet sudah ada di favorit.</Form.Text>
              )}
            </Form.Group>
          </>
        ) : !!searchError ? (
          <>
            <h6 className="font-weight-bold">Hasil Pencarian</h6>
            <CustomAlert>
              Nomor handphone tidak ditemukan. Mohon periksa kembali nomor
              handphone.
            </CustomAlert>
          </>
        ) : recipientSearchStatus === 'pending' ? (
          <TransactionItemLoader />
        ) : null}
        {!!isFavorited && (
          <Form.Group className={styles.nameInputGroup}>
            <Form.Label className="fw-600">Nama e-Wallet</Form.Label>
            <Form.Row className={styles.nameInputRow}>
              <Form.Control
                type="text"
                placeholder="Masukkan nama e-Wallet"
                value={eWalletName}
                onChange={handleEWalletNameInput}
                maxLength="15"
                className={styles.nameInputControl}
              />
              <span className={styles.charLength}>{charLength}</span>
            </Form.Row>
            <Form.Text className="text-muted">
              Nama e-Wallet akan otomatis diperbarui
            </Form.Text>
          </Form.Group>
        )}

        <Form.Group className="py-4">
          <div className="row justify-content-between">
            <div className="col-6">
              <ButtonPrevious onClick={handlePreviousClick}>
                Batal
              </ButtonPrevious>
            </div>
            <div className="col-6">
              {checkObjectIsNotEmpty(recipient) ? (
                <ButtonNext type="button" onClick={handleNextClick}>
                  Lanjut
                </ButtonNext>
              ) : (
                <ButtonNext type="submit" disabled={!isValid}>
                  Cari
                </ButtonNext>
              )}
            </div>
          </div>
        </Form.Group>
      </Form>
    </>
  );
};

export default AddNewEWallet;
