import { useState, useEffect } from 'react';
import { Form, Spinner } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { eWalletThunks, eWalletSelectors } from '../../../features/ewallet';

import { checkObjectIsNotEmpty } from '../../../services/helpers';

import GOPAY from '../../../assets/img/Gopay.png';
import DANA from '../../../assets/img/DANA.png';
import LinkAja from '../../../assets/img/Linkaja.png';
import OVO from '../../../assets/img/OVO.png';

import styles from './EWalletDetail.module.scss';

import ButtonPrevious from '../../../common/components/button/ButtonPrevious';
import ButtonNext from '../../../common/components/button/ButtonNext';
import useQuery from './../../../common/hooks/useQuery';

const EWalletDetail = ({ editMode }) => {
  const vendorImages = {
    GOPAY,
    DANA,
    LinkAja,
    OVO,
  };
  const history = useHistory();
  const dispatch = useDispatch();
  const ewalletName = useQuery().get('ewalletName');

  useEffect(() => {
    dispatch(eWalletThunks.fetchFavoriteEWalletByName(ewalletName));
  }, [dispatch, ewalletName]);

  const selectedEWallet = useSelector(eWalletSelectors.selectEditingEWallet);

  const editingName = checkObjectIsNotEmpty(selectedEWallet)
    ? selectedEWallet.userName
    : ewalletName;

  const [newEwalletName, setNewEwalletName] = useState(editingName);

  const [charLength, setCharLength] = useState(15 - ewalletName.length);

  const handleNameChange = (e) => {
    setNewEwalletName(e.target.value);
    setCharLength(15 - e.target.value.length);
  };

  const handlePreviousClick = () => {
    history.goBack();
  };

  const handleSaveNewEWalletName = () => {
    dispatch(
      eWalletThunks.changeFavoriteEWalletName(
        {
          phoneNumber: selectedEWallet.identityNumber,
          provider: selectedEWallet.provider,
          username: newEwalletName,
        },
        selectedEWallet.id
      )
    );

    history.push('/dashboard');
  };

  const handleTopUpClick = () => {
    // dispatch(
    //   eWalletActions.setEWalletRecipient({
    //     ewalletPaymentName: selectedEWallet.userName,
    //     ewalletPaymentPhoneNumber: selectedEWallet.identityNumber,
    //     ewalletPaymentProvider: selectedEWallet.provider,
    //   })
    // );

    dispatch(
      eWalletThunks.fetchEWalletByPhoneNumber(
        selectedEWallet.identityNumber,
        selectedEWallet.provider
      )
    );

    history.push('/dashboard/ewallet/top-up-ewallet/fill-information');
  };

  const shouldRenderDetailPage = checkObjectIsNotEmpty(selectedEWallet) ? (
    <>
      <div className={styles.imgContainer}>
        <img
          src={vendorImages[selectedEWallet.provider]}
          alt=""
          className={styles.providerImg}
        />
      </div>
      <Form>
        <Form.Group>
          <Form.Label className="fw-600">
            Nama e-Wallet yang tersimpan
          </Form.Label>
          <Form.Row className="d-flex align-items-center">
            <Form.Control
              type="text"
              readOnly={!editMode}
              value={newEwalletName}
              onChange={handleNameChange}
              className={styles.nameInput}
            />
            {editMode && <Form.Text>{charLength}</Form.Text>}
          </Form.Row>
        </Form.Group>
        <Form.Group className="py-4">
          <div className="row justify-content-between">
            <div className="col-6">
              <ButtonPrevious onClick={handlePreviousClick}>
                Kembali
              </ButtonPrevious>
            </div>
            <div className="col-6">
              {editMode ? (
                <ButtonNext type="button" onClick={handleSaveNewEWalletName}>
                  Simpan
                </ButtonNext>
              ) : (
                <ButtonNext onClick={handleTopUpClick}>Top Up</ButtonNext>
              )}
            </div>
          </div>
        </Form.Group>
      </Form>
    </>
  ) : (
    <Spinner animation="border" />
  );
  return <div className="">{shouldRenderDetailPage}</div>;
};

export default EWalletDetail;
