import { Button } from 'react-bootstrap';

import { ReactComponent as PinIcon } from '../../../assets/img/pinicon.svg';

import styles from './ForgotPin.module.scss';

const ForgotPin = ({ previousStep }) => {
  return (
    <div className={styles.forgotpin}>
      <PinIcon className={styles.icon} />
      <p className={styles.title}>Lupa PIN</p>
      <p className={styles.subTitle}>
        Jika kamu lupa PIN, kamu dapat mengatur ulang PIN melalui aplikasi
        Monola. Berikut ini langkah-langkahnya:
      </p>
      <ol className={styles.orderedList}>
        <li>
          Pilih Menu <span className={styles.text600}>Profil</span>
        </li>
        <li>
          Pilih <span className={styles.text600}>Pengaturan</span>
        </li>
        <li>
          Lalu pilih <span className={styles.text600}>Ubah PIN</span>
        </li>
        <li>Masukkan User ID dan Nomor Kartu</li>
        <li>Masukan 6 digit kode OTP yang telah dikirim ke email</li>
        <li>Buat PIN baru</li>
      </ol>
      <Button className={styles.kembaliBtn} onClick={previousStep}>
        KEMBALI
      </Button>
    </div>
  );
};

export default ForgotPin;
