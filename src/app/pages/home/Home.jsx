import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Modal } from 'react-bootstrap';

import Coin from '../../../assets/img/koin-carousel3.png';

import { transferSelectors, transferThunks } from '../../../features/transfer';
import { userSelectors } from '../../../features/user';
import { eWalletSelectors } from '../../../features/ewallet';

import {
  separatorWithDot,
  creditCardNumberFormat,
} from '../../../services/helpers';

import TitleHeader from '../../../common/components/titleHeader/TitleHeader';
import UserCard from '../../../common/components/userCard/UserCard';
import Option from '../../../common/components/option/Option';
import ButtonTransfer from '../../../common/components/button/ButtonTransfer';
import TransactionGroup from '../../../common/components/transactionGroup/TransactionGroup';
import EWalletsList from '../../../common/components/ewalletslist/EWalletsList';
import PopUp from '../../../common/components/popup/PopUp';
import CardLoader from './../../../common/components/Loader/CardLoader';
import EWalletLoader from './../../../common/components/Loader/EWalletLoader';
import TransactionsLoader from '../../../common/components/Loader/TransactionsLoader';
import { EmptyFavorites } from '../../../common/components/EmptyFavorites';
import { Button } from '../../../common/components/button';

import styles from './Home.module.scss';

const Home = React.memo(
  ({ userName, userAccNum, userBalance, showCoinModal, onHide }) => {
    const [isHidden, setIsHidden] = useState(false);

    const history = useHistory();

    const dispatch = useDispatch();

    const favoriteTransactions = useSelector(
      transferSelectors.selectFavoriteTransactions
    );

    const userFetchStatus = useSelector(userSelectors.selectUserFetchingStatus);
    const favoriteEwalletFetchStatus = useSelector(
      eWalletSelectors.selectEWalletFetchStatus
    );

    useEffect(() => {
      return () => {
        if (history.action === 'POP' || history.action === 'REPLACE')
          history.replace('/dashboard');
      };
    }, [history, history.action]);

    const handleHiddenPopUpClick = () => {
      setIsHidden(true);
    };

    const handleItemClick = (recipientId) => {
      dispatch(transferThunks.fetchRecipientById(recipientId));

      history.push(
        `/dashboard/transaksi/transfer/fill-information?recipientId=${recipientId}`
      );
    };

    const nickName = userName ? userName.slice(0, userName.indexOf(' ')) : '';

    return (
      <>
        <div className="mb-3">
          <TitleHeader userName={nickName} className={styles.titleHeader} />
          <UserCard>
            {userFetchStatus === 'pending' ? (
              <CardLoader />
            ) : (
              <>
                <h3 className={styles.name}>{userName}</h3>
                <h4 className={styles.rekening}>
                  {creditCardNumberFormat(userAccNum)}
                </h4>
                <h4 className={styles.saldoTitle}>Saldo Aktif</h4>
                <h4 className={styles.saldo}>
                  Rp. {separatorWithDot(userBalance)}
                </h4>
                <ButtonTransfer />
              </>
            )}
          </UserCard>
          <Option />
          <PopUp isHidden={isHidden} onClick={handleHiddenPopUpClick} />
          {favoriteEwalletFetchStatus === 'pending' ? (
            <EWalletLoader />
          ) : (
            <EWalletsList />
          )}

          <h6 className="font-weight-bold">Transaksi Favorit</h6>
          {!!favoriteTransactions.length ? (
            <TransactionGroup
              items={favoriteTransactions}
              onClick={handleItemClick}
              selector={transferSelectors.selectFavoriteTransactionById}
            />
          ) : favoriteEwalletFetchStatus === 'pending' ? (
            <TransactionsLoader />
          ) : (
            <EmptyFavorites>
              <EmptyFavorites.Head>
                Transaksi Favorit Masih Kosong
              </EmptyFavorites.Head>
              <EmptyFavorites.Paragraph>
                Buat transaksi baru dan pilih simpan transaksi yang sudah
                dilakukan agar akun yang difavoritkan bisa muncul disini.
              </EmptyFavorites.Paragraph>
            </EmptyFavorites>
          )}
        </div>
        <Modal show={showCoinModal} onHide={onHide} className="coin-modal">
          <Modal.Body className="d-flex flex-column align-items-center p-5">
            <div className="">
              <img src={Coin} alt="" />
            </div>
            <div className="py-4 text-center">
              <p>
                Yay! Kamu mendapatkan koin sebesar <b>20 koin</b> dari hasil
                transaksimu. Koin ini bisa ditukar untuk mendapatkan pet, skin,
                atau rumah baru yang tersedia di fitur Saving.
              </p>
            </div>
            <Button.Primary onClick={onHide}>Oke, lanjut</Button.Primary>
          </Modal.Body>
        </Modal>
      </>
    );
  }
);

export default Home;
