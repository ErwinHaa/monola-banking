import { Button } from 'react-bootstrap';

import styles from './LandingPage.module.scss';

import Carousel from '../../../common/components/carousel/Carousel';

import { classNameJoin } from '../../../services/helpers';

import CarouselBg from '../../../assets/img/background-carousel-all.png';
import CarouselContentImg1 from '../../../assets/img/mockup-carousel1.png';
import CarouselContentImg2 from '../../../assets/img/pets-carousel2.png';
import CarouselContentImg3 from '../../../assets/img/koin-carousel3.png';
import imgSection1 from '../../../assets/img/mockup-1.png';
import imgSection2 from '../../../assets/img/mockup-2.png';
import imgSection3 from '../../../assets/img/mockup-3.png';
import Chonky from '../../../assets/img/chonkybg.png'; 
import Kevin from '../../../assets/img/kevinbg.png'; 
import Terry from '../../../assets/img/terrybg.png'; 
import Som from '../../../assets/img/sombg.png'; 

const LandingPage = () => {
  const carouselConfig = [
    {
      id: 1,
      title: 'BIKIN MUDAH DAN SERU!',
      subTitle:
        'Dengan menabung di Monola. Buka rekeningnya sekarang tanpa perlu ke bank.',
      bgImg: CarouselBg,
      contentImg: CarouselContentImg1,
    },
    {
      id: 2,
      title: 'Monopet',
      subTitle:
        'Dimana lagi bisa nabung sambil merawat Pet? Isi tabunganmu agar bisa ditemani pet yang bisa menemani kegiatanmu.',
      bgImg: CarouselBg,
      contentImg: CarouselContentImg2,
    },
    {
      id: 3,
      title: 'Kumpulkan koin, dapatkan skin',
      subTitle:
        'Tidak hanya pet saja, koin yang didapatkan saat kamu bertransaksi bisa dihabiskan untuk membeli item yang disediakan di Monola.',
      bgImg: CarouselBg,
      contentImg: CarouselContentImg3,
    },
  ];

  const onClickDaftar = (e) => {
    e.preventDefault();
    window.open('https://play.google.com', '_blank').focus();
  }

  const textContainerClassName = classNameJoin([
    'col-sm-12 col-md-8 col-lg-9 col-xl-7 px-lg-5 pl-md-5',
    styles.textContainer,
  ]);

  const textContainerMidClassName = classNameJoin([
    'col-md-8 col-lg-9 col-xl-7 px-lg-5 px-md-0 order-sm-1 order-md-0 u-order-xs-1',
    styles.textContainer,
  ]);

  const sectionImgContainerClassName = classNameJoin([
    'col-sm-12 col-md-4 col-lg-3 col-xl-5 text-center',
    styles.sectionImgContainer,
  ]);

  const carouselIndicatorsContainerClassName = classNameJoin([
    'carousel-indicators mb-5',
    styles.carouselIndicatorsContainer,
  ]);

  const renderedCarouselItems = carouselConfig.map(({ id, ...restProps }) => (
    <Carousel key={id} id={id} onClick={onClickDaftar} className={id === 1 ? 'active' : ''} {...restProps} />
  ));

  

  return (
    <>
      {/* Carousel */}
      <div
        id="carouselExampleCaptions"
        className="carousel slide u-margin-top-large u-margin-top-l-medium u-margin-top-md-large u-margin-sm-none"
        data-ride="carousel">
        <ol className={carouselIndicatorsContainerClassName}>
          <li
            data-target="#carouselExampleCaptions"
            data-slide-to="0"
            className={`${styles.carouselIndicator} active`}></li>
          <li
            data-target="#carouselExampleCaptions"
            data-slide-to="1"
            className={styles.carouselIndicator}></li>
          <li
            data-target="#carouselExampleCaptions"
            data-slide-to="2"
            className={styles.carouselIndicator}></li>
        </ol>
        <div className="carousel-inner">{renderedCarouselItems}</div>
      </div>
      {/* end Carousel */}

      <main className={styles.mainContent}>
        <section className={styles.mainContentSection1}>
          <div className="container h-100">
            <div
              className="row 
            justify-content-center align-items-center h-100
            ">
              <div className={sectionImgContainerClassName}>
                <img
                  src={imgSection1}
                  alt="Profit"
                  className={styles.sectionImg}
                />
              </div>
              <div className={textContainerClassName}>
                <h4 className={styles.title}>Nabung Mudah, Bunga berlimpah</h4>
                <p className={styles.subTitle}>
                Dapatkan bunga 2,5% dengan menabung<br/>berapapun kamu mau di Monoflex dan Monosave.
                </p>
              </div>
            </div>
          </div>
        </section>

        <section className={styles.mainContentSection2}>
          <div className="container h-100">
            <div className="row justify-content-center align-items-center h-100 text-right">
              <div className={textContainerMidClassName}>
                <h4 className={styles.title}>
                  Kelola Keuanganmu Hari Ini dan Nanti
                </h4>
                <p className={styles.subTitle}>
                  Monola hadir memberikan kemudahan untuk mengelola keuangan
                  hari ini dan nanti lewat genggamanmu.
                </p>
              </div>
              <div className={sectionImgContainerClassName}>
                <img
                  src={imgSection2}
                  alt="Manage"
                  className={styles.sectionImg}
                />
              </div>
            </div>
          </div>
        </section>

        <section className={styles.mainContentSection3}>
          <div className="container h-100">
            <div className="row justify-content-center align-items-center h-100">
              <div className={sectionImgContainerClassName}>
                <img
                  src={imgSection3}
                  alt="Achieve"
                  className={styles.sectionImg}
                />
              </div>
              <div className={textContainerClassName}>
                <h4 className={styles.title}>Wujudkan Mimpi Bersama Pet-mu</h4>
                <p className={styles.subTitle}>
                  Bertransaksi atau menabung untuk wujudkan mimpimu sambil
                  merawat Pet-mu. Pengalaman menabung menjadi lebih
                  menyenangkan!
                </p>
              </div>
            </div>
          </div>
        </section>

        <div className={styles.mainContentSection4}>
          <div className="d-flex flex-column align-items-center justify-content-evenly h-50 u-z-index-high">
            <div>
              <h1 className={styles.titleRegis}>Jadikan Monola Milikmu</h1>
              <p className={styles.subTitleRegis}>
                Daftar sekarang dan dapatkan kemudahan untuk lakukan aktivitas
                banking-mu.
              </p>
            </div>
            <Button className={styles.btnRegis} onClick={onClickDaftar}>Daftar Sekarang</Button>
            <img src={Chonky} className={styles.chonkyBg} alt="" />
            <img src={Kevin} className={styles.kevinBg} alt="" />
            <img src={Terry} className={styles.terryBg} alt="" />
            <img src={Som} className={styles.somBg} alt="" />
          </div>
        </div>
      </main>
    </>
  );
};

export default LandingPage;
