import { customRender } from '../../../services/test/test-utils';

import LandingPage from './LandingPage';

describe('<LandingPage />', () => {
  test('if LandingPage rendered without crashing', () => {
    customRender(<LandingPage />);
  });
});
