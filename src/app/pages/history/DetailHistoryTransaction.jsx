import { useContext } from 'react';

import { classNameJoin, separatorWithDot } from '../../../services/helpers';
import { useQuery } from './../../../common/hooks';

import { historyContext } from '../../context';

import styles from './DetailHistoryTransaction.module.scss';

const DetailHistoryTransaction = () => {
  // const { itemId } = useParams();
  const itemId = useQuery().get('id');

  const { historyState } = useContext(historyContext);

  const columnHeaderClassName = classNameJoin([
    'col-12 text-center',
    styles.column,
  ]);
  const columnItemsClassName = classNameJoin([
    'col-12 d-flex justify-content-between',
    styles.column,
  ]);

  const {
    userName,
    bankName,
    accountNumber,
    amount,
    dateDetail,
    category,
    // itemId,
  } = historyState;

  const amountSeparated = separatorWithDot(amount);
  return (
    <div className="row justify-content-center m-0">
      <div className={columnHeaderClassName}>
        <p className="m-0">{userName}</p>
        <span className="mr-2">{bankName}</span>
        <span>{accountNumber}</span>
      </div>
      <div className={columnItemsClassName}>
        <span>
          {category === 'OUT'
            ? 'Kamu mengeluarkan'
            : category === 'IN'
            ? 'Kamu Menerima'
            : ''}
        </span>
        <span className={styles.amount}>Rp. {amountSeparated}</span>
      </div>
      <div className={columnItemsClassName}>
        <span>Tanggal transaksi</span>
        <span>{dateDetail}</span>
      </div>
      <div className={columnItemsClassName}>
        <span>ID transaksi</span>
        <span>{itemId}</span>
      </div>
      <div className={columnItemsClassName}>
        <span>Kategori</span>
        <span>
          {category === 'OUT'
            ? 'Tarik Tunai'
            : category === 'IN'
            ? 'Uang Masuk'
            : ''}
        </span>
      </div>
    </div>
  );
};

export default DetailHistoryTransaction;
