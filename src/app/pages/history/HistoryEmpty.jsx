import EmptyTransactions from '../../../assets/img/EmptyTransactions.png';

const HistoryEmpty = () => {
  return (
    <section className="mx-auto d-flex flex-column align-items-center py-5">
      <div className="my-4">
        <img src={EmptyTransactions} alt="" />
      </div>
      <div className="text-center py-4">
        <h3 className="empty-message">Transaksi Masih Kosong</h3>
        <p className="w-50 mx-auto">
          Histori transaksi terisi jika kamu melakukan transaksi. Buat transaksi
          baru agar kamu bisa melihat detail transaksi yang telah kamu lakukan.
        </p>
      </div>
    </section>
  );
};

export default HistoryEmpty;
