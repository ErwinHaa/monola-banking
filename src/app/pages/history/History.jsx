import { useReducer, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  useRouteMatch,
  Route,
  useLocation,
  useHistory,
} from 'react-router-dom';
import { BiLeftArrowAlt } from 'react-icons/bi';

import * as enums from '../../../services/enums';
import { classNameJoin } from '../../../services/helpers';

import HistoryTransactionList from './HistoryTransactionList';
import DetailHistoryTransaction from './DetailHistoryTransaction';

import styles from './History.module.scss';

import { historySelectors, historyThunks } from '../../../features/history';

import historyReducer from '../../reducer/history';
import { historyActions } from '../../reducer/history';
import { HistoryProvider } from '../../context';

import HistoryLoader from './../../../common/components/Loader/HistoryLoader';
import HistoryEmpty from './HistoryEmpty';
import { userSelectors } from '../../../features/user';

const HISTORY_STATE = {
  itemId: '',
  userName: '',
  bankName: '',
  amount: null,
  accountNumber: '',
  dateDetail: '',
  category: '',
};

const History = () => {
  const [historyState, historyDispatch] = useReducer(
    historyReducer,
    HISTORY_STATE
  );

  const dispatch = useDispatch();

  const userHistoriesFetchingStatus = useSelector(
    historySelectors.selectUserHistoriesFetchingStatus
  );
  const userId = useSelector(userSelectors.selectUserId);

  useEffect(() => {
    (userHistoriesFetchingStatus === enums.IDLE ||
      userHistoriesFetchingStatus === enums.REJECTED) &&
      !!userId &&
      dispatch(historyThunks.fetchHistoryTransaction());
  }, [dispatch, userHistoriesFetchingStatus, userId]);

  const history = useHistory();
  const match = useRouteMatch();
  const location = useLocation();

  const handleOnClickHistoryItem = (dataHistoryItem) => {
    historyDispatch(historyActions.setHistoryData(dataHistoryItem));
    history.push(`${match.path}/riwayat?id=${dataHistoryItem.itemId}`);
  };

  const handleOnClickBack = () => {
    history.push(`${match.path}`);
  };

  const renderedTitle = location.pathname.includes('riwayat')
    ? 'Rincian Transaksi'
    : 'Histori Transaksi';

  const renderedBack = location.pathname.includes('riwayat') ? (
    <BiLeftArrowAlt className={styles.iconBack} onClick={handleOnClickBack} />
  ) : (
    ''
  );

  const classNameHeader = classNameJoin([
    'd-flex align-items-center position-relative',
    location.pathname.includes('riwayat') ? 'justify-content-center' : '',
  ]);

  const historiesData = useSelector(
    historySelectors.selectTransactionHistories
  );

  const renderedHistoryTransactionList = historiesData.map((data, id) => (
    <HistoryTransactionList
      key={id}
      date={!!data.transfers.length ? data.date : ''}
      transfers={data.transfers}
      onClickItem={handleOnClickHistoryItem}
    />
  ));

  const shouldRenderHistoriesList = historiesData.length ? (
    renderedHistoryTransactionList
  ) : (
    <HistoryEmpty />
  );

  return (
    <HistoryProvider value={{ historyState }}>
      <section className={styles.historyHome}>
        <header className={classNameHeader}>
          {renderedBack}
          <h3 className={styles.historyHeading}>{renderedTitle}</h3>
        </header>
        <hr />
        <Route exact path={`${match.path}`}>
          {userHistoriesFetchingStatus === 'pending' ? (
            <HistoryLoader />
          ) : (
            shouldRenderHistoriesList
          )}
        </Route>
        <Route path={`${match.path}/riwayat`}>
          <DetailHistoryTransaction />
        </Route>
      </section>
    </HistoryProvider>
  );
};

export default History;
