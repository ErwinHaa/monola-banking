import styles from './HistoryItem.module.scss';

import { BiChevronRight } from "react-icons/bi";

import { classNameJoin, separatorWithDot } from '../../../services/helpers';

const HistoryItem = ({ itemId, name, amount, accountNumber, bank, category, onClick, dateDetail }) => {
  const containerItemClassName = classNameJoin(['row align-items-center', styles.historyItem]);
  const itemAmountClassName = classNameJoin([styles.historyItemAmount, category === 'IN' ? styles.historyItemAmountGreen : category === 'OUT' ? styles.historyItemAmountRed : '']);
  
  const amountTransaction = separatorWithDot(amount);

  const historyDataItem = {
    itemId: itemId,
    userName: name,
    bankName: bank,
    amount: amount,
    accountNumber: accountNumber,
    dateDetail: dateDetail,
    category: category,
  }

  return (
    <div className={containerItemClassName} onClick={() => onClick(historyDataItem)}>
      <div className="col-6">
        <p className={styles.historyItemName}>{name}</p>
        <span className={styles.historyItemBank}>{bank}</span>
        <span className={styles.historyItemNumber}>{accountNumber}</span>
      </div>
      <div className="col-5">
        <span className={itemAmountClassName}>{category === 'IN' ? '+' : category === 'OUT' ? '-' : ''}Rp. {amountTransaction}</span>
      </div>
      <div className="col-1">
        <BiChevronRight size="37px"/>
      </div>
    </div>
  )
}

export default HistoryItem
