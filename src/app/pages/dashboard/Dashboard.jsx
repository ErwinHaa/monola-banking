import { useState, useCallback, useEffect } from 'react';
import { Route, useLocation, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Form, Button } from 'react-bootstrap';

import { SelectorProvider } from '../../context';

import { userActions, userSelectors } from '../../../features/user';
import { balanceSelectors } from '../../../features/balance';

import styles from './Dashboard.module.scss';

import { classNameJoin, localStorageHelpers } from '../../../services/helpers';

import { useDataDispatch } from '../../../common/hooks';

import Sidebar from '../../../common/components/sidebar/Sidebar';
import Transaction from '../transaction/Transaction';
import Saving from '../saving/Saving';
import History from '../history/History';
import Profile from '../profile/Profile';
import Home from '../home/Home';
import CancelTransaction from '../transaction/CancelTransaction';
import EWallet from '../ewallet/EWallet';
import Logout from '../logoutpage/Logout';
import { Modal } from 'react-bootstrap';

const Dashboard = ({ handleUserLogout }) => {
  const location = useLocation();
  const dispatch = useDispatch();
  const history = useHistory();

  const user = useSelector(userSelectors.selectUser);
  const balance = useSelector(balanceSelectors.selectUserBalance);

  const isLoggedIn = useSelector(userSelectors.selectIsLoggedIn);
  const userId = localStorageHelpers.getUserId();

  const [isFavorited, setIsFavorited] = useState(false);
  const [selector, setSelector] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [showCoinReceived, setShowCoinReceived] = useState(false);
  const [showCoinModal, setShowCoinModal] = useState(false);

  useEffect(() => {
    if (!isLoggedIn && !!userId) {
      dispatch(userActions.setUserId(userId));
    }
  }, [isLoggedIn, userId, dispatch]);

  useEffect(() => {
    showCoinReceived && setShowCoinModal(true);
  }, [showCoinReceived]);

  useDataDispatch();

  const handleSetFavorite = useCallback(() => {
    setIsFavorited((prevState) => !prevState);
  }, []);

  const resetFavorite = useCallback(() => {
    setIsFavorited(false);
  }, []);

  const handleOpenModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleCoinReceivedModalOpen = useCallback(() => {
    setShowCoinReceived(true);
  }, []);

  const handleCoinReceivedModalClose = () => {
    setShowCoinReceived(false);
    setShowCoinModal(false);
  };

  const handleConfirmClick = () => {
    dispatch(userActions.logout());

    localStorage.clear();

    history.replace('/');
  };

  const dashboardContainerClassName = classNameJoin([
    'container u-px-6 u-margin-top-small',
    styles.dashboardContainer,
  ]);
  const dashboardHomeClassName = classNameJoin([
    'col-9 px-4',
    styles.dashboardHome,
  ]);
  const sidebarClassName = classNameJoin([
    'col-3',
    styles.sidebar,
    location.pathname.includes('saving') && styles['sidebar--saving'],
  ]);

  if (user) {
    return (
      <SelectorProvider value={{ selector, setSelector }}>
        <div className={dashboardContainerClassName}>
          <div className="row g-3 h-100">
            <div className={sidebarClassName}>
              <Sidebar
                {...user}
                onClick={handleUserLogout}
                onLogoutOpen={handleOpenModal}
                showModal={showModal}
              />
            </div>
            <div className={dashboardHomeClassName}>
              <Route exact path="/dashboard">
                {!!user && (
                  <Home
                    userName={user.userFullName}
                    userAccNum={user.userAccountNumber}
                    userBalance={balance}
                    showCoinModal={showCoinModal}
                    onHide={handleCoinReceivedModalClose}
                  />
                )}
              </Route>
              <Route path="/dashboard/transaksi">
                <Transaction
                  userName={user.userFullName}
                  userAccNum={user.userAccountNumber}
                  userBalance={balance}
                  isFavorited={isFavorited}
                  setFavorite={handleSetFavorite}
                  resetFavorite={resetFavorite}
                  onCoinReceived={handleCoinReceivedModalOpen}
                />
              </Route>
              <Route path="/dashboard/ewallet">
                <EWallet
                  userName={user.userFullName}
                  userAccNum={user.userAccountNumber}
                  userBalance={balance}
                  onCoinReceived={handleCoinReceivedModalOpen}
                />
              </Route>

              <Route path="/dashboard/saving">
                <Saving />
              </Route>
              <Route path="/dashboard/history">
                <History />
              </Route>
              <Route path="/dashboard/profile">
                <Profile />
              </Route>
              <Route exact path="/dashboard/cancel-transaction">
                <CancelTransaction />
              </Route>
              <Route exact path="/dashboard/logout">
                <Logout />
              </Route>
            </div>
          </div>
        </div>
        <Modal
          show={showModal}
          onHide={handleCloseModal}
          className="logout-modal">
          <div className={styles.cancelContainer}>
            <div className={styles.content}>
              <h3 className={styles.heading}>Keluar</h3>
              <p className={styles.paragraph}>
                Apakah kamu yakin ingin keluar?
              </p>
              <Form.Group className="py-3 w-100">
                <div className="row justify-content-between">
                  <div className="col-6">
                    <Button
                      variant="outline-primary"
                      className={styles.cancelBtn}
                      onClick={handleCloseModal}>
                      Batal
                    </Button>
                  </div>
                  <div className="col-6">
                    <Button
                      className={styles.confirmBtn}
                      onClick={handleConfirmClick}>
                      Keluar
                    </Button>
                  </div>
                </div>
              </Form.Group>
            </div>
          </div>
        </Modal>
      </SelectorProvider>
    );
  }
};

export default Dashboard;
