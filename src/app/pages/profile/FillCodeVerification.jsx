import ReactCodeInput from 'react-code-input';
import { useState } from 'react';
import Countdown from 'react-countdown';

import styles from './FillCodeVerification.module.scss';

const FillCodeVerification = ({ email, nextStep }) => {
  const [code, setCode] = useState();
  const [errorMessageCode, setErrorMessageCode] = useState('');

  const handleChangeCode = (value) => {
    setCode(value);
    value === '545421' ? nextStep() : setErrorMessageCode('Kode verifikasi salah, silakan coba lagi');
  }

  const renderer = ({ minutes, seconds, completed }) => {
    if (completed) {
      return '';
    } else {
      return <p className={styles.countdown}>{minutes <10 ? `0${minutes}` : minutes}:{seconds < 10 ? `0${seconds}` : seconds} detik</p>
    };
  };

  return (
    <div className="d-flex flex-column text-center">
      <p className={styles.title}>Masukkan kode verifikasi</p>
      <p className={styles.subTitle}>Masukkan kode verifikasi yang telah dikirimkan ke email {email}</p>
      <ReactCodeInput
          type="number"
          fields={6}
          value={code}
          onChange={handleChangeCode}
          className={styles.pinInput}
        />
      <Countdown date={Date.now() + 60000} renderer={renderer} />
      <p className="mb-0">{errorMessageCode}</p>
      <span className={styles.requestCode}>Kirim Ulang Kode</span>
    </div>
  )
}

export default FillCodeVerification
