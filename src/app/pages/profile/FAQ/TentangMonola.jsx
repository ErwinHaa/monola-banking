import { Route, useRouteMatch, useLocation } from 'react-router';

import Accordion from "./Accordion"
import TentangMonolaContent from './TentangMonolaContent';
import Breadcrumb from '../../../../common/components/BreadCrumb/Breadcrumb';

import styles from './TentangMonola.module.scss';

const TentangMonola = ({ onClickApaMonola, onClickKelebihanMonola, onClickOjk, onClickCabang, onClickPerbedaan, onClickMenutupAkun, onClickMataUang, onClickPengaduan }) => {
  const match = useRouteMatch();
  const location = useLocation();

  const contentTentangMonola = <>
      <div className={styles.itemContainerTop} onClick={onClickApaMonola}>
        <span className={styles.item}>Apa sebenarnya Monola itu?</span>
      </div>
      <div className={styles.itemContainer} onClick={onClickKelebihanMonola}>
        <span>Apa kelebihan Monola?</span>
      </div>
      <div className={styles.itemContainer} onClick={onClickOjk}>
        <span>Apakah Monola diakui OJK?</span>
      </div>
      <div className={styles.itemContainer} onClick={onClickCabang}>
        <span>Dimana saja cabang Monola?</span>
      </div>
      <div className={styles.itemContainer} onClick={onClickPerbedaan}>
        <span>Apa perbedaan Monola Web dengan Monola App?</span>
      </div>
    </>
  
  const contentAkunMonola = <>
    <div className={styles.itemContainerTop} onClick={onClickMenutupAkun}>
        <span className={styles.item}>Bagaimana cara menutup akun Monola?</span>
      </div>
      <div className={styles.itemContainer} onClick={onClickMataUang}>
        <span>Mata uang apa saja yang dilayani Monola?</span>
      </div>
      <div className={styles.itemContainer} onClick={onClickPengaduan}>
        <span>Bagaimana prosedur pengaduan Nasabah?</span>
      </div>
  </>
  return (
    <div>
      <Breadcrumb>
        <Breadcrumb.Item to="/dashboard/profile">
          Profil
        </Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item to="/dashboard/profile/faq">FAQ</Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item to="/dashboard/profile/faq/tentang-monola">Tentang Monola</Breadcrumb.Item>
        {location.pathname === `${match.path}/apa-sebenarnya-monola-itu` ? 
        <>
          <Breadcrumb.Separator />
          <Breadcrumb.Item>Apa sebenarnya Monola itu?</Breadcrumb.Item>
        </> 
        : location.pathname === `${match.path}/apa-kelebihan-monola` ? 
        <>
          <Breadcrumb.Separator />
          <Breadcrumb.Item>Apa kelebihan Monola</Breadcrumb.Item>
        </> 
        : location.pathname === `${match.path}/diakui-ojk` ? 
        <>
          <Breadcrumb.Separator />
          <Breadcrumb.Item>Apakah Monola diakui OJK?</Breadcrumb.Item>
        </>
        : location.pathname === `${match.path}/cabang-monola` ? 
        <>
          <Breadcrumb.Separator />
          <Breadcrumb.Item>Dimana saja cabang Monola?</Breadcrumb.Item>
        </>
        : location.pathname === `${match.path}/perbedaan-web-dan-app` ? 
        <>
          <Breadcrumb.Separator />
          <Breadcrumb.Item>Apa perbedaan Monola Web dengan Monola App</Breadcrumb.Item>
        </>
        : location.pathname === `${match.path}/menutup-akun` ? 
        <>
          <Breadcrumb.Separator />
          <Breadcrumb.Item>Bagaimana cara menutup akun Monola?</Breadcrumb.Item>
        </>
        : location.pathname === `${match.path}/mata-uang` ? 
        <>
          <Breadcrumb.Separator />
          <Breadcrumb.Item>Mata uang apa saja yang dilayani Monola?</Breadcrumb.Item>
        </>
        : location.pathname === `${match.path}/pengaduan` ? 
        <>
          <Breadcrumb.Separator />
          <Breadcrumb.Item>Bagaimana prosedur pengaduan Nasabah?</Breadcrumb.Item>
        </>
        : ''}
    </Breadcrumb>
      <h6 className={styles.title}>Tentang Monola</h6>
      <Route exact path={`${match.path}`}>
        <Accordion title="Monola" content={contentTentangMonola}/>
        <Accordion title="Akun Monola Kamu" content={contentAkunMonola}/>
      </Route>
      <Route exact path={`${match.path}/apa-sebenarnya-monola-itu`}>
        <TentangMonolaContent title="Apa sebenarnya Monola itu?" subTitle="It’s a more fun way to bank! Monola membantu mengatur  uang kamu dengan menggunakan beragam teknologi yang inovatif. Download dan registrasi akunnya sekarang dari ponsel: paperless and signatureless."/>
      </Route>
      <Route exact path={`${match.path}/apa-kelebihan-monola`}>
        <TentangMonolaContent title="Apa kelebihan Monola?" subTitle="Monola memberikan kemudahan berbagai kebutuhan keuanganmu dengan teknologi terdepan dan keamanan yang terjamin." 
          text='Melalui Monola kamu bisa:'
          points={
            [
            'Memisahkan uang ke berbagai tabungan yang bisa kamu beri nama sendiri agar kamu lebih mudah untuk mengaturnya.', 
            'Melakukan berbagai transaksi dengan mudah sambil merawat pet.',
            'Melakukan transfer secara otomatis.'
            ]
          }/>
      </Route>
      <Route exact path={`${match.path}/diakui-ojk`}>
        <TentangMonolaContent title="Apakah Monola diakui OJK?" subTitle="Monola sudah terdaftar di OJK. Selain itu kami juga melakukan pengawasan untuk risiko penipuan dan memperbarui sistem keamanan." />
      </Route>
      <Route exact path={`${match.path}/cabang-monola`}>
        <TentangMonolaContent title="Dimana saja cabang Monola?" subTitle="Dengan Monola, kamu serasa punya bank dalam genggaman. Jadi, kamu bisa menghindari kemacetan dan tetap melakukan transaksi perbankan melalui ponsel; tanpa perlu menjejakkan kaki di bank. Namun, jika kamu ada kebutuhan untuk bertransaksi melalui kantor cabang, maka kamu bisa datang ke kantor cabang Monola yang tersebar di berbagai kota." />
      </Route>
      <Route exact path={`${match.path}/perbedaan-web-dan-app`}>
        <TentangMonolaContent title="Apa perbedaan Monola Web dengan Monola App?" subTitle="Pada Monola App terdapat fitur ubah pin yang tidak terdapat pada Monola Web." />
      </Route>
      <Route exact path={`${match.path}/menutup-akun`}>
        <TentangMonolaContent title="Bagaimana cara menutup akun Monola?" subTitle="Jika kamu ingin menutup Akun Monola, kamu bisa menghubungi Monola Customer Centre 1800-563  atau +62 21 298 52777 (dari luar Indonesia)." />
      </Route>
      <Route exact path={`${match.path}/mata-uang`}>
        <TentangMonolaContent title="Mata uang apa saja yang dilayani Monola?" subTitle="Saat ini Monola hanya melayani transaksi perbankan dalam mata uang Rupiah. Kedepannya, kami akan menginfokan apabila ada penambahan mata uang lain yang dapat dilayani oleh Monola." />
      </Route>
      <Route exact path={`${match.path}/pengaduan`}>
        <TentangMonolaContent 
          title="Bagaimana prosedur pengaduan Nasabah?"
          subTitle="Penyampaian pengaduan dapat dilakukan oleh Nasabah atau Perwakilan Nasabah yang dapat disampaikan dengan cara sebagai berikut:"
          points={
            [
              'Nasabah dapat mendatangi kantor cabang Monola terdekat dan menyampaikan pengaduan kepada Customer Service atau Relationship Manager.',
              'Nasabah dapat menghubungi Customer Centre Monola di nomor 1800-563 atau +62 21 298 52777 (dari luar Indonesia). Layanan Customer Center kami tersedia untuk Anda 24 jam sehari dan 7 hari dalam seminggu.',
              'Nasabah dapat juga mengirimkan email untuk menyampaikan pengaduan ke alamat email Monola di tanya@monola.com.'
            ]
          } />
      </Route>
    </div>
  )
}

export default TentangMonola