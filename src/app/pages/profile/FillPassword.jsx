import { Form, Button } from 'react-bootstrap';

import styles from './FillPassword.module.scss';

const FillPassword = ({ 
    onClickBackPengaturan, 
    onChangeNewPassword,
    onChangeCurrentPassword,
    onChangeConfirmNewPassword,
    newPassword,
    currentPassword,
    confirmNewPassword,
    nextStep }) => {

  return (
    <div className={styles.fillPassword}>
      <h6 className={styles.title}>Gunakan password yang unik untuk menjaga<br/>keamanan akun dan transaksimu</h6>
      <Form className={styles.form}>
        <Form.Group>
          <Form.Label className={styles.formLabel}>Password Saat Ini</Form.Label>
          <Form.Control 
            onChange={onChangeCurrentPassword}
            value={currentPassword}
            className={styles.formInput}
            type="password"
            placeholder="Masukkan Passwordmu Saat Ini"/>
        </Form.Group>
        <Form.Group>
          <Form.Label className={styles.formLabel}>Password Baru</Form.Label>
          <Form.Control 
            onChange={onChangeNewPassword}
            value={newPassword}
            className={styles.formInput}
            type="password"
            placeholder="Masukkan Password Barumu"/>
        </Form.Group>
        <Form.Group>
          <Form.Label className={styles.formLabel}>Konfirmasi Password Baru</Form.Label>
          <Form.Control
            onChange={onChangeConfirmNewPassword}
            value={confirmNewPassword}
            className={styles.formInput}
            type="password"
            placeholder="Konfirmasi Password Barumu"/>
            {confirmNewPassword &&
            confirmNewPassword !== newPassword 
            ? <span className={styles.errorMessage}>Password kamu tidak cocok</span>
            : ''}
        </Form.Group>
        <div className={styles.containerButton}>
          <Button className={styles.kembaliBtn} onClick={onClickBackPengaturan}>KEMBALI</Button>
          <Button className={styles.lanjutBtn} onClick={nextStep}>LANJUT</Button>
        </div>
      </Form>
    </div>
  )
}

export default FillPassword
