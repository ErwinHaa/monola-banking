import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useRouteMatch, Route, useHistory, useLocation } from 'react-router';

import { userSelectors, userThunks } from '../../../features/user';

import DashboardLayout from './../../../common/components/DashboardLayout/DashboardLayout';
import ProfileHome from './ProfileHome';
import ProfileSaya from './ProfileSaya';
import ChangeEmailSteps from './ChangeEmailSteps';
import Settings from './Settings';
import ChangePasswordSteps from './ChangePasswordSteps';
import FreezeCard from './FreezeCard/FreezeCard';
import ContactUs from './ContactUs/ContactUs';
import Faq from './FAQ/Faq';

import TentangMonola from './FAQ/TentangMonola';
import UbahPin from './UbahPin';

const Profile = () => {
  const match = useRouteMatch();
  const history = useHistory();
  let location = useLocation();
  const dispatch = useDispatch();

  const [currentPassword, setCurrentPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmNewPassword, setConfirmNewPassword] = useState('');

  const fetchUserProfileStatus = useSelector(
    userSelectors.selectUserProfileFetchingStatus
  );

  const userId = useSelector(userSelectors.selectUserId);

  useEffect(() => {
    fetchUserProfileStatus === 'idle' &&
      !!userId &&
      dispatch(userThunks.fetchUserProfile());
  }, [fetchUserProfileStatus, dispatch, userId]);

  const handleChangeCurrentPassword = (e) => {
    setCurrentPassword(e.target.value);
  };

  const handleChangeNewPassowrd = (e) => {
    setNewPassword(e.target.value);
  };

  const handleChangeConfirmNewPassword = (e) => {
    setConfirmNewPassword(e.target.value);
  };

  const handleOnClickProfilLink = () => {
    history.push(`${match.path}`);
  };

  const handleOnClickChangeEmail = () => {
    history.push(`${match.path}/profil-saya/ubah-email`);
  };

  const userProfile = useSelector(userSelectors.selectUserProfile);

  const handleClickProfileSaya = () => {
    history.push(`${match.path}/profil-saya`);
  };

  const handleOnClickPengaturan = () => {
    history.push(`${match.path}/pengaturan`);
  };

  const handleOnClickChangePassword = () => {
    history.push(`${match.path}/pengaturan/ubah-password`);
  };

  const handleOnClickFinish = () => {
    history.push(`${match.path}`);
  };

  const handleKembaliLogin = () => {
    history.push(`/login`);
  };

  const handleClickFreezeCard = () => {
    history.push(`${match.path}/pengaturan/freeze-card`);
  };

  const handleClickHubungiKami = () => {
    history.push(`${match.path}/hubungi-kami`);
  };

  const handleClickUbahPin = () => {
    history.push(`${match.path}/pengaturan/ubah-pin`);
  };

  const handleClickFaq = () => {
    history.push(`${match.path}/faq`);
  };

  const handleClickFaqTentangMonola = () => {
    history.push(`${match.path}/faq/tentang-monola`);
  };

  const handleClickApaMonola = () => {
    history.push(`${match.path}/faq/tentang-monola/apa-sebenarnya-monola-itu`);
  };

  const handleClickKelebihanMonola = () => {
    history.push(`${match.path}/faq/tentang-monola/apa-kelebihan-monola`);
  };

  const handleClickOjk = () => {
    history.push(`${match.path}/faq/tentang-monola/diakui-ojk`);
  };

  const handleClickCabang = () => {
    history.push(`${match.path}/faq/tentang-monola/cabang-monola`);
  };

  const handleClickPerbedaan = () => {
    history.push(`${match.path}/faq/tentang-monola/perbedaan-web-dan-app`);
  };

  const handleClickMenutupAkun = () => {
    history.push(`${match.path}/faq/tentang-monola/menutup-akun`);
  };

  const handleClickMataUang = () => {
    history.push(`${match.path}/faq/tentang-monola/mata-uang`);
  };

  const handleClickPengaduan = () => {
    history.push(`${match.path}/faq/tentang-monola/pengaduan`);
  };

  return (
    <DashboardLayout>
      <DashboardLayout.Head>
        <h3 className="dashboard-heading">
          {location.pathname.includes('ubah-email')
            ? 'Ubah Email'
            : location.pathname.includes('profil-saya')
            ? 'Profil Saya'
            : location.pathname.includes('pengaturan')
            ? 'Pengaturan'
            : location.pathname.includes('ubah-password')
            ? 'Ubah Password'
            : location.pathname.includes('freeze-card')
            ? 'Freeze Card'
            : location.pathname.includes('hubungi-kami')
            ? 'Hubungi Kami'
            : location.pathname.includes('ubah-pin')
            ? 'Ubah Pin'
            : location.pathname.includes('faq')
            ? 'FAQ'
            : 'Profil'}
        </h3>
      </DashboardLayout.Head>
      <Route exact path={`${match.path}`}>
        <ProfileHome
          onClickProfil={handleClickProfileSaya}
          onClickPengaturan={handleOnClickPengaturan}
          onClickHubungiKami={handleClickHubungiKami}
          onClickFaq={handleClickFaq}
        />
      </Route>
      <Route exact path={`${match.path}/profil-saya`}>
        <ProfileSaya
          user={userProfile}
          onClickProfil={handleOnClickProfilLink}
          onClickChangeEmail={handleOnClickChangeEmail}
        />
      </Route>
      <Route exact path={`${match.path}/profil-saya/ubah-email`}>
        <ChangeEmailSteps
          onClickToProfilSaya={handleClickProfileSaya}
          onClickFinish={handleOnClickFinish}
        />
      </Route>
      <Route exact path={`${match.path}/pengaturan`}>
        <Settings
          onClickProfil={handleOnClickProfilLink}
          onClickChangePassword={handleOnClickChangePassword}
          onClickFreezeCard={handleClickFreezeCard}
          onClickUbahPin={handleClickUbahPin}
        />
      </Route>
      <Route exact path={`${match.path}/pengaturan/ubah-password`}>
        <ChangePasswordSteps
          onClickBackPengaturan={handleOnClickPengaturan}
          currentPassword={currentPassword}
          newPassword={newPassword}
          confirmNewPassword={confirmNewPassword}
          onChangeCurrentPassword={handleChangeCurrentPassword}
          onChangeNewPassword={handleChangeNewPassowrd}
          onChangeConfirmNewPassword={handleChangeConfirmNewPassword}
          onClickBackLogin={handleKembaliLogin}
        />
      </Route>
      <Route exact path={`${match.path}/pengaturan/freeze-card`}>
        <FreezeCard onClickPengaturan={handleOnClickPengaturan} />
      </Route>
      <Route exact path={`${match.path}/hubungi-kami`}>
        <ContactUs />
      </Route>
      <Route exact path={`${match.path}/pengaturan/ubah-pin`}>
        <UbahPin previousStep={handleOnClickPengaturan} />
      </Route>
      <Route exact path={`${match.path}/faq`}>
        <Faq onClickTentangMonola={handleClickFaqTentangMonola} />
      </Route>
      <Route path={`${match.path}/faq/tentang-monola`}>
        <TentangMonola
          onClickApaMonola={handleClickApaMonola}
          onClickKelebihanMonola={handleClickKelebihanMonola}
          onClickOjk={handleClickOjk}
          onClickCabang={handleClickCabang}
          onClickPerbedaan={handleClickPerbedaan}
          onClickMenutupAkun={handleClickMenutupAkun}
          onClickMataUang={handleClickMataUang}
          onClickPengaduan={handleClickPengaduan}
        />
      </Route>
    </DashboardLayout>
  );
};

export default Profile;
