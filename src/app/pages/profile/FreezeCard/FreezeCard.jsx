import { Button } from 'react-bootstrap';
import { useState } from 'react';

import ListItemNavigation from '../../../../common/components/listItemNavigation/ListItemNavigation';
import Breadcrumb from '../../../../common/components/BreadCrumb/Breadcrumb';

import styles from './FreezeCard.module.scss';

const FreezeCard = ({ onClickPengaturan }) => {
  const [isOn, setIsOn] = useState(false);

  const handleOnClickToggle = () => {
    isOn ? setIsOn(false) : setIsOn(true)
  }

  return (
    <div className={styles.freeze}>
      <Breadcrumb>
        <Breadcrumb.Item to="/dashboard/profile">
          Profil
        </Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item to="/dashboard/profile/pengaturan">Pengaturan</Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item>Freeze Card</Breadcrumb.Item>
    </Breadcrumb>
      <p className={styles.title}>Freeze card-mu jika kartumu hilang.<br/>Kamu dapat mengaktikannya dengan mudah.</p>
      <ListItemNavigation text="Freeze Card" toggle={isOn} onClick={handleOnClickToggle}/>
      <div className={styles.containerButton}>
        <Button className={styles.kembaliBtn} onClick={onClickPengaturan}>KEMBALI</Button>
        <Button className={styles.simpanBtn}>SIMPAN</Button>
      </div>
    </div>
  )
}

export default FreezeCard