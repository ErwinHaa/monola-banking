import Breadcrumb from "../../../common/components/BreadCrumb/Breadcrumb"
import ForgotPin from "../forgotpin/ForgotPin"

const UbahPin = ({ previousStep }) => {
  return (
    <>
    <Breadcrumb>
        <Breadcrumb.Item to="/dashboard/profile">
          Profil
        </Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item to="/dashboard/profile/pengaturan">Pengaturan</Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item>Ubah PIN</Breadcrumb.Item>
    </Breadcrumb>
    <ForgotPin previousStep={previousStep} />
    </>
  )
}

export default UbahPin
