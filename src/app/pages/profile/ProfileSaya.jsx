import { useState } from 'react';

import UserDefault from '../../../assets/img/DefaultUser.png';
import { ReactComponent as Pencil } from '../../../assets/img/pencil.svg';
import { ReactComponent as PencilBlue } from '../../../assets/img/pencil-bluebg.svg';

import styles from './ProfileSaya.module.scss';

import UploadPhotoModal from './UploadPhotoModal';
import Breadcrumb from '../../../common/components/BreadCrumb/Breadcrumb';

const ProfileSaya = ({ user, onClickProfil, onClickChangeEmail }) => {
  const [modalShow, setModalShow] = useState(false);

  const handleClickEditPhoto = () => {
    setModalShow(true);
  };

  const hideModal = () => {
    setModalShow(false);
  };

  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item to="/dashboard/profile">Profil</Breadcrumb.Item>
        <Breadcrumb.Separator />
        <Breadcrumb.Item isActive>Profil Saya</Breadcrumb.Item>
      </Breadcrumb>
      <div className="row">
        <div className="col-7">
          <div className={styles.containerText}>
            <p className={styles.label}>Nama</p>
            <p className={styles.text}>{user.userFullName}</p>
          </div>
          <div className={styles.containerText}>
            <p className={styles.label}>User ID</p>
            <p className={styles.text}>{user.userUsername}</p>
          </div>
          <div className={styles.containerText}>
            <p className={styles.label}>Email</p>
            <span className={styles.text}>{user.userEmail}</span>
            <Pencil
              role="button"
              className="float-right"
              onClick={onClickChangeEmail}
            />
          </div>
          <div className={styles.containerText}>
            <p className={styles.label}>Nomor HP</p>
            <p className={styles.text}>{user.userPhoneNumber}</p>
          </div>
          <div className={styles.containerText}>
            <p className={styles.label}>KTP</p>
            <p className={styles.text}>{user.userKtp}</p>
          </div>
          <div className={styles.containerText}>
            <p className={styles.label}>NPWP</p>
            <p className={styles.text}>{user.userNpwp}</p>
          </div>
        </div>
        <div className="col-5">
          <div className={styles.photoContainer}>
            <img
              className={styles.photoProfile}
              src={user.userPhoto ? user.userPhoto : UserDefault}
              alt=""
            />
            <PencilBlue
              className={styles.pencilBlue}
              onClick={handleClickEditPhoto}
            />
          </div>
        </div>
        <UploadPhotoModal show={modalShow} onHide={hideModal} />
      </div>
    </>
  );
};

export default ProfileSaya;
