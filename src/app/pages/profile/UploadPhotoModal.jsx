import { Modal, Button } from 'react-bootstrap';
import { AiOutlineCloseCircle } from 'react-icons/ai';

import styles from './UploadPhotoModal.module.scss';

import { ReactComponent as Cloud } from '../../../assets/img/cloud-upload.svg';

const UploadPhotoModal = ({ show, onHide }) => {
  return show ? (
    <Modal
      show={show}
      onHide={onHide}
      backdrop={false}
      className="profile-modal">
      <Modal.Header className={styles.modalHeader}>
        <h3 className={styles.title}>Unggah Foto</h3>
        <AiOutlineCloseCircle
          className={styles.close}
          size="22px"
          color="#9E9E9E"
          onClick={onHide}
        />
      </Modal.Header>
      <div className={styles.garisContainer}>
        <hr className={styles.garis} />
      </div>
      <Modal.Body className={styles.modalBody}>
        <div className={styles.dashedBorder}>
          <Cloud />
          <span className={styles.textUpload}>
            Letakan file foto yang akan diunggah
            <br />
            atau pilih file
          </span>
        </div>
      </Modal.Body>
      <Modal.Footer className={styles.modalFooter}>
        <Button className={styles.batalBtn} onClick={onHide}>
          BATAL
        </Button>
        <Button className={styles.unggahBtn}>UNGGAH</Button>
      </Modal.Footer>
    </Modal>
  ) : (
    ''
  );
};

export default UploadPhotoModal;
