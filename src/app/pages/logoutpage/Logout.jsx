import { Form, Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import styles from './Logout.module.scss';

import { userActions } from '../../../features/user';

const Logout = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const handleBackClick = () => {
    history.goBack();
  };

  const handleConfirmClick = () => {
    dispatch(userActions.logout());

    localStorage.clear();

    history.replace('/');
  };

  return (
    <div className={styles.cancelContainer}>
      <div className={styles.content}>
        <h3 className={styles.heading}>Keluar</h3>
        <p className={styles.paragraph}>Apakah kamu yakin ingin keluar?</p>
        <Form.Group className="py-4 w-100">
          <div className="row justify-content-between">
            <div className="col-6">
              <Button
                variant="outline-primary"
                className={styles.cancelBtn}
                onClick={handleBackClick}>
                Batal
              </Button>
            </div>
            <div className="col-6">
              <Button
                className={styles.confirmBtn}
                onClick={handleConfirmClick}>
                Keluar
              </Button>
            </div>
          </div>
        </Form.Group>
      </div>
    </div>
  );
};

export default Logout;
