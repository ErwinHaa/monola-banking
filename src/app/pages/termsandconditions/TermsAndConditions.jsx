import { classNameJoin } from '../../../services/helpers';

import styles from './TermsAndConditions.module.scss';

const TermsAndServices = () => {
  const { topHeading, topContainer } = {
    topContainer: classNameJoin([
      'container-fluid u-margin-top-large',
      styles.topContainer,
    ]),
    topHeading: classNameJoin(['text-center', styles.topHeading]),
  };

  return (
    <div className={topContainer}>
      <div className={styles.heading}>
        <h1 className={topHeading}>Syarat dan Ketentuan</h1>
      </div>

      <main className={styles.termsAndConditions}>
        <h4 className={styles.textSectionHeading}>
          Instruksi Anda kami terima secara digital
        </h4>
        <ul className="my-0">
          <li>
            Seluruh instruksi perbankan Anda akan berasal dari aplikasi mobile
            atau online.
          </li>
          <li>
            Anda akan diminta untuk melakukan otorisasi atas instruksi Anda
            dengan menggunakan berbagai jenis informasi keamanan (misalnya PIN,
            nama pengguna, kata sandi, token). Anda bertanggung jawab sepenuhnya
            atas penggunaan dan kerahasiaan atas segala jenis informasi keamanan
            yang digunakan dalam bertransaksi. Pastikan Anda tidak
            memberitahukan informasi keamanan Anda kepada orang lain, termasuk
            staf Bank.
          </li>
          <li>
            Setelah melakukan otorisasi, kami akan menjalankan instruksi Anda.
          </li>
          <li>
            {' '}
            Anda memahami dan menyetujui metode pengiriman informasi secara
            elektronik yang digunakan untuk mengirimkan data nasabah dan segala
            risikonya. Anda menyatakan menyetujui untuk membebaskan Bank dari
            segala kerugian yang timbul.
          </li>
        </ul>
        <h4 className={styles.textSectionHeading}>
          Penyimpanan Data Pribadi Anda
        </h4>
        <ul className="mb-0">
          <li>
            Sesuai dengan persyaratan dari OJK (Otoritas Jasa Keuangan), Anda
            diminta untuk memberikan data pribadi saat melakukan pembukaan
            rekening di Bank.
          </li>
          <li>
            Pastikan informasi yang Anda berikan adalah informasi yang benar dan
            akurat, dan mohon menginformasikan kembali kepada Bank jika terdapat
            perubahan.
          </li>
          <li>
            Kami menyimpan kerahasiaan dan keamanan informasi diri yang Anda
            berikan. Penggunaan data Anda akan kami lakukan sesuai dengan
            ketentuan yang berlaku.
          </li>
          <li>
            Anda wajib memberitahukan dan menyampaikan perubahan Data kepada
            Bank. Perubahan tersebut hanya berlaku jika telah diterima dan/atau
            disetujui oleh Bank. Dalam hal Bank tidak menerima informasi apapun
            mengenai perubahan Data, maka Bank akan menggunakan Data Nasabah
            yang tercatat pada sistem Bank.
          </li>
          <li>
            Anda wajib menanggung segala akibat dan/atau kerugian yang mungkin
            timbul dari kelalaian Nasabah dalam memperbaharui Data pada Bank.
          </li>
        </ul>
        <h4 className={styles.textSectionHeading}>Kami memahami Anda</h4>
        <ul>
          <li>
            Kami akan selalu memberikan informasi mengenai produk/layanan kami,
            termasuk ketika terdapat perubahan dalam Syarat dan Ketentuan sesuai
            dengan ketentuan yang berlaku.
          </li>
          <li>
            Agar kami dan Anda saling memahami, kami meminta Anda untuk membaca
            seluruh isi Syarat dan Ketentuan ini atau ketentuan lain di situs
            kami, www.monola.com sesuai spesifik layanan yang akan Anda peroleh
            sehingga dengan ini Anda mengakui bahwa Anda telah membaca seluruh
            isi Syarat dan Ketentuan ini pada saat Anda mengajukan permohonan
            pemberian layanan, dan Anda memahami dan menyetujui sepenuhnya atas
            seluruh ketentuan dalam Syarat dan Ketentuan ini. Jika Anda tidak
            setuju dengan ketentuan dan/atau isi Syarat dan Ketentuan ini, atau
            tidak dapat secara akurat memahami interpretasi terhadap
            ketentuan-ketentuan tersebut, maka dimohon untuk tidak melanjutkan
            Syarat dan Ketentuan ini.
          </li>
          <li>
            Jika ada pertanyaan atau masukan mengenai Monola, Layanan Nasabah
            (Monola Help) (1-500-365) siap membantu Anda 24/7. Melalui layanan
            Monola Help, Anda dengan ini mengijinkan Bank untuk merekam atau
            mencatat pembicaraan Anda dengan Bank.
          </li>
        </ul>
      </main>
    </div>
  );
};

export default TermsAndServices;
