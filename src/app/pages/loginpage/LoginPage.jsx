import { useState, useEffect } from 'react';
import { MdVisibility, MdVisibilityOff } from 'react-icons/md';
import { Spinner } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, Link } from 'react-router-dom';

import { userThunks, userSelectors } from '../../../features/user';

import {
  login,
  leftSection,
  brand,
  title,
  subTitle,
  registerButton,
  tagline,
  disclaimerSection,
  disclaimer,
  rightSection,
  label,
  buttonLogin,
  forgotPassword,
  input,
  formEmail,
  formIcon,
  labelPassword,
  formControlPassword,
  formPassword,
  errorMessage,
  errorVisible,
} from './LoginPage.module.scss';
import styles from './LoginPage.module.scss';

import Card from '../../../assets/img/plainCreditCard.svg';

import Button from '../../../common/components/button/Button';
import CustomAlert from './../../../common/components/alert/CustomAlert';

import {
  isValidEmail,
  isValidPassword,
  localStorageHelpers,
} from '../../../services/helpers';

const LoginPage = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const loginStatus = useSelector(userSelectors.selectLoginStatus);

  const classNames = {
    loginRow: ['row', login].join(' ').trim(),
    loginLeftColumn: [
      'col-7 d-flex flex-column justify-content-between',
      leftSection,
    ]
      .join(' ')
      .trim(),
    buttonPurple: ['btn', registerButton].join(' ').trim(),
    bottom: ['d-flex row', disclaimerSection].join(' ').trim(),
    loginRightColumn: ['col-5', rightSection].join(' ').trim(),
    buttonGrey: ['btn btn-secondary btn-block', buttonLogin].join(' ').trim(),
    formGroupEmail: ['form-group', formEmail].join(' ').trim(),
    formGroupPassword: ['form-group d-flex flex-wrap', formPassword]
      .join(' ')
      .trim(),
    labelPasswords: [label, labelPassword].join(' ').trim(),
  };

  const {
    loginRow,
    loginLeftColumn,
    bottom,
    loginRightColumn,
    formGroupEmail,
    labelPasswords,
    formGroupPassword,
  } = classNames;

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isEmailTouched, setIsEmailTouched] = useState(false);
  const [isPasswordTouched, setIsPasswordTouched] = useState(false);
  const [formEmailErrorMessage, setFormEmailErrorMessage] = useState('');
  const [formPasswordErrorMessage, setFormPasswordErrorMessage] = useState('');
  const [visible, setVisible] = useState(false);

  const loginError = useSelector(userSelectors.selectLoginError);
  const userToken = localStorageHelpers.getUserToken();

  const formControl = [
    'form-control',
    input,
    (!isValidEmail(email) && isEmailTouched) || loginError
      ? styles.inputError
      : '',
  ]
    .join(' ')
    .trim();

  const formControlPasswords = [
    'form-control',
    input,
    formControlPassword,
    (!isValidPassword(password) && isPasswordTouched) || loginError
      ? styles.inputError
      : '',
  ]
    .join(' ')
    .trim();

  useEffect(() => {
    if (loginStatus === 'fulfilled' && userToken) {
      history.replace('/dashboard');
    } else if (loginStatus === 'rejected') {
      setFormEmailErrorMessage('Periksa kembali alamat email kamu');
      setFormPasswordErrorMessage('Periksa kembali password kamu');
    }
  }, [loginStatus, history, userToken]);

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
    if (!isValidEmail(email) && !loginError) {
      setFormEmailErrorMessage('Masukkan alamat email yang valid');
    } else {
      setFormEmailErrorMessage('');
    }
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);

    if (!isValidPassword(password) && !loginError) {
      setFormPasswordErrorMessage('Masukkan 6-24 karakter');
    } else {
      setFormPasswordErrorMessage('');
    }
  };

  //show and hide password
  const handlePasswordVisibilty = () => {
    setVisible((prevState) => !prevState);
  };

  //submit
  const handleSubmit = (e) => {
    e.preventDefault();

    dispatch(userThunks.logUserIn({ email, password }));
  };

  const emailError = [
    errorMessage,
    (!isValidEmail(email) && isEmailTouched) || loginError ? errorVisible : '',
  ]
    .join(' ')
    .trim();
  const passwordError = [
    errorMessage,
    (!isValidPassword(password) && isPasswordTouched) || loginError
      ? errorVisible
      : '',
  ]
    .join(' ')
    .trim();

  const isValid =
    !!email && !!password && isValidEmail(email) && isValidPassword(password);

  const shouldShowSpinner =
    loginStatus === 'pending' ? (
      <Spinner variant="primary" animation="border" className="mx-auto" />
    ) : (
      <Button variant="primary" disabled={!isValid} className={styles.btnLogin}>
        LOG IN
      </Button>
    );

  const shouldShowAlert = loginError && (
    <CustomAlert>
      Ups... email atau password yang kamu masukkan salah!
    </CustomAlert>
  );

  return (
    <div className="container-fluid d-flex u-margin-top-large">
      <div className={loginRow}>
        <div className={loginLeftColumn}>
          <div className="d-flex flex-column">
            <h4 className={brand}>monola</h4>
            <span className={tagline}>Make Banking Easy and Fun!</span>
          </div>
          <div>
            <div className="row mb-5">
              <div className="col-8">
                <h4 className={title}>Buka Rekening Monola di Smartphone-mu</h4>
                <p className={subTitle}>
                  Mudah buka rekening lewat aplikasi tanpa perlu ke bank untuk
                  mengaktivasi.
                </p>
              </div>
              <div className="col-4">
                <img src={Card} alt="" />
              </div>
            </div>
            <div className="row">
              <div className="col">
                <a
                  href="https://play.google.com/store"
                  target="_blank"
                  rel="noreferrer"
                  className={styles.btnRegis}>
                  Daftar Sekarang
                </a>
              </div>
            </div>
          </div>
          <div className={bottom}>
            <div className="col-7">
              <p className={disclaimer}>
                PT Monola Tbk terdaftar dan diawasi oleh Otoritas Jasa Keuangan
                (OJK) serta dijamin oleh Lembaga Penjamin Simpanan (LPS)
              </p>
            </div>
          </div>
        </div>
        <div className={loginRightColumn}>
          <h4 className={title}>Hai, Selamat Datang!</h4>
          <p className={subTitle}>
            Masukan e-mail dari password untuk log in ke Monola
          </p>
          {shouldShowAlert}
          <form onSubmit={handleSubmit}>
            <div className={formGroupEmail}>
              <label htmlFor="inputEmail" className={label}>
                Email
              </label>
              <input
                type="email"
                className={formControl}
                id="inputEmail"
                aria-describedby="emailHelp"
                placeholder="Masukkan alamat email"
                value={email}
                onChange={handleEmailChange}
                onBlur={() => setIsEmailTouched(true)}
              />
              <p className={emailError}>{formEmailErrorMessage}</p>
            </div>
            <div className={formGroupPassword}>
              <label htmlFor="inputPassword" className={labelPasswords}>
                Password
              </label>
              <input
                type={visible ? 'text' : 'password'}
                className={formControlPasswords}
                id="inputPassword"
                placeholder="Masukkan password"
                value={password}
                onChange={handlePasswordChange}
                maxLength="24"
                onBlur={() => setIsPasswordTouched(true)}
              />
              {visible ? (
                <MdVisibilityOff
                  className={formIcon}
                  onClick={handlePasswordVisibilty}
                />
              ) : (
                <MdVisibility
                  className={formIcon}
                  onClick={handlePasswordVisibilty}
                />
              )}
              <p className={passwordError}>{formPasswordErrorMessage}</p>
            </div>
            <Link className={forgotPassword} to="/">
              Lupa Password?
            </Link>
            <div className="mt-5 w-100 d-flex">{shouldShowSpinner}</div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
