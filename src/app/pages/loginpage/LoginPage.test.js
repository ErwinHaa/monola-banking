import { customRender, screen } from '../../../services/test/test-utils';
import userEvent from '@testing-library/user-event';

import LoginPage from './LoginPage';

describe('<LoginPage />', () => {
  test('if Login Page rendered without crashing', () => {
    customRender(<LoginPage />);
  });

  test('if email input error message is rendered', () => {
    customRender(<LoginPage />);

    const emailInput = screen.getByLabelText('Email');

    expect(emailInput).toBeInTheDocument();

    userEvent.type(emailInput, 'abc');

    userEvent.click(screen.getByText(/Hai/i));

    expect(
      screen.getByText('Masukkan alamat email yang valid')
    ).toBeInTheDocument();
  });
});
