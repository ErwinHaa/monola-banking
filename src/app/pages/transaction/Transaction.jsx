import { useState, useCallback } from 'react';
import { Route, useRouteMatch, Link, useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { transferActions, transferSelectors } from '../../../features/transfer';

import styles from './Transaction.module.scss';

import { ReactComponent as Add } from '../../../assets/img/Add.svg';

import { Breadcrumb } from '../../../common/components/BreadCrumb';
import { DashboardLayout } from '../../../common/components/DashboardLayout';
import AddNewAccount from './AddNewAccount';
import Transfer from './Transfer';
import TransactionHome from './TransactionHome';
import TransactionSteps from './TransactionSteps';
import TransactionFailed from '../../../common/components/transactionfailed/TransactionFailed';

const Transaction = ({
  userName,
  userAccNum,
  userBalance,
  setFavorite,
  isFavorited,
  resetFavorite,
  onCoinReceived,
}) => {
  const recipient = useSelector(transferSelectors.selectRecipient);
  const favoriteTransactions = useSelector(
    (state) => state.transfer.favoriteTransactions
  );

  const [bankSelect, setBankSelect] = useState('');
  const [doesExists, setDoesExists] = useState(false);

  const match = useRouteMatch();
  const location = useLocation();
  const dispatch = useDispatch();

  const handleBankSelect = (e) => {
    setBankSelect(e.target.value);
  };

  const handleSetExistence = useCallback(() => {
    dispatch(transferActions.setDoesExists(doesExists));
  }, [dispatch, doesExists]);

  const checkExistence = useCallback(() => {
    favoriteTransactions.forEach((transaction) => {
      if (
        recipient &&
        transaction.identityNumber === recipient.identityNumber
      ) {
        setDoesExists(true);
      }
    });
  }, [favoriteTransactions, recipient]);

  const shouldRenderAddBtn =
    location.pathname === '/dashboard/transaksi/transfer/search-account' ? (
      <Link to={`${match.url}/transfer/add-new-account`}>
        <Add className={styles.addIcon} />
      </Link>
    ) : null;

  const whichHeadingText = location.pathname.includes('transfer')
    ? 'Transfer'
    : 'Transaksi';

  const shouldRenderBreadcrumb = location.pathname.includes('transfer') ? (
    <Breadcrumb>
      <Breadcrumb.Item to="/dashboard/transaksi">Transaksi</Breadcrumb.Item>
      <Breadcrumb.Separator />
      <Breadcrumb.Item isActive={true}>Transfer</Breadcrumb.Item>
    </Breadcrumb>
  ) : null;

  return (
    <DashboardLayout>
      <DashboardLayout.Head>
        <h3 className="dashboard-heading">{whichHeadingText}</h3>
        {shouldRenderAddBtn}
      </DashboardLayout.Head>
      {shouldRenderBreadcrumb}
      <Route exact path={`${match.path}`}>
        <TransactionHome
          userName={userName}
          userAccNum={userAccNum}
          userBalance={userBalance}
        />
      </Route>
      <Route exact path={`${match.path}/transfer/search-account`}>
        <Transfer />
      </Route>

      <Route exact path={`${match.path}/transfer/fill-information`}>
        <TransactionSteps
          recipient={recipient}
          userName={userName}
          userAccNum={userAccNum}
          userBalance={userBalance}
          checkExistence={checkExistence}
          setExistence={handleSetExistence}
          setFavorite={setFavorite}
          isFavorited={isFavorited}
          resetFavorite={resetFavorite}
          onCoinReceived={onCoinReceived}
        />
      </Route>
      <Route exact path={`${match.path}/transfer/add-new-account`}>
        <AddNewAccount
          bankSelect={bankSelect}
          onChange={handleBankSelect}
          checkExistence={checkExistence}
          handleSetExistence={handleSetExistence}
        />
      </Route>
      <Route exact path={`${match.path}/transfer/transfer-failed`}>
        <TransactionFailed />
      </Route>
    </DashboardLayout>
  );
};

export default Transaction;
