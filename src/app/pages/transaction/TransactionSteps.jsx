import React, { useState, useEffect, useReducer } from 'react';
import StepWizard from 'react-step-wizard';
import { Modal } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import scheduleReducer from '../../reducer/schedule';
import { scheduleActions } from '../../reducer/schedule';

import styles from './TransactionSteps.module.scss';

import { ScheduleProvider } from '../../context';

import {
  transferActions,
  transferSelectors,
  transferThunks,
} from '../../../features/transfer';

import Panicman from '../../../assets/img/Panicman.png';

import FillInformation from './FillInformation';
import InsertPin from '../../../common/components/insertpin/InsertPin';
import TransactionResult from '../../../common/components/transactionresult/TransactionResult';
import TransactionSuccess from '../../../common/components/transactionsuccess/TransactionSuccess';
import { Button } from '../../../common/components/button';
import ForgotPin from '../forgotpin/ForgotPin';

const SCHEDULE_STATE = {
  startDate: null,
  endDate: null,
  whenDate: null,
  dayOrMonth: null,
  selectedDate: null,
  interval: 0,
};

const TransactionSteps = ({
  recipient,
  userName,
  userBalance,
  userAccNum,
  checkExistence,
  setExistence,
  setFavorite,
  isFavorited,
  resetFavorite,
  onCoinReceived,
}) => {
  const [scheduleState, scheduleDispatch] = useReducer(
    scheduleReducer,
    SCHEDULE_STATE
  );

  const history = useHistory();

  const [nominal, setNominal] = useState(0);
  const [description, setDescription] = useState('');
  const [transactionSchedule, setTransactionSchedule] = useState('SEKARANG');
  const [pinInput, setPinInput] = useState('');
  const [attempt, setAttempt] = useState(0);
  const [showModal, setShowModal] = useState(false);

  const savedNominal = useSelector((state) => state.transfer.transferAmount);
  const savedDescription = useSelector(
    (state) => state.transfer.transferDescription
  );
  const savedSchedule = useSelector((state) => state.transfer.transferSchedule);

  useEffect(() => {
    checkExistence();
  }, [checkExistence]);

  useEffect(() => {
    if (attempt === 3) {
      setShowModal(true);
    }
  }, [attempt]);

  /**
   * Schedule change handlers
   */

  const handleStartChange = (value) => {
    scheduleDispatch(scheduleActions.setStartDate(value));
  };

  const handleEndChange = (value) => {
    scheduleDispatch(scheduleActions.setEndDate(value));
  };

  const handleWhenChange = (value) => {
    scheduleDispatch(scheduleActions.setWhenDate(value));
  };

  const handleSelectedDateChange = (value) => {
    scheduleDispatch(scheduleActions.setSelectedDate(value));
  };

  const handleDayOrMonthChange = (e) => {
    scheduleDispatch(scheduleActions.setDayOrMonth(e.target.value));
  };

  const handleIntervalChange = (e) => {
    scheduleDispatch(scheduleActions.setInterval(e.target.value));
  };

  const handleSelectedDayChange = (e) => {
    scheduleDispatch(scheduleActions.setSelectedDay(e.target.value));
  };

  const scheduleHandlers = {
    onStartChange: handleStartChange,
    onEndChange: handleEndChange,
    onWhenChange: handleWhenChange,
    onDayOrMonthChange: handleDayOrMonthChange,
    onIntervalChange: handleIntervalChange,
    onSelectedDateChange: handleSelectedDateChange,
    onSelectedDayChange: handleSelectedDayChange,
  };

  const date = new Date().toLocaleString('id-ID', {
    day: '2-digit',
    month: 'long',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
  });

  const dispatch = useDispatch();

  const userId = useSelector((state) => state.user.user.userId);

  const handleTransferSubmit = () => {
    const transferInfoObj = {
      accountName: recipient.userName,
      accountNumber: recipient.identityNumber,
      amount: +savedNominal,
      bankName: recipient.provider,
      description: savedDescription,
      pin: +pinInput,
      userId,
    };

    dispatch(transferThunks.transferCash(transferInfoObj));
    setAttempt((prevState) => prevState + 1);
  };

  const handleNominalInput = (value) => {
    setNominal(value);
  };

  const handleDescriptionInput = (e) => {
    setDescription(e.target.value);
  };

  const handleScheduleSelect = (e) => {
    setTransactionSchedule(e.target.value);
    scheduleDispatch(scheduleActions.resetScheduleState(SCHEDULE_STATE));
  };

  const handlePinChange = (value) => {
    setPinInput(value);
  };

  const handleModalClose = () => {
    setAttempt(0);
    setShowModal(false);

    dispatch(transferActions.resetTransferStatus());

    history.replace('/dashboard');
  };

  return (
    <ScheduleProvider
      value={{
        scheduleState,
        scheduleHandlers,
      }}>
      <div>
        <StepWizard isHashEnabled isLazyMount>
          <FillInformation
            recipient={recipient}
            onNominalChange={handleNominalInput}
            onDescriptionChange={handleDescriptionInput}
            onScheduleChange={handleScheduleSelect}
            nominal={nominal}
            description={description}
            schedule={transactionSchedule}
            userName={userName}
            userBalance={userBalance}
            userAccNum={userAccNum}
            resetFavorite={resetFavorite}
            setExistence={setExistence}
          />
          <TransactionResult
            recipient={recipient}
            nominal={savedNominal}
            description={savedDescription}
            schedule={savedSchedule}
            onCheck={setFavorite}
            isFavorited={isFavorited}
          />
          <InsertPin
            pin={pinInput}
            onChange={handlePinChange}
            onClick={handleTransferSubmit}
            isFavorited={isFavorited}
            attempt={attempt}
          />
          <ForgotPin />
          <TransactionSuccess
            recipient={recipient}
            date={date}
            nominal={savedNominal}
            description={savedDescription}
            selector={transferSelectors.selectRecipient}
            onCoinReceived={onCoinReceived}
          />
        </StepWizard>
      </div>
      <Modal
        show={showModal}
        onHide={handleModalClose}
        backdrop="static"
        className={styles.modal}>
        <Modal.Body className="d-flex flex-column justify-content-center px-5 pt-5">
          <img src={Panicman} alt="" className={styles.pinModalBody} />
          <p className={styles.paragraph}>
            Oh, tidak! Kamu telah memasukkan PIN yang salah berkali-kali. Untuk
            memulihkan PIN, segera hubungi Monola Help yang siap membantumu
            kapan saja melalui 1800-563.
          </p>
        </Modal.Body>
        <Button.Primary
          className={styles.backButton}
          onClick={handleModalClose}>
          Kembali ke beranda
        </Button.Primary>
      </Modal>
    </ScheduleProvider>
  );
};

export default TransactionSteps;
