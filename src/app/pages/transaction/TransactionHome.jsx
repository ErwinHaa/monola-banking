import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { balanceSelectors, balanceThunks } from '../../../features/balance';

import {
  separatorWithDot,
  creditCardNumberFormat,
} from '../../../services/helpers';

import UserCard from '../../../common/components/userCard/UserCard';
import Option from '../../../common/components/option/Option';
import ButtonTransfer from '../../../common/components/button/ButtonTransfer';
import CardLoader from './../../../common/components/Loader/CardLoader';

import styles from './TransactionHome.module.scss';

const TransactionHome = ({ userName, userAccNum, userBalance }) => {
  const dispatch = useDispatch();

  const fetchBalanceStatus = useSelector(
    balanceSelectors.selectBalanceFetchStatus
  );

  useEffect(() => {
    fetchBalanceStatus === 'idle' && dispatch(balanceThunks.fetchUserBalance());
  }, [dispatch, fetchBalanceStatus]);

  return (
    <div className="">
      <UserCard variant>
        {fetchBalanceStatus === 'pending' ? (
          <CardLoader />
        ) : (
          <>
            <h3 className={styles.name}>{userName}</h3>
            <h4 className={styles.rekening}>
              {creditCardNumberFormat(userAccNum)}
            </h4>
            <h4 className={styles.saldoTitle}>Saldo Aktif</h4>
            <h4 className={styles.saldo}>
              Rp. {separatorWithDot(userBalance)}
            </h4>
            <ButtonTransfer />
          </>
        )}
      </UserCard>
      <Option />
    </div>
  );
};

export default TransactionHome;
