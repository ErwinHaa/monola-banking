import { useState, useEffect, useContext } from 'react';
import { Form } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import isAfter from 'date-fns/isAfter';
import isEqual from 'date-fns/isEqual';
import CurrencyInput from 'react-currency-input-field';

import { scheduleContext } from '../../context';

import {
  separatorWithDot,
  creditCardNumberFormat,
} from '../../../services/helpers';

import { useQuery } from '../../../common/hooks';

import {
  transferSelectors,
  transferActions,
  transferThunks,
} from '../../../features/transfer';

import TransactionItem from '../../../common/components/transactionGroup/TransactionItem';
import UserCard from '../../../common/components/userCard/UserCard';
import ButtonNext from '../../../common/components/button/ButtonNext';
import ButtonPrevious from '../../../common/components/button/ButtonPrevious';
import CustomAlert from '../../../common/components/alert/CustomAlert';
import ScheduleOptions from './ScheduleOptions';
import TransactionItemLoader from './../../../common/components/Loader/TransactionItemLoader';

const FillInformation = ({
  recipient,
  nextStep,
  onNominalChange,
  onDescriptionChange,
  onScheduleChange,
  nominal,
  description,
  schedule,
  userName,
  userAccNum,
  userBalance,
  resetFavorite,
  setExistence,
}) => {
  const { scheduleState } = useContext(scheduleContext);

  const dispatch = useDispatch();
  const history = useHistory();

  const recipientId = useQuery().get('recipientId');

  const fetchRecipientStatus = useSelector(
    transferSelectors.selectFetchRecipientStatus
  );

  const [isTouched, setIsTouched] = useState(false);

  const { startDate, endDate, whenDate } = scheduleState;

  const isDateValid =
    !!endDate && !!startDate ? isAfter(endDate, startDate) : false;

  const isValid = nominal >= 10000 && schedule && nominal < userBalance;

  const whichValidation = () => {
    if (schedule === 'SEKARANG') {
      return isValid;
    } else if (schedule === 'TANGGAL_TERTENTU') {
      return isValid && !!whenDate;
    } else {
      return isValid && isDateValid;
    }
  };

  useEffect(() => {
    resetFavorite();
  }, [resetFavorite]);

  useEffect(() => {
    fetchRecipientStatus === 'idle' && !!recipientId
      ? dispatch(transferThunks.fetchRecipientById(recipientId))
      : !recipientId && history.replace('/dashboard');
  }, [fetchRecipientStatus, recipientId, dispatch, history]);

  const handlePreviousClick = () => {
    history.push('/dashboard/transaksi');
  };

  const handleBalanceCheck = () => {
    dispatch(
      transferActions.saveTransferInformation({
        transferAmount: nominal,
        transferDescription: description,
        transferSchedule: schedule,
      })
    );
    setExistence();
    nextStep();
  };

  const shouldShowAlert =
    nominal > userBalance && isTouched ? (
      <CustomAlert>
        Nominal transfer yang anda masukkan melebihi jumlah saldo yang anda
        miliki!
      </CustomAlert>
    ) : nominal < 10000 && isTouched && nominal > 0 ? (
      <CustomAlert>
        Nominal transfer yang anda masukkan tidak boleh kurang dari Rp 10.000
      </CustomAlert>
    ) : !!startDate && !!endDate && isEqual(startDate, endDate) ? (
      <CustomAlert>
        Tanggal mulai transfer tidak boleh sama dengan tanggal berakhir transfer
      </CustomAlert>
    ) : !isDateValid && !!endDate && !!startDate ? (
      <CustomAlert>
        Tanggal berakhir transfer tidak boleh kurang dari tanggal mulai transfer
      </CustomAlert>
    ) : null;

  const shouldShowMinimalInput =
    nominal < 10000 && isTouched && nominal > 0 ? (
      <Form.Text className="text-danger">Minimal transaksi Rp 10.000</Form.Text>
    ) : null;

  return (
    <>
      <UserCard small>
        <div className="row">
          <div className="col-6">
            <p className="text text--thin mb-0">Saldo Aktif</p>
            <p className="text text--bold mb-0">
              Rp. {separatorWithDot(userBalance)}
            </p>
          </div>
          <div className="col-6 d-flex align-items-end flex-column justify-content-center">
            <p className="text text--bold mb-0">{userName}</p>
            <p className="text text--thin mb-0">
              {creditCardNumberFormat(userAccNum)}
            </p>
          </div>
        </div>
      </UserCard>
      <h6 className="font-weight-bold my-4">Informasi Transaksi</h6>
      {fetchRecipientStatus === 'fulfilled' && !!recipient ? (
        <TransactionItem selector={transferSelectors.selectRecipient} />
      ) : (
        fetchRecipientStatus === 'pending' && <TransactionItemLoader />
      )}
      {/* Alert here */}
      {shouldShowAlert}
      <Form>
        <Form.Group>
          <Form.Label className="fw-600">Nominal</Form.Label>
          <CurrencyInput
            value={nominal}
            onValueChange={onNominalChange}
            placeholder="Masukkan Nominal"
            className="form-control"
            prefix="Rp."
            groupSeparator="."
            onFocus={() => setIsTouched(true)}
          />
          {shouldShowMinimalInput}
        </Form.Group>
        <Form.Group>
          <Form.Label className="fw-600">Deskripsi (opsional)</Form.Label>
          <Form.Control
            placeholder="Masukkan Deskripsi"
            onChange={onDescriptionChange}
            value={description}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label className="fw-600">Kapan</Form.Label>
          <Form.Control
            as="select"
            onChange={onScheduleChange}
            value={schedule}>
            <option value="SEKARANG">Sekarang</option>
            <option value="TANGGAL_TERTENTU">Pada Tanggal Tertentu</option>
            <option value="SETIAP_N_HARI_BULAN">Setiap N Hari/Bulan</option>
            <option value="SETIAP_MINGGU">Setiap Minggu</option>
            <option value="SETIAP_BULAN">Setiap Bulan</option>
          </Form.Control>
        </Form.Group>
        <ScheduleOptions schedule={schedule} />
        <Form.Group className="py-4">
          <div className="row justify-content-between">
            <div className="col-6">
              <ButtonPrevious onClick={handlePreviousClick}>
                Batal
              </ButtonPrevious>
            </div>
            <div className="col-6">
              <ButtonNext
                onClick={handleBalanceCheck}
                disabled={!whichValidation()}
                type="button">
                Lanjut
              </ButtonNext>
            </div>
          </div>
        </Form.Group>
      </Form>
    </>
  );
};

export default FillInformation;
