import { useContext } from 'react';
import { Form } from 'react-bootstrap';

import { scheduleContext } from '../../context';

import CustomCalendar from '../../../common/components/customcalendar/CustomCalendar';

const ScheduleOptions = ({ schedule }) => {
  const { scheduleState, scheduleHandlers } = useContext(scheduleContext);

  const {
    startDate,
    endDate,
    whenDate,
    dayOrMonth,
    selectedDate,
    interval,
    selectedDay,
  } = scheduleState;

  const {
    onStartChange,
    onEndChange,
    onWhenChange,
    onSelectedDateChange,
    onDayOrMonthChange,
    onIntervalChange,
    onSelectedDayChange,
  } = scheduleHandlers;

  switch (schedule) {
    case 'SEKARANG':
      return null;
    case 'TANGGAL_TERTENTU':
      return (
        <Form.Group>
          <Form.Label className="fw-600">Pada</Form.Label>
          <CustomCalendar selected={whenDate} onChange={onWhenChange} />
        </Form.Group>
      );
    case 'SETIAP_N_HARI_BULAN':
      return (
        <>
          <div className="row">
            <div className="col-6">
              <Form.Group>
                <Form.Label>Setiap</Form.Label>
                <Form.Control
                  as="select"
                  value={interval}
                  onChange={onIntervalChange}>
                  <option>Masukkan Interval Jadwal</option>
                  <option value={1}>1</option>
                  <option value={2}>2</option>
                </Form.Control>
              </Form.Group>
            </div>
            <div className="col-6">
              <Form.Group>
                <Form.Label>Pilih Hari/Bulan</Form.Label>
                <Form.Control
                  as="select"
                  value={dayOrMonth}
                  onChange={onDayOrMonthChange}>
                  <option value="">Pilih Hari/Bulan</option>
                  <option value="day">Hari</option>
                  <option value="month">Bulan</option>
                </Form.Control>
              </Form.Group>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <Form.Group>
                <Form.Label>Tanggal Mulai</Form.Label>
                <CustomCalendar selected={startDate} onChange={onStartChange} />
              </Form.Group>
            </div>
            <div className="col-6">
              <Form.Group>
                <Form.Label>Tanggal Berakhir</Form.Label>
                <CustomCalendar selected={endDate} onChange={onEndChange} />
              </Form.Group>
            </div>
          </div>
        </>
      );
    case 'SETIAP_MINGGU':
      return (
        <>
          <div className="row">
            <div className="col">
              <Form.Group>
                <Form.Label>Pilih Hari</Form.Label>
                <Form.Control
                  as="select"
                  value={selectedDay}
                  onChange={onSelectedDayChange}>
                  <option value="">Pilih Hari</option>
                  <option value="senin">Senin</option>
                  <option value="selasa">Selasa</option>
                  <option value="rabu">Rabu</option>
                  <option value="kamis">Kamis</option>
                  <option value="jumat">Jumat</option>
                  <option value="sabtu">Sabtu</option>
                  <option value="minggu">Minggu</option>
                </Form.Control>
              </Form.Group>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <Form.Group>
                <Form.Label>Tanggal Mulai</Form.Label>
                <CustomCalendar selected={startDate} onChange={onStartChange} />
              </Form.Group>
            </div>
            <div className="col-6">
              <Form.Group>
                <Form.Label>Tanggal Berakhir</Form.Label>
                <CustomCalendar selected={endDate} onChange={onEndChange} />
              </Form.Group>
            </div>
          </div>
        </>
      );
    case 'SETIAP_BULAN':
      return (
        <>
          <div className="row">
            <div className="col">
              <Form.Group>
                <Form.Label>Pilih Tanggal</Form.Label>
                <CustomCalendar
                  selected={selectedDate}
                  onChange={onSelectedDateChange}
                />
              </Form.Group>
            </div>
          </div>
          <div className="row">
            <div className="col-6">
              <Form.Group>
                <Form.Label>Tanggal Mulai</Form.Label>
                <CustomCalendar selected={startDate} onChange={onStartChange} />
              </Form.Group>
            </div>
            <div className="col-6">
              <Form.Group>
                <Form.Label>Tanggal Berakhir</Form.Label>
                <CustomCalendar selected={endDate} onChange={onEndChange} />
              </Form.Group>
            </div>
          </div>
        </>
      );
    default:
      return null;
  }
};

export default ScheduleOptions;
