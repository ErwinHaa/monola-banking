import { useState, useEffect, useContext } from 'react';
import { Form } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { selectorContext } from '../../context';

import {
  transferSelectors,
  transferThunks,
  transferActions,
} from '../../../features/transfer';

import styles from './AddNewAccount.module.scss';

import TransactionItem from '../../../common/components/transactionGroup/TransactionItem';
import ButtonNext from '../../../common/components/button/ButtonNext';
import ButtonPrevious from '../../../common/components/button/ButtonPrevious';
import CustomAlert from '../../../common/components/alert/CustomAlert';
import TransactionItemLoader from '../../../common/components/Loader/TransactionItemLoader';

const AddNewAccount = () => {
  const [bank, setBank] = useState('');
  const [rekening, setRekening] = useState('');
  const { selectRecipient } = transferSelectors;

  const { setSelector } = useContext(selectorContext);

  const history = useHistory();

  const recipient = useSelector(selectRecipient);
  const fetchRecipientError = useSelector(
    transferSelectors.selectFetchRecipientError
  );
  const fetchRecipientStatus = useSelector(
    transferSelectors.selectFetchRecipientStatus
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(transferActions.resetRecipient());
  }, [dispatch]);

  let isValid = false;

  const handleBankSelect = (e) => {
    setBank(e.target.value);
  };

  const handleRekeningInput = (e) => {
    setRekening(e.target.value);
  };

  const handleSearchSubmit = (e) => {
    e.preventDefault();
    dispatch(transferThunks.fetchRecipientByAccount(rekening, bank));
  };

  const handlePreviousClick = () => {
    history.goBack();
  };

  const handleNextClick = () => {
    setSelector(() => transferSelectors.selectRecipient);
    history.push('/dashboard/transaksi/transfer/fill-information');
  };

  if (!!bank && !!rekening) {
    isValid = true;
  }

  return (
    <>
      <h5 className="fw-600 my-4">Informasi Akun Bank</h5>
      <Form onSubmit={handleSearchSubmit}>
        <Form.Group>
          <Form.Label className="fw-600">Bank</Form.Label>
          <Form.Control
            as="select"
            onChange={handleBankSelect}
            value={bank}
            className="px-3">
            <option>Pilih Bank</option>
            <option value="BCA">Bank BCA</option>
            <option value="Permata" disabled className={styles.disabledOption}>
              Bank Permata
            </option>
            <option value="BNI" disabled>
              Bank BNI
            </option>
            <option value="CIMBNiaga" disabled>
              Bank CIMB Niaga
            </option>
            <option value="Mandiri" disabled>
              Bank Mandiri
            </option>
            <option value="jenius" disabled>
              Jenius
            </option>
            <option value="BRI" disabled>
              Bank BRI
            </option>
          </Form.Control>
        </Form.Group>
        <Form.Group>
          <Form.Label className="fw-600">Nomor Rekening</Form.Label>
          <Form.Control
            placeholder="Masukkan nomor rekening"
            value={rekening}
            onChange={handleRekeningInput}
          />
        </Form.Group>
        {!!recipient && !fetchRecipientError ? (
          <>
            <h5>Hasil Pencarian</h5>
            <TransactionItem
              selector={transferSelectors.selectRecipient}
            />{' '}
          </>
        ) : fetchRecipientError ? (
          <>
            <h6 className="font-weight-bold">Hasil Pencarian</h6>
            <CustomAlert>
              Nomor rekening tidak ditemukan. Mohon periksa kembali nomor
              rekening.
            </CustomAlert>
          </>
        ) : fetchRecipientStatus === 'pending' ? (
          <TransactionItemLoader />
        ) : null}
        <Form.Group className="py-4">
          <div className="row justify-content-between">
            <div className="col-6">
              {/* <Button variant="outline-primary w-100 text-uppercase">
                Batal
              </Button> */}
              <ButtonPrevious onClick={handlePreviousClick}>
                Batal
              </ButtonPrevious>
            </div>
            <div className="col-6">
              {!!recipient ? (
                <ButtonNext onClick={handleNextClick}>Lanjut</ButtonNext>
              ) : (
                <ButtonNext type="submit" disabled={!isValid}>
                  Cari
                </ButtonNext>
              )}
            </div>
          </div>
        </Form.Group>
      </Form>
    </>
  );
};

export default AddNewAccount;
