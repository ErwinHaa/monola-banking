import { classNameJoin } from '../../../services/helpers';

import styles from './PrivacyPolicy.module.scss';

const PrivacyPolicy = () => {
  const { miniHeading, topHeading, topContainer } = {
    topContainer: classNameJoin([
      'container-fluid u-margin-top-large',
      styles.topContainer,
    ]),
    topHeading: classNameJoin(['text-center', styles.topHeading]),
    miniHeading: classNameJoin(['d-block', styles.miniHeading]),
  };

  return (
    <div className={topContainer}>
      <div className={styles.heading}>
        <h1 className={topHeading}>
          Kebijakan Privasi
          <span className={miniHeading}>
            Melindungi seluruh informasi Nasabah dan transparan dengan apa yang
            kami lakukan terhadap informasi tersebut adalah hal terpenting dalam
            membangun kepercayaan dengan Nasabah Monola.
          </span>
        </h1>
      </div>

      <main className={styles.policies}>
        <section className={styles.textSection}>
          <h4 className={styles.textSectionHeading}>Privasi Nasabah</h4>
          <p className={styles.textSectionParagraph}>
            Tujuan kami adalah untuk mempertahankan kepercayaan dan keyakinan
            ketika menangani informasi kamu.
          </p>
        </section>

        <section className={styles.textSection}>
          <h4 className={styles.textSectionHeading}>
            Peluang untuk membuat pilihan
          </h4>
          <p className={styles.textSectionParagraph}>
            Sebagai nasabah Monola, kamu punya kesempatan untuk menetapkan
            pilihan. Ketika kamu mempertimbangkan pilihanmu, kami menganjurkan
            kamu untuk membuat pilihan yang memungkinkan kami untuk dapat
            memberikan produk dan layanan yang berkualitas untuk memenuhi
            kebutuhan financial dan tujuanmu.
          </p>
        </section>

        <section className={styles.textSection}>
          <h4 className={styles.textSectionHeading}>
            Kerahasiaan informasi pribadi
          </h4>
          <p className={styles.textSectionParagraph}>
            Prioritas kami adalah menjaga keamanan identitas personalmu. Guna
            menjaga informasi kamu, kami menjaga secara fisik; elektronik; serta
            melindungi prosedur hukum yang sesuai dengan ketentuan yang berlaku.
            Kami juga melatih para karyawan kami untuk dapat menjaga kerahasiaan
            informasi pribadi dengan cara yang tepat. Apabila kami bekerjasama
            dengan institusi atau perusahaan lain yang memberikan pelayanan bagi
            kami, kami mewajibkan tiap perusahaan tersebut untuk melindungi
            kerahasiaan informasi pribadi yang mereka terima.
          </p>
        </section>

        <section className={styles.textSection}>
          <h4 className={styles.textSectionHeading}>Kebijakan Cookie</h4>
          <p className={styles.textSectionParagraph}>
            Situs Monola menggunakan cookie untuk mengumpulkan informasi guna
            memberikan layanan yang lebih baik kepada semua pengunjung. Cookie
            merupakan sebuah berkas teks berupa informasi yang tersimpan di
            komputermu (termasuk tablet dan smartphonemu). Informasi ini
            meliputi hal mendasar seperti istilah yang kamu gunakan saat
            menelusuri situs Monola dan konten yang kamu baca. Informasi dari
            cookie inilah yang membantu kami mengumpulkan informasi statistik
            pengunjung untuk meningkatkan layanan dari situs Monola.
            <br />
            <br />
            Beberapa cookie sangat penting untuk layanan situs Monola agar dapat
            bekerja dengan baik, cookie yang lain digunakan untuk mengumpulkan
            informasi statistik pengunjung sehingga kami dapat membuat situs
            Monola lebih nyaman dan berguna bagi kamu. Beberapa cookie bersifat
            sementara dan akan hilang ketika kamu menutup browser, sementara ada
            pula cookie yang tetap bertahan dan akan tinggal di komputermu untuk
            beberapa waktu. Kami juga menggunakan beberapa cookie yang terkait
            dengan kampanye dan akan hilang ketika kampanye berakhir.
            <br />
            <br />
            Jika kamu memilih untuk tidak menggunakan cookie selama kunjunganmu,
            kamu harus menyadari bahwa fungsi dan halaman tertentu tidak akan
            bekerja secera optimal.Jika kamu ingin menghapus cookie yang sudah
            ada di komputermu, silahkan lihat petunjuk browser dengan mengklik
            "Help" di menu browsermu.
          </p>
          <br />
          <br />
        </section>
      </main>
    </div>
  );
};

export default PrivacyPolicy;
