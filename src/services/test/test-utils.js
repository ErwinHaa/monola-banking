import { render as rtlRender } from '@testing-library/react';
import { Provider } from 'react-redux';

import { configureStore } from './configureStore';

const render = (ui, store, ...renderOptions) => {
  const Wrapper = ({ children }) => {
    return <Provider store={store || configureStore()}>{children}</Provider>;
  };

  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions });
};

export * from '@testing-library/react';
export { render as customRender };
