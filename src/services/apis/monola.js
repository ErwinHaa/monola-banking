import axios from 'axios';

const { REACT_APP_BASEURL: BASEURL } = process.env;

export default axios.create({
  baseURL: BASEURL,
});
