import { rest } from 'msw';

import { getAllAccessories } from './accessoriesResolvers';

export const handlers = [rest.get('/accessories', getAllAccessories)];
