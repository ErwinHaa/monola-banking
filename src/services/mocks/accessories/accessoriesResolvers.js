import accessoriesData from './accessoriesData';

const getAllAccessories = (req, res, ctx) => {
  return res(
    ctx.status(200),
    ctx.json({
      type: 'Success',
      message: 'Get data success',
      content: accessoriesData,
    })
  );
};

export { getAllAccessories };
