import Chonky from '../../../assets/img/Chonky.svg';
import Kevin from '../../../assets/img/Kevin.svg';
import Som from '../../../assets/img/Som.svg';
import Terry from '../../../assets/img/Terry.svg';

const petsData = [
  {
    id: 1,
    petName: 'Chonky',
    petImg: Chonky,
    petPrice: 500,
  },
  {
    id: 2,
    petName: 'Kevin',
    petImg: Kevin,
    petPrice: 1000,
  },
  {
    id: 3,
    petName: 'Som',
    petImg: Som,
    petPrice: 1500,
  },
  {
    id: 4,
    petName: 'Terry',
    petImg: Terry,
    petPrice: 1600,
  },
];

export default petsData;
