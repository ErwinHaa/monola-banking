import { rest } from 'msw';

import { getAllPets } from './petsResolvers';

const petHandlers = [rest.get('/pet', getAllPets)];

export default petHandlers;
