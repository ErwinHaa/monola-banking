import { ReactComponent as GOPAY } from '../../assets/img/Gopay.svg';
import { ReactComponent as DANA } from '../../assets/img/DANA.svg';
import { ReactComponent as OVO } from '../../assets/img/OVO.svg';
import { ReactComponent as LinkAja } from '../../assets/img/Linkaja.svg';

import { ReactComponent as BCA } from '../../assets/img/BCA.svg';
import { ReactComponent as Permata } from '../../assets/img/Permata.svg';
import { ReactComponent as BNI } from '../../assets/img/BNI.svg';
import { ReactComponent as CIMBNiaga } from '../../assets/img/CIMB.svg';
import { ReactComponent as Mandiri } from '../../assets/img/Mandiri.svg';
import { ReactComponent as BTPN } from '../../assets/img/BTPN.svg';
import { ReactComponent as BRI } from '../../assets/img/BRI.svg';
import { ReactComponent as Jenius } from '../../assets/img/Jenius.svg';

const getVendorComponent = (userVendor) => {
  const vendors = {
    GOPAY,
    DANA,
    OVO,
    LinkAja,
    BCA,
    Permata,
    BNI,
    CIMBNiaga,
    Mandiri,
    BTPN,
    BRI,
    Jenius,
  };

  return vendors[userVendor];
};

export default getVendorComponent;
