const setUserToken = (token) => {
  return localStorage.setItem('userToken', token);
};

const getUserToken = () => {
  return localStorage.getItem('userToken');
};

const setUserId = (userId) => localStorage.setItem('userId', userId);

const getUserId = () => localStorage.getItem('userId');

export { setUserToken, getUserToken, setUserId, getUserId };
