import { ReactComponent as Ambulance } from '../../assets/img/ambulance.svg';
import { ReactComponent as Plane } from '../../assets/img/airplane.svg';
import { ReactComponent as Car } from '../../assets/img/car.svg';
import { ReactComponent as GradCap } from '../../assets/img/gradcap.svg';
import { ReactComponent as Camera } from '../../assets/img/camera.svg';
import { ReactComponent as Paint } from '../../assets/img/paint.svg';

const renderSavingIcon = () => {
  const icons = [
    {
      id: 1,
      Component: Ambulance,
      goalIcon: 'Ambulance'
    },
    {
      id: 2,
      Component: Plane,
      goalIcon: 'Plane'
    },
    {
      id: 3,
      Component: Car,
      goalIcon: 'Car'
    },
    {
      id: 4,
      Component: GradCap,
      goalIcon: 'GradCap'
    },
    {
      id: 5,
      Component: Camera,
      goalIcon: 'Camera'
    },
    {
      id: 6,
      Component: Paint,
      goalIcon: 'Paint'
    }
  ]
  return icons;
}

export default renderSavingIcon;