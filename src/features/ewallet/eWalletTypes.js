const fetchFavoriteEWallet = {
  pending: 'eWallet/fetchFavoriteEWalletPending',
  fulfilled: 'eWallet/fetchFavoriteEWalletFulfilled',
  rejected: 'eWallet/fetchFavoriteEWalletRejected',
};

const addEWalletToFavorite = {
  fulfilled: 'eWallet/addEWalletToFavoriteFulfilled',
};

const fetchEWalletByPhoneNumber = {
  pending: 'eWallet/fetchEWalletByPhoneNumberPending',
  fulfilled: 'eWallet/fetchEWalletByPhoneNumberFulfilled',
  rejected: 'eWallet/fetchEWalletByPhoneNumberRejected',
};

const fetchFavoriteEWalletByName = {
  pending: 'eWallet/fetchEWalletByNamePending',
  fulfilled: 'eWallet/fetchEWalletByNameFulfilled',
  rejected: 'eWallet/fetchEWalletByNameRejected',
};

const topUpEWallet = {
  pending: 'eWallet/topUpEWalletPending',
  fulfilled: 'eWallet/topUpEWalletFulfilled',
  rejected: 'eWallet/topUpEWalletRejected',
};

const setEWalletRecipient = 'eWallet/setEWalletRecipient';

const setEditingEWallet = 'eWallet/setEditingEWallet';

const resetEWalletRecipient = 'eWallet/resetEWalletRecipient';

const saveTopUpInfo = 'eWallet/saveTopUpInfo';

const saveNewEwalletName = 'eWallet/saveNewEwalletName';

/**
 * Resets
 */

const resetSavedInfos = 'eWallet/resetSavedInfos';

const resetTopUpStatus = 'eWallet/resetTopUpStatus';

const resetFavoriteEWalletFetchStatus =
  'eWallet/resetFavoriteEWalletFetchStatus';

const resetEWalletRecipientSearchStatus =
  'eWallet/resetEWalletRecipientSearchStatus';

export {
  fetchFavoriteEWallet,
  addEWalletToFavorite,
  resetEWalletRecipient,
  fetchEWalletByPhoneNumber,
  fetchFavoriteEWalletByName,
  topUpEWallet,
  setEWalletRecipient,
  setEditingEWallet,
  saveTopUpInfo,
  resetSavedInfos,
  saveNewEwalletName,
  resetTopUpStatus,
  resetFavoriteEWalletFetchStatus,
  resetEWalletRecipientSearchStatus,
};
