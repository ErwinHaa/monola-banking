import * as types from './eWalletTypes';

const fetchUserFavoriteEWallet = {
  pending() {
    return { type: types.fetchFavoriteEWallet.pending };
  },
  fulfilled(favoriteEWallets) {
    return {
      type: types.fetchFavoriteEWallet.fulfilled,
      payload: favoriteEWallets,
    };
  },
  rejected() {
    return { type: types.fetchFavoriteEWallet.rejected };
  },
};

const fetchEWalletByPhoneNumber = {
  pending() {
    return { type: types.fetchEWalletByPhoneNumber.pending };
  },
  fulfilled(recipient) {
    return {
      type: types.fetchEWalletByPhoneNumber.fulfilled,
      payload: recipient,
    };
  },
  rejected() {
    return { type: types.fetchEWalletByPhoneNumber.rejected };
  },
};

const fetchFavoriteEWalletByName = {
  pending() {
    return { type: types.fetchFavoriteEWalletByName.pending };
  },
  fulfilled(ewallet) {
    return {
      type: types.fetchFavoriteEWalletByName.fulfilled,
      payload: ewallet,
    };
  },
  rejected() {
    return { type: types.fetchFavoriteEWalletByName.rejected };
  },
};

const addEWalletToFavorite = (favoriteEWallet) => {
  return {
    type: types.addEWalletToFavorite.fulfilled,
    payload: favoriteEWallet,
  };
};

const setEWalletRecipient = (recipientObj) => {
  return { type: types.setEWalletRecipient, payload: recipientObj };
};

const setEditingEWallet = (eWallet) => {
  return { type: types.setEditingEWallet, payload: eWallet };
};

const topUpEWallet = {
  pending() {
    return { type: types.topUpEWallet.pending };
  },
  fulfilled() {
    return { type: types.topUpEWallet.fulfilled };
  },
  rejected(errMsg) {
    return { type: types.topUpEWallet.rejected, error: errMsg };
  },
};

const resetEWalletRecipient = () => ({ type: types.resetEWalletRecipient });

const saveTopUpInfo = (topUpInfo) => ({
  type: types.saveTopUpInfo,
  payload: topUpInfo,
});

const resetSavedInfos = () => ({ type: types.resetSavedInfos });

const saveNewEwalletName = (name, phoneNumber) => ({
  type: types.saveNewEwalletName,
  payload: { name, phoneNumber },
});

const resetTopUpStatus = () => ({ type: types.resetTopUpStatus });

const resetFavoriteEWalletFetchStatus = () => ({
  type: types.resetFavoriteEWalletFetchStatus,
});

const resetEWalletRecipientSearchStatus = () => ({
  type: types.resetEWalletRecipientSearchStatus,
});

export {
  fetchUserFavoriteEWallet,
  resetEWalletRecipient,
  fetchEWalletByPhoneNumber,
  fetchFavoriteEWalletByName,
  addEWalletToFavorite,
  topUpEWallet,
  setEWalletRecipient,
  setEditingEWallet,
  saveTopUpInfo,
  resetSavedInfos,
  saveNewEwalletName,
  resetTopUpStatus,
  resetFavoriteEWalletFetchStatus,
  resetEWalletRecipientSearchStatus,
};
