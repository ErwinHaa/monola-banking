import { createSelector } from 'reselect';
import _ from 'lodash';

import { limitData } from '../../services/helpers';

/**
 * Select Favorite eWallets
 */

const selectFavoriteEwallets = (state) => state.eWallet.favoriteEWallets;

const selectUniqueFavoriteEwallets = createSelector(
  selectFavoriteEwallets,
  (favoriteEwallets) => {
    if (favoriteEwallets.length) {
      return _.uniqBy(favoriteEwallets, 'identityNumber');
    }

    return favoriteEwallets;
  }
);

const selectLimitedFavoriteEwallets = createSelector(
  selectUniqueFavoriteEwallets,
  (uniqueFavoriteEwallets) => {
    if (uniqueFavoriteEwallets.length) {
      return limitData(uniqueFavoriteEwallets, 4);
    }

    return uniqueFavoriteEwallets;
  }
);

const selectFilteredFavoriteEwallets = createSelector(
  [selectLimitedFavoriteEwallets, (_, filter) => filter],
  (favoriteEwallets, filter) => {
    if (filter) {
      return favoriteEwallets.filter((ewallet) =>
        ewallet.userName.toLowerCase().includes(filter.toLowerCase())
      );
    }

    return favoriteEwallets;
  }
);

const selectFavoriteEWalletById = createSelector(
  [selectUniqueFavoriteEwallets, (_, eWalletId) => eWalletId],
  (eWallets, eWalletId) => {
    if (eWallets.length) {
      return eWallets.find((ewallet) => ewallet.id === eWalletId);
    }

    return {};
  }
);

const selectEWalletRecipient = (state) => state.eWallet.eWalletRecipient;

const selectEWalletSearchError = (state) => state.eWallet.eWalletSearchError;

const selectEWalletSearchStatus = (state) => state.eWallet.eWalletSearchStatus;

const selectEWalletFetchStatus = (state) => state.eWallet.eWalletsFetchStatus;

const selectEWalletRecipientFetchStatus = (state) =>
  state.eWallet.eWalletSearchStatus;

const selectEditingEWallet = (state) => state.eWallet.eWalletOnEdit;

const selectTopUpErrorMsg = (state) => state.eWallet.topUpErrorMessage;

export {
  selectFavoriteEwallets,
  selectEWalletRecipient,
  selectEWalletSearchError,
  selectUniqueFavoriteEwallets,
  selectFilteredFavoriteEwallets,
  selectLimitedFavoriteEwallets,
  selectFavoriteEWalletById,
  selectEWalletFetchStatus,
  selectEWalletRecipientFetchStatus,
  selectEditingEWallet,
  selectTopUpErrorMsg,
  selectEWalletSearchStatus,
};
