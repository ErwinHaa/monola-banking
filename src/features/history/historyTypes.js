const fetchHistoryTransaction = {
  pending: 'history/fetchHistoryTransactionPending',
  fulfilled: 'history/fetchHistoryTransactionFulfilled',
  rejected: 'history/fetchHistoryTransactionRejected',
};

export { fetchHistoryTransaction };