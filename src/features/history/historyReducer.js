import * as types from './historyTypes';
import { userTypes } from '../user';

import { IDLE, PENDING, FULFILLED, REJECTED } from '../../services/enums';

const INITIAL_STATE = {
  histories: [],
  userHistoriesFetchingStatus: IDLE,
  userHistoriesFetchingError: false,
};

const historyReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.fetchHistoryTransaction.pending: {
      return {
        ...state,
        histories: [],
        userHistoriesFetchingStatus: PENDING,
        userHistoriesFetchingError: false,
      };
    }
    case types.fetchHistoryTransaction.fulfilled: {
      const userHistoriesTransaction = action.payload;
      return {
        ...state,
        histories: userHistoriesTransaction,
        userHistoriesFetchingStatus: FULFILLED,
      };
    }
    case types.fetchHistoryTransaction.rejected: {
      return {
        ...state,
        userHistoriesFetchingStatus: REJECTED,
        userHistoriesFetchingError: true,
      };
    }

    case userTypes.logout: {
      return INITIAL_STATE;
    }

    default:
      return state;
  }
};

export default historyReducer;
