import { createSelector } from 'reselect';
import _ from 'lodash';

import { limitData } from '../../services/helpers';

const selectUserHistories = (state) => state.history.histories;

const selectUserHistoriesFetchingStatus = (state) =>
  state.history.userHistoriesFetchingStatus;

const selectTransactionHistories = createSelector(
  (state) => state.history.histories,
  (transactions) =>
    _.orderBy(
      transactions.map((transaction) => ({
        date: transaction.date,
        transfers: limitData(transaction.transfers, 5),
      })),
      ['date'],
      ['desc']
    )
);

export {
  selectUserHistories,
  selectUserHistoriesFetchingStatus,
  selectTransactionHistories,
};
