import * as api from './historyAPI';
import * as actions from './historyActions';

const fetchHistoryTransaction = () => async (dispatch, getState) => {
  const userId = getState().user.user.userId;

  dispatch(actions.fetchHistoryTransaction.pending());

  try {
    const { content } = await api.fetchHistoryTransaction(userId);

    dispatch(actions.fetchHistoryTransaction.fulfilled(content));
  } catch (err) {
    dispatch(actions.fetchHistoryTransaction.rejected());
  }
}

export { fetchHistoryTransaction };