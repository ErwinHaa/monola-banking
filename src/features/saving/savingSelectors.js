import { createSelector } from 'reselect';

const selectUserSavings = (state) => state.saving.savings;

const selectUserSavingsById = createSelector(
  [selectUserSavings, (_, savingId) => savingId],
  (savings, savingId) => savings.find((saving) => saving.id === savingId)
);

const selectManagedSaving = (state) => state.saving.managedSaving;

const selectUserSavingsFetchingStatus = (state) =>
  state.saving.userSavingsFetchingStatus;

const selectAddNewSavingGoalStatus = (state) =>
  state.saving.addNewSavingGoalStatus;

const selectManagedSavingFetchingStatus = (state) =>
  state.saving.managedSavingFetchingStatus;

export {
  selectUserSavings,
  selectManagedSaving,
  selectUserSavingsById,
  selectUserSavingsFetchingStatus,
  selectAddNewSavingGoalStatus,
  selectManagedSavingFetchingStatus,
};
