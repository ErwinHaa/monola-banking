import * as types from './savingTypes';

const fetchSaving = {
  pending() {
    return { type: types.fetchSaving.pending };
  },
  fullfilled(saving) {
    return {
      type: types.fetchSaving.fulfilled,
      payload: saving,
    };
  },
  rejected() {
    return { type: types.fetchSaving.rejected };
  },
};

const setSavingItem = (savingItem) => ({
  type: types.setSavingItem,
  payload: savingItem,
});

const resetSaving = () => ({ type: types.resetSaving });

const updateSavings = {
  pending() {
    return { type: types.updateSavings.pending };
  },
  fulfilled(newSaving) {
    return { type: types.updateSavings.fulfilled, payload: newSaving };
  },
  rejected() {
    return { type: types.updateSavings.rejected };
  },
};

const getAndSetManagedSaving = {
  pending() {
    return { type: types.getAndSetManagedSaving.pending };
  },
  fulfilled(savings, savingId) {
    return {
      type: types.getAndSetManagedSaving.fulfilled,
      payload: { savings, savingId },
    };
  },
  rejected() {
    return { type: types.getAndSetManagedSaving.rejected };
  },
};

export {
  fetchSaving,
  setSavingItem,
  resetSaving,
  updateSavings,
  getAndSetManagedSaving,
};
