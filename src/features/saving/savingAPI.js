import monola from '../../services/apis/monola';

export const getSavingByUserId = async (userId) => {
  const response = await monola.get('/savings', {params: { userId }});

  return response.data;
};

export const postNewSaving = async (infoObj) => {
  const response = await monola.post('/savings/goals', { ...infoObj });

  return response.data;
}

export const postTopupSaving = async (infoObj) => {
  const response = await monola.post('/savings/top-up', { ...infoObj });

  return response.data;
};

export const postWithdrawSaving = async (infoObj) => {
  const response = await monola.post('/savings/withdraw', { ...infoObj });
  
  return response.data;
}