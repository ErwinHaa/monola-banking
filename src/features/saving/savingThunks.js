import * as api from './savingAPI';
import * as actions from './savingActions';
import { balanceActions } from '../balance';
import { userThunks } from '../user';

const fetchSaving = () => async (dispatch, getState) => {
  const userId = getState().user.user.userId;

  dispatch(actions.fetchSaving.pending());

  try {
    const { content } = await api.getSavingByUserId(userId);

    dispatch(actions.fetchSaving.fullfilled(content));
    dispatch(userThunks.fetchUserCurrentVariant());
  } catch (err) {
    dispatch(actions.fetchSaving.rejected());
  }
};

const topUpSavingGoal = (amountTopup) => async (dispatch, getState) => {
  const goalTopupId = getState().saving.managedSaving.id;
  const topupUserId = getState().user.user.userId;

  const infoObjTopup = {
    amount: amountTopup,
    goalId: goalTopupId,
    userId: topupUserId,
  };

  try {
    await api.postTopupSaving(infoObjTopup);

    dispatch(balanceActions.deductUserBalance(infoObjTopup.amount));

    dispatch(actions.resetSaving());
    dispatch(fetchSaving());
  } catch (err) {
    console.error(err.message);
  }
};

const withdrawSavingGoal = (amountWithdraw) => async (dispatch, getState) => {
  const goalWithdrawId = getState().saving.managedSaving.id;
  const withdrawUserId = getState().user.user.userId;

  const infoObj = {
    amount: amountWithdraw,
    goalId: goalWithdrawId,
    userId: withdrawUserId,
  };

  try {
    await api.postWithdrawSaving(infoObj);
    dispatch(balanceActions.addUserBalance(infoObj.amount));

    dispatch(actions.resetSaving());
    dispatch(fetchSaving());
  } catch (err) {
    console.error(err.message);
  }
};

const createNewSaving =
  (newSavingGoalName, newSavingAmount, newGoalIcon) =>
  async (dispatch, getState) => {
    dispatch(actions.updateSavings.pending());
    const userId = getState().user.user.userId;

    const infoObj = {
      amount: +newSavingAmount,
      goalName: newSavingGoalName,
      goalIcon: newGoalIcon,
      userId,
    };

    try {
      const { content } = await api.postNewSaving(infoObj);

      if (content) {
        const infoObjNewSaving = {
          id: content.id,
          goalName: content.goalName,
          amount: content.amount,
          goalIcon: content.goalIcon,
          userId,
        };

        dispatch(balanceActions.deductUserBalance(infoObj.amount));
        dispatch(userThunks.fetchUserCurrentVariant());
        dispatch(actions.updateSavings.fulfilled(infoObjNewSaving));
      }
    } catch (err) {
      dispatch(actions.updateSavings.rejected());
    }
  };

const getAndSetManagedSaving = (savingId) => async (dispatch, getState) => {
  const userId = getState().user.user.userId;
  dispatch(actions.getAndSetManagedSaving.pending());

  try {
    const { content } = await api.getSavingByUserId(userId);

    dispatch(actions.getAndSetManagedSaving.fulfilled(content, savingId));
  } catch (err) {
    dispatch(actions.getAndSetManagedSaving.rejected());
  }
};

export {
  fetchSaving,
  topUpSavingGoal,
  withdrawSavingGoal,
  createNewSaving,
  getAndSetManagedSaving,
};
