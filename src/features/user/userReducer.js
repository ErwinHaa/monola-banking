import * as types from './userTypes';
import { petTypes } from '../pet';

import { IDLE, PENDING, FULFILLED, REJECTED } from '../../services/enums';

const INITIAL_STATE = {
  user: {},
  userProfile: {},
  userPets: [],
  userAccessories: [],
  userInventory: [],
  currentUserPet: {},
  currentDisplayedPetVariant: {},
  isLoggedIn: false,
  fetchUserStatus: IDLE,
  fetchUserProfileStatus: IDLE,
  loginStatus: IDLE,
  fetchUserCoinsStatus: IDLE,
  fetchUserPetsStatus: IDLE,
  fetchUserCurrentPetStatus: IDLE,
  fetchUserAccessoriesStatus: IDLE,
  fetchUserVariantStatus: IDLE,
  loginError: false,
  fetchUserError: false,
  fetchUserProfileError: false,
  fetchUserCoinsError: false,
  fetchUserPetsError: false,
  fetchUserCurrentPetError: false,
  fetchUserVariantError: false,
  fetchUserAccessoriesError: false,
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.login.pending: {
      return {
        ...state,
        loginStatus: PENDING,
        isLoggedIn: false,
        loginError: false,
      };
    }

    case types.login.fulfilled: {
      const userId = action.payload;
      return {
        ...state,
        loginStatus: FULFILLED,
        isLoggedIn: true,
        user: { ...state.user, userId },
      };
    }

    case types.login.rejected: {
      return { ...state, loginStatus: REJECTED, loginError: true };
    }

    case types.fetchUserProfile.pending: {
      return {
        ...state,
        userProfile: {},
        fetchUserProfileStatus: PENDING,
        fetchUserProfileError: false,
      };
    }

    case types.fetchUserProfile.fulfilled: {
      const userProfile = action.payload;
      return { ...state, userProfile, fetchUserAccessoriesStatus: FULFILLED };
    }

    case types.fetchUserProfile.rejected: {
      return {
        ...state,
        fetchUserProfileStatus: REJECTED,
        fetchUserProfileError: true,
      };
    }

    case types.setUserId: {
      const userId = action.payload;

      return { ...state, isLoggedIn: true, user: { ...state.user, userId } };
    }

    case types.logout: {
      return INITIAL_STATE;
    }

    case types.fetchUserData.pending: {
      return {
        ...state,
        fetchUserStatus: PENDING,
        fetchUserError: false,
        user: {},
      };
    }

    case types.fetchUserData.fulfilled: {
      const userData = action.payload;
      return { ...state, fetchUserStatus: FULFILLED, user: userData };
    }

    case types.fetchUserData.rejected: {
      return { ...state, fetchUserStatus: REJECTED, fetchUserError: true };
    }

    case types.fetchUserCoins.pending: {
      return {
        ...state,
        fetchUserCoinsStatus: PENDING,
        fetchUserCoinsError: false,
      };
    }

    case types.fetchUserCoins.fulfilled: {
      const userCoins = action.payload;

      return {
        ...state,
        fetchUserCoinsStatus: FULFILLED,
        user: { ...state.user, userCoin: userCoins },
      };
    }

    case types.fetchUserCoins.rejected: {
      return {
        ...state,
        fetchUserCoinsStatus: REJECTED,
        fetchUserCoinsError: true,
      };
    }

    case types.fetchUserPets.pending: {
      return {
        ...state,
        userPets: [],
        fetchUserPetsStatus: PENDING,
        fetchUserPetsError: false,
      };
    }

    case types.fetchUserPets.fulfilled: {
      const userPets = action.payload.map(
        ({ userPetId, userId, petId, petName, petImage, isChosen }) => ({
          userPetId,
          userId,
          id: petId,
          itemName: petName,
          itemImg: petImage,
          isChosen,
        })
      );

      return { ...state, userPets, fetchUserPetsStatus: FULFILLED };
    }

    case types.fetchUserPets.rejected: {
      return {
        ...state,
        fetchUserPetsStatus: REJECTED,
        fetchUserPetsError: true,
      };
    }

    case types.fetchUserCurrentPet.pending: {
      return {
        ...state,
        currentUserPet: {},
        fetchUserCurrentPetStatus: PENDING,
        fetchUserCurrentPetError: false,
      };
    }

    case types.fetchUserCurrentPet.fulfilled: {
      const currentUserPet = action.payload;

      return {
        ...state,
        currentUserPet,
        fetchUserCurrentPetStatus: FULFILLED,
      };
    }

    case types.fetchUserCurrentPet.rejected: {
      return {
        ...state,
        fetchUserCurrentPetStatus: REJECTED,
        fetchUserCurrentPetError: true,
      };
    }

    case types.fetchUserAccessories.pending: {
      return {
        ...state,
        userAccessories: [],
        fetchUserAccessoriesStatus: PENDING,
        fetchUserAccessoriesError: false,
      };
    }

    case types.fetchUserAccessories.fulfilled: {
      const userAccessories = action.payload
        .map((accessory) => ({
          userAccessoryId: accessory.userAccessoryId,
          userId: accessory.userAccessoryId,
          id: accessory.accessoryId,
          itemName: accessory.accessoryName,
          itemImg: accessory.accessoryImage,
          isChosen: accessory.isChosen,
        }))
        .filter((accessory) => accessory.itemImg);

      return {
        ...state,
        userAccessories,
        fetchUserAccessoriesStatus: FULFILLED,
      };
    }

    case types.fetchUserAccessories.rejected: {
      return {
        ...state,
        fetchUserAccessoriesStatus: REJECTED,
        fetchUserAccessoriesError: true,
      };
    }

    case types.setUserInventory: {
      const { pets, accessories } = action.payload;
      return { ...state, userInventory: [...pets, ...accessories] };
    }

    case petTypes.purchasePet.fulfilled: {
      return {
        ...state,
        fetchUserPetsStatus: IDLE,
        fetchUserCurrentPetStatus: IDLE,
      };
    }

    case types.fetchUserCurrentVariant.pending: {
      return {
        ...state,
        currentDisplayedPetVariant: {},
        fetchUserVariantStatus: PENDING,
        fetchUserVariantError: false,
      };
    }

    case types.fetchUserCurrentVariant.fulfilled: {
      const userCurrentVariant = action.payload;

      return {
        ...state,
        currentDisplayedPetVariant: userCurrentVariant,
        fetchUserVariantStatus: FULFILLED,
      };
    }

    case types.fetchUserCurrentVariant.rejected: {
      return {
        ...state,
        fetchUserVariantStatus: REJECTED,
        fetchUserVariantError: true,
      };
    }

    case types.switchUserCurrentVariant: {
      const updatedVariant = action.payload;

      return {
        ...state,
        currentDisplayedPetVariant: updatedVariant,
      };
    }

    case types.changeUserPetName: {
      const newPetName = action.payload;

      return {
        ...state,
        currentDisplayedPetVariant: {
          ...state.currentDisplayedPetVariant,
          userPetName: newPetName,
        },
      };
    }

    // case petTypes.purchasePet.fulfilled: {
    //   const purchasedPet = action.payload;

    //   if(purchasedPet.isChosen) {
    //     return {
    //       ...state,
    //       currentUserPet:
    //     }
    //   }
    // }

    default:
      return state;
  }
};

export default userReducer;
