import * as apis from './userAPI';
import * as actions from './userActions';

import { localStorageHelpers } from '../../services/helpers';

const logUserIn = (userCredentials) => async (dispatch) => {
  dispatch(actions.login.pending());

  try {
    const { content } = await apis.login(userCredentials);
    localStorageHelpers.setUserToken(content.token);
    localStorageHelpers.setUserId(content.userId);

    dispatch(actions.login.fulfilled(content.userId));
  } catch (err) {
    dispatch(actions.login.rejected());
  }
};

const fetchUserData = () => async (dispatch, getState) => {
  const userId = getState().user.user.userId;
  // const userId = localStorageHelpers.getUserToken();
  dispatch(actions.fetchUserData.pending());

  try {
    const { content } = await apis.fetchUserData(userId);

    dispatch(actions.fetchUserData.fulfilled(content.user));
    dispatch(actions.setUserBalance(content.balance));
  } catch (err) {
    dispatch(actions.fetchUserData.rejected());
  }
};

const fetchUserProfile = () => async (dispatch, getState) => {
  const userId = getState().user.user.userId;

  dispatch(actions.fetchUserProfile.rejected());

  try {
    const { content } = await apis.fetchUserProfile(userId);

    dispatch(actions.fetchUserProfile.fulfilled(content));
  } catch (err) {
    dispatch(actions.fetchUserProfile.rejected());
  }
};

const fetchUserCoins = () => async (dispatch, getState) => {
  const userId = getState().user.user.userId;
  dispatch(actions.fetchUserCoins.pending());

  try {
    const { content } = await apis.fetchUserCoins(userId);

    dispatch(actions.fetchUserCoins.fulfilled(content.userCoin));
  } catch (err) {
    dispatch(actions.fetchUserCoins.rejected());
  }
};

const fetchUserPets = () => async (dispatch, getState) => {
  const userId = getState().user.user.userId;

  dispatch(actions.fetchUserPets.pending());

  try {
    const { content } = await apis.fetchUserPets(userId);

    dispatch(actions.fetchUserPets.fulfilled(content));
  } catch (err) {
    dispatch(actions.fetchUserPets.rejected());
  }
};

const fetchUserCurrentPet = () => async (dispatch, getState) => {
  const userId = getState().user.user.userId;

  dispatch(actions.fetchUserCurrentPet.pending());

  try {
    const { content } = await apis.fetchUserCurrentPet(userId);

    dispatch(actions.fetchUserCurrentPet.fulfilled(content));
  } catch (err) {
    dispatch(actions.fetchUserCurrentPet.rejected());
  }
};

const fetchUserCurrentVariant = () => async (dispatch, getState) => {
  const userId = getState().user.user.userId;
  dispatch(actions.fetchUserCurrentVariant.pending());

  try {
    const { content } = await apis.fetchUserCurrentVariant(userId);

    dispatch(actions.fetchUserCurrentVariant.fulfilled(content));
  } catch (err) {
    dispatch(actions.fetchUserCurrentVariant.rejected());
  }
};

const fetchUserAccessories = () => async (dispatch, getState) => {
  const userId = getState().user.user.userId;
  dispatch(actions.fetchUserAccessories.pending());

  try {
    const { content } = await apis.fetchUserAccessories(userId);

    dispatch(actions.fetchUserAccessories.fulfilled(content));
  } catch (err) {
    dispatch(actions.fetchUserAccessories.rejected());
  }
};

const switchUserCurrentVariant = (updateObj) => async (dispatch, getState) => {
  const userId = getState().user.user.userId;

  try {
    const { content } = await apis.updateUserCurrentVariant(userId, updateObj);

    dispatch(actions.switchUserCurrentVariant(content));

    dispatch(fetchUserCurrentPet());
    dispatch(fetchUserPets());
    dispatch(fetchUserAccessories());
  } catch (err) {}
};

const switchUserDisplayedPet = (petId) => async (dispatch, getState) => {
  const userId = getState().user.user.userId;
  const currentPetId = getState().user.currentUserPet.petId;

  if (currentPetId === '512004e2-5364-4f54-97ad-60ba4c330d2e') {
    await apis.switchAccessory('e267eea2-88fc-4ab6-96c1-c121fb91d9d8', userId);
  }

  try {
    await apis.updateUserPet(petId, userId);

    const updateObj = {
      petId,
    };

    dispatch(switchUserCurrentVariant(updateObj));
  } catch (err) {}
};

const switchUserPetEquippedAccessory =
  (accId) => async (dispatch, getState) => {
    const userId = getState().user.user.userId;

    try {
      await apis.switchAccessory(accId, userId);

      const updateObj = {
        accessoryId: accId,
      };

      dispatch(switchUserCurrentVariant(updateObj));
    } catch (err) {}
  };

const purchaseAccessory = (accId) => async (dispatch, getState) => {
  const userId = getState().user.user.userId;

  const purchaseObj = {
    accessoryId: accId,
    isChosen: true,
    userId,
  };

  const updateObj = {
    accessoryId: accId,
  };

  dispatch(actions.purchaseAccessory.pending());
  try {
    await apis.purchaseAccessories(purchaseObj);

    // await apis.updateUserCurrentVariant(userId, updateObj);

    dispatch(switchUserCurrentVariant(updateObj));
    dispatch(fetchUserCurrentVariant());
    dispatch(fetchUserCoins());
    dispatch(actions.purchaseAccessory.fulfilled());
  } catch (err) {
    dispatch(actions.purchaseAccessory.rejected());
  }
};

const switchUserAccessory = (accId) => async (dispatch, getState) => {
  const userId = getState().user.user.userId;

  try {
    await apis.switchAccessory(accId, userId);
  } catch (err) {}
};

const changeUserPetName = (newPetName) => async (dispatch, getState) => {
  const userId = getState().user.user.userId;

  const updateObj = {
    userPetName: newPetName,
  };

  try {
    await apis.changeUserPetName(updateObj, userId);

    dispatch(actions.changeUserPetName(newPetName));
  } catch (err) {}
};

export {
  logUserIn,
  fetchUserData,
  fetchUserProfile,
  fetchUserCoins,
  fetchUserPets,
  fetchUserCurrentPet,
  fetchUserCurrentVariant,
  fetchUserAccessories,
  switchUserCurrentVariant,
  switchUserDisplayedPet,
  purchaseAccessory,
  switchUserAccessory,
  switchUserPetEquippedAccessory,
  changeUserPetName,
};
