const login = {
  pending: 'user/loginPending',
  fulfilled: 'user/loginFulfilled',
  rejected: 'user/loginRejected',
};

const logout = 'user/logout';

const setUserBalance = 'user/setUserBalance';

const fetchUserData = {
  pending: 'user/fetchUserDataPending',
  fulfilled: 'user/fetchUserDataFulfilled',
  rejected: 'user/fetchUserDataRejected',
};

const fetchUserProfile = {
  pending: 'user/fetchUserProfilePending',
  fulfilled: 'user/fetchUserProfileFulfilled',
  rejected: 'user/fetchUserProfileRejected',
};

const fetchUserCoins = {
  pending: 'user/fetchUserCoinsPending',
  fulfilled: 'user/fetchUserCoinsFulfilled',
  rejected: 'user/fetchUserCoinsRejected',
};

const fetchUserPets = {
  pending: 'user/fetchUserPetsPending',
  fulfilled: 'user/fetchUserPetsFulfilled',
  rejected: 'user/fetchUserPetsRejected',
};

const fetchUserAccessories = {
  pending: 'user/fetchUserAccessoriesPending',
  fulfilled: 'user/fetchUserAccessoriesFulfilled',
  rejected: 'user/fetchUserAccessoriesRejected',
};

const fetchUserCurrentPet = {
  pending: 'user/fetchUserCurrentPetPending',
  fulfilled: 'user/fetchUserCurrentPetFulfilled',
  rejected: 'user/fetchUserCurrentPetRejected',
};

const fetchUserCurrentVariant = {
  pending: 'user/fetchUserCurrentVariantPending',
  fulfilled: 'user/fetchUserCurrentVariantFulfilled',
  rejected: 'user/fetchUserCurrentVariantRejected',
};

const switchUserCurrentVariant = 'user/switchUserCurrentVariant';

const purchaseAccessory = {
  pending: 'user/purchaseAccessoryPending',
  fulfilled: 'user/purchaseAccessoryFulfilled',
  rejected: 'user/purchaseAccessoryRejected',
};

const changeUserPetName = 'user/changeUserPetName';

const setUserInventory = 'user/setUserInventory';

const setUserId = 'user/setUserId';

export {
  login,
  logout,
  setUserBalance,
  fetchUserData,
  fetchUserProfile,
  fetchUserCoins,
  fetchUserPets,
  fetchUserAccessories,
  fetchUserCurrentPet,
  fetchUserCurrentVariant,
  switchUserCurrentVariant,
  setUserInventory,
  purchaseAccessory,
  changeUserPetName,
  setUserId,
};
