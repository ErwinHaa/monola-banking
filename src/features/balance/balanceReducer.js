import * as types from './balanceTypes';
import { userTypes } from '../user';

import { IDLE, PENDING, FULFILLED, REJECTED } from '../../services/enums';

const INITIAL_STATE = {
  userBalance: 0,
  status: IDLE,
};

const balanceReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.fetchUserBalance.pending: {
      return { ...state, status: PENDING, userBalance: 0 };
    }

    case types.fetchUserBalance.fulfilled: {
      const userBalance = action.payload;
      return { ...state, status: FULFILLED, userBalance };
    }

    case types.fetchUserBalance.rejected: {
      return { ...state, status: REJECTED };
    }

    case userTypes.setUserBalance: {
      const userBalance = action.payload;
      return { ...state, userBalance };
    }

    case types.deductUserBalance: {
      const deductionAmount = action.payload;
      const newBalance = state.userBalance - deductionAmount;
      return { ...state, userBalance: newBalance };
    }
    case types.addUserBalance:
      const addAmount = action.payload;
      const newBalanceAfterAdd = state.userBalance + addAmount;
      return { ...state, userBalance: newBalanceAfterAdd };

    case userTypes.logout: {
      return INITIAL_STATE;
    }

    default:
      return state;
  }
};

export default balanceReducer;
