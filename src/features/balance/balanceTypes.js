const fetchUserBalance = {
  pending: 'balance/fetchUserBalancePending',
  fulfilled: 'balance/fetchUserBalanceFulfilled',
  rejected: 'balance/fetchUserBalanceRejected',
};

const deductUserBalance = 'balance/deductUserBalance';

const addUserBalance = 'balance/addUserBalance';

export { fetchUserBalance, deductUserBalance, addUserBalance };
