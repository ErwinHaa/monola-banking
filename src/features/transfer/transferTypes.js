const fetchRecipientByAccount = {
  pending: 'transfer/fetchRecipientByAccountPending',
  fulfilled: 'transfer/fetchRecipientByAccountFulfilled',
  rejected: 'transfer/fetchRecipientByAccountRejected',
};

const fetchRecipientById = {
  pending: 'transfer/fetchRecipientByIdPending',
  fulfilled: 'transfer/fetchRecipientByIdFulfilled',
  rejected: 'transfer/fetchRecipientByIdRejected',
};

const fetchRecipientByName = {
  pending: 'transfer/fetchRecipientByNamePending',
  fulfilled: 'transfer/fetchRecipientByNameFulfilled',
  rejected: 'transfer/fetchRecipientByNameRejected',
};

const fetchUserFavoriteTransactions = {
  pending: 'transfer/fetchUserFavoriteTransactionsPending',
  fulfilled: 'transfer/fetchUserFavoriteTransactionsFulfilled',
  rejected: 'transfer/fetchUserFavoriteTransactionsRejected',
};

const setRecipientAsFavorite = 'transfer/setRecipientAsFavorite';

const fetchUserLastTransactions = {
  pending: 'transfer/fetchUserLastTransactionsPending',
  fulfilled: 'transfer/fetchUserLastTransactionsFulfilled',
  rejected: 'transfer/fetchUserLastTransactionsRejected',
};

const resetRecipient = 'transfer/resetRecipient';

const setRecipient = 'transfer/setRecipient';

const saveTransferInformation = 'transfer/saveTransferInformation';

const setIsFavorite = 'transfer/setIsFavorite';

const resetSavedInformations = 'transfer/resetSavedInformations';

const transferCash = {
  pending: 'transfer/transferCashPending',
  fulfilled: 'transfer/transferCashFulfilled',
  rejected: 'transfer/transferCashRejected',
};

const resetTransferStatus = 'transfer/resetTransferStatus';

const setDoesExists = 'transfer/setDoesExits';

const resetFetchLastTransactionsStatus =
  'transfer/resetFetchLastTransactionsStatus';

const resetFetchFavoriteTransactionsStatus =
  'transfer/resetFetchFavoriteTransactionsStatus';

export {
  fetchRecipientByAccount,
  fetchRecipientById,
  fetchRecipientByName,
  fetchUserFavoriteTransactions,
  setRecipientAsFavorite,
  fetchUserLastTransactions,
  setRecipient,
  resetRecipient,
  saveTransferInformation,
  setIsFavorite,
  resetSavedInformations,
  transferCash,
  resetTransferStatus,
  setDoesExists,
  resetFetchLastTransactionsStatus,
  resetFetchFavoriteTransactionsStatus,
};
