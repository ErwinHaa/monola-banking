import reducer from './transferReducer';

import * as transferThunks from './transferThunks';
import * as transferTypes from './transferTypes';
import * as transferSelectors from './transferSelectors';
import * as transferActions from './transferActions';

export { transferThunks, transferTypes, transferSelectors, transferActions };

export default reducer;
