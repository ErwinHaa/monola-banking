import * as api from './transferAPI';

import * as actions from './transferActions';
import { balanceActions } from '../balance';
import { historyThunks } from '../history';
import { userThunks } from '../user';

import { formatObjectData } from '../../services/helpers';

const fetchRecipientByAccount = (accNumber, bankName) => async (dispatch) => {
  dispatch(actions.fetchRecipientByAccount.pending());

  try {
    const { content } = await api.getRecipientByAccountNumber(
      accNumber,
      bankName
    );

    const formattedData = formatObjectData(
      content.userId,
      content.userFullName,
      content.userBankName,
      content.userAccountNumber
    );

    dispatch(actions.fetchRecipientByAccount.fulfilled(formattedData));
  } catch (err) {
    dispatch(actions.fetchRecipientByAccount.rejected());
  }
};

const fetchRecipientById = (recipientId) => async (dispatch) => {
  dispatch(actions.fetchRecipientById.pending());

  try {
    const { content } = await api.getRecipientById(recipientId);

    dispatch(actions.fetchRecipientById.fulfilled(content));
  } catch (err) {
    dispatch(actions.fetchRecipientById.rejected());
  }
};

const fetchRecipientByName = (userFullName) => async (dispatch) => {
  dispatch(actions.fetchRecipientByName.pending());

  try {
    const { content } = await api.getRecipientByName(userFullName);

    dispatch(actions.fetchRecipientByName.fulfilled(content));
  } catch (err) {
    dispatch(actions.fetchRecipientByName.rejected());
  }
};

/**
 * Fetch User Last Transactions
 */

const fetchUserLastTransactions = () => async (dispatch, getState) => {
  const userId = getState().user.user.userId;
  dispatch(actions.fetchUserLastTransactions.pending());

  try {
    const { content } = await api.getUserLastTransactions(userId);

    const formattedData = content.map((transaction) => {
      const format = formatObjectData(
        transaction.id,
        transaction.accountName,
        transaction.bankName,
        transaction.accountNumber
      );

      format.receiverId = transaction.userReceiver;

      return format;
    });

    dispatch(actions.fetchUserLastTransactions.fulfilled(formattedData));
  } catch (err) {
    dispatch(actions.fetchUserLastTransactions.rejected());
  }
};

const fetchUserFavoriteTransactions = () => async (dispatch, getState) => {
  const userId = getState().user.user.userId;
  dispatch(actions.fetchUserFavoriteTransactions.pending());

  try {
    const { content } = await api.getUserFavoriteTransactions(userId);

    const formattedData = content.map((transaction, i) => {
      const format = formatObjectData(
        i + 1,
        transaction.name,
        transaction.bankProvider,
        transaction.accountNumber
      );

      format.receiverId = transaction.userFavoriteId;

      return format;
    });

    dispatch(actions.fetchUserFavoriteTransactions.fulfilled(formattedData));
  } catch (err) {
    dispatch(actions.fetchUserFavoriteTransactions.rejected());
  }
};

const setRecipientAsFavorite = () => async (dispatch, getState) => {
  const recipientName = getState().transfer.recipient.userName;
  const recipientAccNumber = getState().transfer.recipient.identityNumber;
  const userId = getState().user.user.userId;

  const infoObj = {
    accountNumber: recipientAccNumber,
    name: recipientName,
    userId,
  };

  try {
    await api.postFavoriteTransaction(infoObj);

    dispatch(actions.resetFetchFavoriteTransactionsStatus());
    // dispatch(actions.setRecipientAsFavorite());
  } catch (err) {
    console.error(err.message);
  }
};

const transferCash = (infoObj) => async (dispatch) => {
  dispatch(actions.transferCash.pending());

  try {
    await api.postUserTransfer(infoObj);

    dispatch(balanceActions.deductUserBalance(infoObj.amount));
    dispatch(actions.transferCash.fulfilled());
    dispatch(historyThunks.fetchHistoryTransaction());
    dispatch(userThunks.fetchUserCoins());
  } catch (err) {
    const errorMsg = err.response.data.message;
    dispatch(actions.transferCash.rejected(errorMsg));
  }
};

export {
  fetchRecipientByAccount,
  fetchRecipientByName,
  fetchRecipientById,
  fetchUserLastTransactions,
  fetchUserFavoriteTransactions,
  transferCash,
  setRecipientAsFavorite,
};
