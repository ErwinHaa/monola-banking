import { createSelector } from 'reselect';
import _ from 'lodash';

import { limitData } from '../../services/helpers';

const selectRecipient = (state) => state.transfer.recipient;

const selectFetchRecipientStatus = (state) =>
  state.transfer.recipientFetchStatus;

/**
 * Last Transactions
 */

const selectLastTransactions = (state) => state.transfer.lastTransactions;

const selectLimitedLastTransactions = createSelector(
  selectLastTransactions,
  (transactions) => {
    if (transactions.length) {
      return limitData(transactions, 4);
    }

    return [];
  }
);

const selectFilteredLastTransactions = createSelector(
  [selectLimitedLastTransactions, (_, filter) => filter],
  (transactions, filter) => {
    if (transactions.length && filter) {
      return _.uniqBy(
        transactions.filter((transaction) =>
          transaction.userName.toLowerCase().includes(filter.toLowerCase())
        ),
        'accountName'
      );
    }

    return transactions;
  }
);

const selectLastTransactionsById = createSelector(
  [selectLimitedLastTransactions, (_, transactionId) => transactionId],
  (transactions, transactionId) =>
    transactions.find((transaction) => transaction.id === transactionId)
);

/**
 * Favorite Transactions
 */
const selectFavoriteTransactions = (state) =>
  state.transfer.favoriteTransactions;

const selectFilteredFavoriteTransactions = createSelector(
  (state) => state.transfer.favoriteTransactions,
  (favoriteTransactions) => {
    const filteredTransactions = _.uniqBy(favoriteTransactions, 'userName');
    return filteredTransactions;
  }
);

const selectFavoriteTransactionById = createSelector(
  [selectFavoriteTransactions, (_, transactionId) => transactionId],
  (favoriteTransactions, transactionId) =>
    favoriteTransactions.find((transaction) => transaction.id === transactionId)
);

const selectFetchRecipientError = (state) => state.transfer.fetchRecipientError;

const selectLastTransactionsFetchStatus = (state) =>
  state.transfer.lastTransactionsFetchStatus;

const selectFavoriteTransactionsFetchStatus = (state) =>
  state.transfer.favoriteTransactionsFetchStatus;

const selectTransferErrorMsg = (state) => state.transfer.transferErrorMessage;

export {
  selectRecipient,
  selectFetchRecipientStatus,
  selectLastTransactionsFetchStatus,
  selectLastTransactions,
  selectLimitedLastTransactions,
  selectFilteredLastTransactions,
  selectLastTransactionsById,
  selectFavoriteTransactions,
  selectFavoriteTransactionsFetchStatus,
  selectFilteredFavoriteTransactions,
  selectFavoriteTransactionById,
  selectFetchRecipientError,
  selectTransferErrorMsg,
};
