import * as types from './transferTypes';

const setRecipient = (recipient) => ({
  type: types.setRecipient,
  payload: recipient,
});

const fetchRecipientByAccount = {
  pending() {
    return { type: types.fetchRecipientByAccount.pending };
  },
  fulfilled(recipient) {
    return {
      type: types.fetchRecipientByAccount.fulfilled,
      payload: recipient,
    };
  },
  rejected() {
    return { type: types.fetchRecipientByAccount.rejected };
  },
};

const fetchRecipientById = {
  pending() {
    return { type: types.fetchRecipientById.pending };
  },
  fulfilled(recipient) {
    return { type: types.fetchRecipientById.fulfilled, payload: recipient };
  },
  rejected() {
    return { type: types.fetchRecipientById.rejected };
  },
};

const fetchRecipientByName = {
  pending() {
    return { type: types.fetchRecipientByName.pending };
  },
  fulfilled(recipient) {
    return { type: types.fetchRecipientByName.fulfilled, payload: recipient };
  },
  rejected() {
    return { type: types.fetchRecipientByName.rejected };
  },
};

const fetchUserFavoriteTransactions = {
  pending() {
    return { type: types.fetchUserFavoriteTransactions.pending };
  },
  fulfilled(favTransactions) {
    return {
      type: types.fetchUserFavoriteTransactions.fulfilled,
      payload: favTransactions,
    };
  },
  rejected() {
    return { type: types.fetchUserFavoriteTransactions.rejected };
  },
};

const setRecipientAsFavorite = () => ({
  type: types.setRecipientAsFavorite,
});

const fetchUserLastTransactions = {
  pending() {
    return { type: types.fetchUserLastTransactions.pending };
  },
  fulfilled(lastTransactions) {
    return {
      type: types.fetchUserLastTransactions.fulfilled,
      payload: lastTransactions,
    };
  },
  rejected() {
    return { type: types.fetchUserLastTransactions.rejected };
  },
};

const resetRecipient = () => ({ type: types.resetRecipient });

const saveTransferInformation = (transferInfoObj) => ({
  type: types.saveTransferInformation,
  payload: transferInfoObj,
});

const setIsFavorite = (isFavorite) => ({
  type: types.setIsFavorite,
  payload: isFavorite,
});

const resetSavedInformations = () => ({ type: types.resetSavedInformations });

const transferCash = {
  pending() {
    return { type: types.transferCash.pending };
  },
  fulfilled() {
    return { type: types.transferCash.fulfilled };
  },
  rejected(message) {
    return { type: types.transferCash.rejected, error: message };
  },
};

const resetTransferStatus = () => ({ type: types.resetTransferStatus });

const setDoesExists = (doesExists) => ({
  type: types.setDoesExists,
  payload: doesExists,
});

const resetFetchLastTransactionsStatus = () => ({
  type: types.resetFetchLastTransactionsStatus,
});

const resetFetchFavoriteTransactionsStatus = () => ({
  type: types.resetFetchFavoriteTransactionsStatus,
});

export {
  fetchRecipientByAccount,
  fetchRecipientById,
  fetchRecipientByName,
  fetchUserFavoriteTransactions,
  setRecipientAsFavorite,
  setRecipient,
  fetchUserLastTransactions,
  resetRecipient,
  saveTransferInformation,
  setIsFavorite,
  resetSavedInformations,
  transferCash,
  resetTransferStatus,
  resetFetchLastTransactionsStatus,
  resetFetchFavoriteTransactionsStatus,
  setDoesExists,
};
