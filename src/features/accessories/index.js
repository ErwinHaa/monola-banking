import reducer from './accessoriesReducer';

import * as accessoriesActions from './accessoriesActions';
import * as accessoriesTypes from './accessoriesTypes';
import * as accessoriesThunks from './accessoriesThunks';
import * as accessoriesSelectors from './accessoriesSelectors';

export {
  accessoriesActions,
  accessoriesTypes,
  accessoriesThunks,
  accessoriesSelectors,
};

export default reducer;
