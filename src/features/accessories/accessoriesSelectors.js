import { createSelector } from 'reselect';

const selectAllAccessories = (state) => state.accessories.accessories;

const selectAccessoryById = createSelector(
  [selectAllAccessories, (_, accessoryId) => accessoryId],
  (accessories, accessoryId) =>
    accessories.find((accessory) => accessory.id === accessoryId)
);

const selectAccessoriesFetchingStatus = (state) =>
  state.accessories.fetchAccessoriesStatus;

export {
  selectAllAccessories,
  selectAccessoryById,
  selectAccessoriesFetchingStatus,
};
