import * as types from './accessoriesTypes';

const fetchAccessories = {
  pending() {
    return { type: types.fetchAccessories.pending };
  },
  fulfilled(accessories) {
    return { type: types.fetchAccessories.fulfilled, payload: accessories };
  },
  rejected() {
    return { type: types.fetchAccessories.rejected };
  },
};

export { fetchAccessories };
