import monola from '../../services/apis/monola';

const fetchPets = async () => {
  const response = await monola.get('/pet');

  return response.data;
};

const postUserPet = async (petObj) => {
  const response = await monola.post('/user-pets', petObj);

  return response.data;
};

export { fetchPets, postUserPet };
