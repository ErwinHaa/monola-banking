const fetchPets = {
  pending: 'pet/fetchPetsPending',
  fulfilled: 'pet/fetchPetsFulfilled',
  rejected: 'pet/fetchPetsRejected',
};

const purchasePet = {
  pending: 'pet/purchasePetPending',
  fulfilled: 'pet/purchasePetFulfilled',
  rejected: 'pet/purchasePetRejected',
};

export { fetchPets, purchasePet };
