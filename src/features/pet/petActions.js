import * as types from './petTypes';

const fetchPets = {
  pending() {
    return { type: types.fetchPets.pending };
  },
  fulfilled(pets) {
    return { type: types.fetchPets.fulfilled, payload: pets };
  },
  rejected() {
    return { type: types.fetchPets.rejected };
  },
};

const purchasePet = {
  pending() {
    return { type: types.purchasePet.pending };
  },
  fulfilled() {
    return { type: types.purchasePet.fulfilled };
  },
  rejected() {
    return { type: types.purchasePet.rejected };
  },
};

export { fetchPets, purchasePet };
