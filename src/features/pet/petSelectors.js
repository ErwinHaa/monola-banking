import { createSelector } from 'reselect';

// import { userSelectors } from '../user';

const selectAllPets = (state) => state.pet.pets;

// const selectFilteredShopPets = createSelector([selectAllPets, userSelectors.selectUserPets], (pets, userPets) => {

// })

const selectPetById = createSelector(
  [selectAllPets, (_, petId) => petId],
  (pets, petId) => pets.find((pet) => pet.id === petId)
);

const selectPetsFetchingStatus = (state) => state.pet.petFetchingStatus;

export { selectAllPets, selectPetById, selectPetsFetchingStatus };
